webpackJsonp([0],{

/***/ "../../../../../src async recursive":
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = "../../../../../src async recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<app-icon-set></app-icon-set>\n<app-top-bar *ngIf=\"_user\"></app-top-bar>\n<app-popup></app-popup>\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  display: block; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user_user_service__ = __webpack_require__("../../../../../src/app/user/user.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = (function () {
    function AppComponent(_userService) {
        var _this = this;
        this._userService = _userService;
        this._userService.user$
            .subscribe(function (user) { return _this._user = user; });
    }
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-root',
        template: __webpack_require__("../../../../../src/app/app.component.html"),
        styles: [__webpack_require__("../../../../../src/app/app.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__user_user_service__["a" /* UserService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__user_user_service__["a" /* UserService */]) === "function" && _a || Object])
], AppComponent);

var _a;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_core_module__ = __webpack_require__("../../../../../src/app/core/core.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__user_user_module__ = __webpack_require__("../../../../../src/app/user/user.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__dashboard_dashboard_module__ = __webpack_require__("../../../../../src/app/dashboard/dashboard.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__card_card_module__ = __webpack_require__("../../../../../src/app/card/card.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__user_user_resolver_service__ = __webpack_require__("../../../../../src/app/user/user-resolver.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__routes__ = __webpack_require__("../../../../../src/app/routes.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_14_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_rxjs_add_operator_do__ = __webpack_require__("../../../../rxjs/add/operator/do.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_rxjs_add_operator_do___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15_rxjs_add_operator_do__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_rxjs_add_operator_toPromise__ = __webpack_require__("../../../../rxjs/add/operator/toPromise.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_16_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17_rxjs_add_operator_filter__ = __webpack_require__("../../../../rxjs/add/operator/filter.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17_rxjs_add_operator_filter___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_17_rxjs_add_operator_filter__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18_rxjs_add_operator_take__ = __webpack_require__("../../../../rxjs/add/operator/take.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18_rxjs_add_operator_take___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_18_rxjs_add_operator_take__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19_rxjs_add_observable_throw__ = __webpack_require__("../../../../rxjs/add/observable/throw.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19_rxjs_add_observable_throw___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_19_rxjs_add_observable_throw__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




















var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* AppComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* RouterModule */].forRoot(__WEBPACK_IMPORTED_MODULE_12__routes__["a" /* routes */]),
            __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__["a" /* SharedModule */],
            __WEBPACK_IMPORTED_MODULE_6__core_core_module__["a" /* CoreModule */],
            __WEBPACK_IMPORTED_MODULE_7__user_user_module__["a" /* UserModule */],
            __WEBPACK_IMPORTED_MODULE_8__dashboard_dashboard_module__["a" /* DashboardModule */],
            __WEBPACK_IMPORTED_MODULE_9__card_card_module__["a" /* CardModule */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_11__user_user_resolver_service__["a" /* UserResolver */]
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/card/card-edit/canvas/canvas.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"bar\">\n  <div class=\"group background-tools\">\n    <app-input\n      [type]=\"'color'\"\n      [name]=\"'backgroundColor'\"\n      [model]=\"_backgroundColor\"\n      (modelChange)=\"onBackgroundColorChange($event);\"\n      title=\"Background color\"\n    ></app-input>\n  </div>\n  <div class=\"group new-object-tools\">\n    <app-button (click)=\"onAddImageClick();\" [size]=\"'small'\" [icon]=\"'file-media'\" title=\"Add image field\"></app-button>\n    <app-button (click)=\"onAddTextFieldClick();\" [size]=\"'small'\" [icon]=\"'text-size'\" title=\"Add text field\"></app-button>\n    <app-button (click)=\"onAddSquareClick();\" [size]=\"'small'\" [icon]=\"'primitive-square'\" title=\"Add rectangle\"></app-button>\n    <app-button (click)=\"onAddCircleClick();\" [size]=\"'small'\" [icon]=\"'primitive-dot'\" title=\"Add circle\"></app-button>\n  </div>\n  <div class=\"group\" *ngIf=\"_selectedObject\">\n    <app-button (click)=\"onRemoveSelectedObjectClick();\" [size]=\"'small'\" [icon]=\"'trashcan'\" title=\"Remove selected object\"></app-button>\n  </div>\n  <div class=\"group layer-tools\" *ngIf=\"_selectedObject\">\n    <app-button (click)=\"onBringToFrontClick();\" [size]=\"'small'\" [icon]=\"'arrow-up'\" title=\"Bring object to front layer\"></app-button>\n    <app-button (click)=\"onBringToBackClick();\" [size]=\"'small'\" [icon]=\"'arrow-down'\" title=\"Bring object to bottom layer\"></app-button>\n    <app-button (click)=\"onLayerUpClick();\" [size]=\"'small'\" [icon]=\"'chevron-up'\" title=\"Move object one layer up\"></app-button>\n    <app-button (click)=\"onLayerDownClick();\" [size]=\"'small'\" [icon]=\"'chevron-down'\" title=\"Move object one layer down\"></app-button>\n  </div>\n  <div class=\"group text-tools\" *ngIf=\"_selectedObject && (_selectedObject.type === 'rect' || _selectedObject.type === 'circle')\">\n    <app-input\n      [type]=\"'color'\"\n      [name]=\"'figureColor'\"\n      [model]=\"_figureColor\"\n      (modelChange)=\"onFigureColorChange($event);\"\n      title=\"Figure color\"\n    ></app-input>\n  </div>\n  <div class=\"group text-tools\" *ngIf=\"_selectedObject && _selectedObject.type === 'i-text'\">\n    <div class=\"select\">\n      <select (change)=\"onFontFamilyChange();\" [(ngModel)]=\"_fontFamily\" class=\"small\" title=\"Text font\">\n        <option *ngFor=\"let fontFamily of _fontFamilies\" [ngValue]=\"fontFamily\">{{fontFamily}}</option>\n      </select>\n    </div>\n    <div class=\"select\">\n      <select (change)=\"onTextStyleChange();\" [(ngModel)]=\"_textStyle\" class=\"small\" title=\"Text style\">\n        <option *ngFor=\"let textStyle of _textStyles\" [ngValue]=\"textStyle\">{{textStyle.name}}</option>\n      </select>\n    </div>\n    <div class=\"select\">\n      <select (change)=\"onTextSizeChange();\" [(ngModel)]=\"_textSize\" class=\"small\" title=\"Text size\">\n        <option *ngFor=\"let textSize of _textSizes\" [ngValue]=\"textSize\">{{textSize}}</option>\n      </select>\n    </div>\n    <app-input\n      [type]=\"'color'\"\n      [name]=\"'textColor'\"\n      [model]=\"_textColor\"\n      (modelChange)=\"onTextColorChange($event);\"\n      title=\"Text color\"\n    ></app-input>\n  </div>\n</div>\n<div\n  class=\"image-upload\"\n  [ngClass]=\"{'active': _imageUploadActive}\"\n>\n  <div class=\"controls\">\n    <app-button (click)=\"onCloseImageUploadClick();\" [size]=\"'small'\" [icon]=\"'x'\"></app-button>\n  </div>\n  <app-image-addition\n    [card]=\"card\"\n    (addToCard)=\"onAddLibraryItemToCard($event);\"\n  ></app-image-addition>\n</div>\n<canvas id=\"canvas\"></canvas>\n"

/***/ }),

/***/ "../../../../../src/app/card/card-edit/canvas/canvas.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  display: block;\n  position: relative;\n  overflow: hidden;\n  width: 1280px; }\n\n.bar {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  background: #5d605d;\n  position: relative;\n  z-index: 10;\n  height: 2.4em; }\n  .bar .group {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    padding: 0.3em 0.4em;\n    margin-right: 0.6em;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    .bar .group app-button, .bar .group .select {\n      margin-right: 0.4em; }\n      .bar .group app-button:last-child, .bar .group .select:last-child {\n        margin-right: 0; }\n    .bar .group app-input {\n      margin: 0; }\n    .bar .group.text-tools {\n      background: #767a76; }\n\n.image-upload {\n  position: absolute;\n  z-index: 5;\n  background: #767a76;\n  padding: 0.8em;\n  width: 40%;\n  top: 2.4em;\n  left: 30%;\n  transition: all 200ms ease-in-out;\n  -webkit-transform: translate3d(0, -100%, 0);\n          transform: translate3d(0, -100%, 0); }\n  .image-upload.active {\n    -webkit-transform: translate3d(0, 0, 0);\n            transform: translate3d(0, 0, 0); }\n\n.canvas-container {\n  width: 100%; }\n  .canvas-container canvas {\n    background: #fff; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/card/card-edit/canvas/canvas.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__card__ = __webpack_require__("../../../../../src/app/card/card.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__preview_service__ = __webpack_require__("../../../../../src/app/card/preview.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__fabric_text_service__ = __webpack_require__("../../../../../src/app/card/card-edit/canvas/fabric-text.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_api_service__ = __webpack_require__("../../../../../src/app/shared/api.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CanvasComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CANVAS_WIDTH = 1280;
var CANVAS_HEIGHT = 720;
var CanvasComponent = (function () {
    function CanvasComponent(_previewService, _fabricTextService, _apiService) {
        var _this = this;
        this._previewService = _previewService;
        this._fabricTextService = _fabricTextService;
        this._apiService = _apiService;
        this._previewOptions = { format: 'png', multiplier: 0.2 };
        this._backgroundColor = '#FFFFFF';
        this._defaultObjectPosition = { left: 100, top: 100 };
        this._selectedObject = null;
        this._defaultFigureColor = '#2E7D32';
        this._figureColor = this._defaultFigureColor;
        this._textStyle = null;
        this._fontFamily = null;
        this._textSize = null;
        this._fabricTextService.textStyles$.subscribe(function (textStyles) { return _this._textStyles = textStyles; });
        this._fabricTextService.fontFamilies$.subscribe(function (fontFamilies) { return _this._fontFamilies = fontFamilies; });
        this._fabricTextService.textSizes$.subscribe(function (textSizes) { return _this._textSizes = textSizes; });
    }
    CanvasComponent.prototype.ngOnInit = function () {
        this.initCanvas();
        this._imageUploadActive = false;
    };
    CanvasComponent.prototype.onObjectSelect = function (object) {
        this._selectedObject = object;
        if (this._selectedObject.type === 'i-text')
            this.onTextObjectSelect();
        if (this._selectedObject.type === 'rect' || this._selectedObject.type === 'circle')
            this.onFigureObjectSelect();
    };
    CanvasComponent.prototype.onObjectDeselect = function () {
        this._selectedObject = null;
    };
    CanvasComponent.prototype.onFigureObjectSelect = function () {
        this._figureColor = this._selectedObject.fill;
    };
    CanvasComponent.prototype.onTextObjectSelect = function () {
        this._textStyle = this._fabricTextService.getMatchingTextStyle(this._selectedObject);
        this._fontFamily = this._selectedObject.fontFamily;
        this._textSize = this._selectedObject.fontSize;
        this._textColor = this._selectedObject.fill;
    };
    CanvasComponent.prototype.addObjectToCanvas = function (object) {
        this._canvas.add(object);
    };
    CanvasComponent.prototype.onObjectAdded = function (object) {
        var _this = this;
        object.on('removed', function (event) { return _this.onObjectRemoved(event); });
    };
    CanvasComponent.prototype.onObjectRemoved = function (object) {
        console.log('Removed', object);
    };
    CanvasComponent.prototype.addImage = function (imageUrl) {
        var _this = this;
        fabric.Image.fromURL(imageUrl, function (image) { return _this.addObjectToCanvas(image); }, { crossOrigin: 'use-credentials' });
    };
    CanvasComponent.prototype.onRemoveSelectedObjectClick = function () {
        this._canvas.getActiveObject().remove();
    };
    CanvasComponent.prototype.onAddImageClick = function () {
        this._imageUploadActive = !this._imageUploadActive;
    };
    CanvasComponent.prototype.onCloseImageUploadClick = function () {
        this._imageUploadActive = false;
    };
    CanvasComponent.prototype.onAddLibraryItemToCard = function (image) {
        var absoluteImageUrl = this._apiService.base + '/card/img/' + this.card._id + '/' + image.fileName;
        this.addImage(absoluteImageUrl);
        this._imageUploadActive = false;
    };
    CanvasComponent.prototype.onAddTextFieldClick = function () {
        this.addObjectToCanvas(new fabric.IText('empty text', __WEBPACK_IMPORTED_MODULE_4_lodash__["merge"](this._defaultObjectPosition, this._fabricTextService.getDefaultTextProperties())));
    };
    CanvasComponent.prototype.onAddSquareClick = function () {
        this.addObjectToCanvas(new fabric.Rect(__WEBPACK_IMPORTED_MODULE_4_lodash__["merge"](this._defaultObjectPosition, { width: 200, height: 150, fill: this._defaultFigureColor })));
    };
    CanvasComponent.prototype.onAddCircleClick = function () {
        this.addObjectToCanvas(new fabric.Circle(__WEBPACK_IMPORTED_MODULE_4_lodash__["merge"](this._defaultObjectPosition, { width: 200, height: 150, radius: 100, fill: this._defaultFigureColor })));
    };
    CanvasComponent.prototype.onBackgroundColorChange = function (colorHex) {
        var _this = this;
        var color = new fabric.Color(colorHex);
        this._canvas.setBackgroundColor(color.toRgba(), function () {
            _this._canvas.renderAll.bind(_this._canvas)();
        });
    };
    CanvasComponent.prototype.onBringToFrontClick = function () {
        this._selectedObject.bringToFront();
    };
    CanvasComponent.prototype.onBringToBackClick = function () {
        this._selectedObject.sendToBack();
    };
    CanvasComponent.prototype.onLayerUpClick = function () {
        this._selectedObject.bringForward();
    };
    CanvasComponent.prototype.onLayerDownClick = function () {
        this._selectedObject.sendBackwards();
    };
    CanvasComponent.prototype.onFigureColorChange = function (colorHex) {
        this._figureColor = colorHex;
        this._selectedObject.setFill(this._figureColor);
        this._canvas.renderAll.bind(this._canvas)();
    };
    CanvasComponent.prototype.onTextStyleChange = function () {
        if (this._selectedObject.type === 'i-text') {
            this._selectedObject.setFontWeight(this._textStyle.fontWeight);
            this._selectedObject.setFontStyle(this._textStyle.fontStyle);
            this._canvas.renderAll.bind(this._canvas)();
        }
    };
    CanvasComponent.prototype.onFontFamilyChange = function () {
        if (this._selectedObject.type === 'i-text') {
            this._selectedObject.setFontFamily(this._fontFamily);
            this._canvas.renderAll.bind(this._canvas)();
        }
    };
    CanvasComponent.prototype.onTextSizeChange = function () {
        if (this._selectedObject.type === 'i-text') {
            this._selectedObject.setFontSize(this._textSize);
            this._canvas.renderAll.bind(this._canvas)();
        }
    };
    CanvasComponent.prototype.onTextColorChange = function (colorHex) {
        if (this._selectedObject.type === 'i-text') {
            this._textColor = colorHex;
            this._selectedObject.setFill(this._textColor);
            this._canvas.renderAll.bind(this._canvas)();
        }
    };
    CanvasComponent.prototype.serialize = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.card.data = _this._canvas.toJSON();
            _this._previewService.save(_this._canvas.toDataURL(_this._previewOptions), _this.card)
                .then(function (fileName) {
                _this.card.preview = fileName;
                resolve();
            });
        });
    };
    CanvasComponent.prototype.deserialize = function () {
        var _this = this;
        return new Promise(function (resolve) {
            if (_this.card.data.background) {
                var color = fabric.Color.fromRgba(_this.card.data.background);
                _this._backgroundColor = '#' + color.toHex();
            }
            _this._canvas.loadFromJSON(_this.card.data, function () {
                var color = new fabric.Color(_this._backgroundColor);
                _this._canvas.setBackgroundColor(color.toRgba(), function () {
                    _this._canvas.renderAll.bind(_this._canvas)();
                    resolve();
                });
            });
        });
    };
    CanvasComponent.prototype.initCanvas = function () {
        var _this = this;
        this._canvas = new fabric.Canvas('canvas', {
            width: CANVAS_WIDTH,
            height: CANVAS_HEIGHT,
            preserveObjectStacking: true
        });
        this.deserialize()
            .then(function () {
            _this._canvas.on('object:selected', function (event) { return _this.onObjectSelect(event.target); });
            _this._canvas.on('selection:cleared', function () { return _this.onObjectDeselect(); });
            _this._canvas.on('object:added', function (event) { return _this.onObjectAdded(event.target); });
            var objects = _this._canvas.getObjects();
            __WEBPACK_IMPORTED_MODULE_4_lodash__["forEach"](objects, function (object) {
                object.on('removed', function (event) { return _this.onObjectRemoved(event); });
            });
        });
    };
    return CanvasComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__card__["a" /* Card */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__card__["a" /* Card */]) === "function" && _a || Object)
], CanvasComponent.prototype, "card", void 0);
CanvasComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-canvas',
        template: __webpack_require__("../../../../../src/app/card/card-edit/canvas/canvas.component.html"),
        styles: [__webpack_require__("../../../../../src/app/card/card-edit/canvas/canvas.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__preview_service__["a" /* PreviewService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__preview_service__["a" /* PreviewService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__fabric_text_service__["a" /* FabricTextService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__fabric_text_service__["a" /* FabricTextService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5__shared_api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__shared_api_service__["a" /* ApiService */]) === "function" && _d || Object])
], CanvasComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=canvas.component.js.map

/***/ }),

/***/ "../../../../../src/app/card/card-edit/canvas/fabric-text.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__text_styles__ = __webpack_require__("../../../../../src/app/card/card-edit/canvas/text-styles.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__text_properties__ = __webpack_require__("../../../../../src/app/card/card-edit/canvas/text-properties.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FabricTextService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var FabricTextService = (function () {
    function FabricTextService() {
        this.textStyles$ = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.fontFamilies$ = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.textSizes$ = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.initTextStyles();
        this.initFontFamilies();
        this.initTextSizes();
    }
    FabricTextService.prototype.initTextStyles = function () {
        this.textStyles$.next(__WEBPACK_IMPORTED_MODULE_3__text_styles__["a" /* TEXT_STYLES */]);
    };
    FabricTextService.prototype.initFontFamilies = function () {
        this.fontFamilies$.next([
            'Arial',
            'Times New Roman',
            'Courier New',
            'Comic Sans MS',
            'Helvetica',
            'Tahoma',
            'Verdana'
        ]);
    };
    FabricTextService.prototype.initTextSizes = function () {
        this.textSizes$.next([24, 30, 36, 42, 48, 54, 60, 70, 80]);
    };
    FabricTextService.prototype.getDefaultTextProperties = function () {
        var defaults = new __WEBPACK_IMPORTED_MODULE_4__text_properties__["a" /* TextProperties */]();
        var defaultStyles = __WEBPACK_IMPORTED_MODULE_2_lodash__["head"](this.textStyles$.getValue());
        defaults.fontStyle = defaultStyles.fontStyle;
        defaults.fontWeight = defaultStyles.fontWeight;
        defaults.fontFamily = __WEBPACK_IMPORTED_MODULE_2_lodash__["head"](this.fontFamilies$.getValue());
        defaults.fontSize = 42;
        defaults.fill = '#212121';
        return defaults;
    };
    FabricTextService.prototype.getMatchingTextStyle = function (object) {
        return __WEBPACK_IMPORTED_MODULE_2_lodash__["find"](this.textStyles$.getValue(), {
            fontStyle: object.fontStyle,
            fontWeight: object.fontWeight,
        });
    };
    return FabricTextService;
}());
FabricTextService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [])
], FabricTextService);

//# sourceMappingURL=fabric-text.service.js.map

/***/ }),

/***/ "../../../../../src/app/card/card-edit/canvas/text-properties.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TextProperties; });
var TextProperties = (function () {
    function TextProperties() {
    }
    return TextProperties;
}());

//# sourceMappingURL=text-properties.js.map

/***/ }),

/***/ "../../../../../src/app/card/card-edit/canvas/text-styles.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TEXT_STYLES; });
var TEXT_STYLES = [
    {
        name: 'Regular',
        fontStyle: 'normal',
        fontWeight: 400
    },
    {
        name: 'Bold',
        fontStyle: 'normal',
        fontWeight: 700
    },
    {
        name: 'Italic',
        fontStyle: 'italic',
        fontWeight: 400
    },
    {
        name: 'Bold Italic',
        fontStyle: 'italic',
        fontWeight: 700
    },
];
//# sourceMappingURL=text-styles.js.map

/***/ }),

/***/ "../../../../../src/app/card/card-edit/card-edit.component.html":
/***/ (function(module, exports) {

module.exports = "<app-title-bar [title]=\"'Card edit'\" [backEnabled]=\"true\" (back)=\"onBackClick()\"></app-title-bar>\n\n<form #f=\"ngForm\" (ngSubmit)=\"onSubmit(f)\" novalidate>\n  <div class=\"row\">\n    <div class=\"column basic\">\n      <div class=\"form-group\">\n        <label>Name</label>\n        <input class=\"form-control\" type=\"text\" name=\"name\" placeholder=\"card name\" [(ngModel)]=\"_card.name\">\n      </div>\n      <app-canvas [card]=\"_card\"></app-canvas>\n    </div>\n    <div class=\"column extra\">\n      <app-input [type]=\"'number'\" [label]=\"'Pin limit'\" [name]=\"'pinLimit'\" [(model)]=\"_card.pinLimit\" ngDefaultControl></app-input>\n      <app-input [type]=\"'wysiwyg'\" [label]=\"'Comments'\" [name]=\"'comments'\" [(model)]=\"_card.comments\" ngDefaultControl></app-input>\n      <div class=\"infos\">\n        <div class=\"info\">\n          Created {{_card.created | date:'medium'}} by {{_card.createdBy.displayName}}\n        </div>\n        <div class=\"info\">\n          Last modified {{_card.modified | date:'medium'}} by {{_card.modifiedBy.displayName}}\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"row\">\n    <div class=\"column controls\">\n      <app-button [type]=\"'submit'\" [icon]=\"'check'\" [caption]=\"'Save'\"></app-button>\n      <app-button (click)=\"onCancelClick();\" [icon]=\"'x'\" [caption]=\"'Cancel'\"></app-button>\n    </div>\n  </div>\n</form>\n"

/***/ }),

/***/ "../../../../../src/app/card/card-edit/card-edit.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: stretch;\n      -ms-flex-align: stretch;\n          align-items: stretch;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  padding: 5.2em 2em 2em; }\n  :host.flex-end {\n    -webkit-box-pack: end;\n        -ms-flex-pack: end;\n            justify-content: flex-end; }\n  :host > * {\n    -webkit-box-flex: 1;\n        -ms-flex: 1 1 auto;\n            flex: 1 1 auto; }\n    :host > *.no-flex {\n      -webkit-box-flex: 0;\n          -ms-flex: none;\n              flex: none; }\n\n.form-group {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: stretch;\n      -ms-flex-align: stretch;\n          align-items: stretch;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-flex: 0;\n      -ms-flex-positive: 0;\n          flex-grow: 0;\n  margin-bottom: 1em; }\n  .form-group.flex-end {\n    -webkit-box-pack: end;\n        -ms-flex-pack: end;\n            justify-content: flex-end; }\n  .form-group > * {\n    -webkit-box-flex: 1;\n        -ms-flex: 1 1 auto;\n            flex: 1 1 auto; }\n    .form-group > *.no-flex {\n      -webkit-box-flex: 0;\n          -ms-flex: none;\n              flex: none; }\n  .form-group label {\n    text-transform: uppercase; }\n  .form-group .form-control {\n    color: #FFFFFF;\n    background: #5d605d;\n    border: 0 none;\n    padding: 0 0.5em;\n    height: 3em;\n    line-height: 3em;\n    padding: 0 1.5em;\n    height: 3em;\n    line-height: 3em; }\n    .form-group .form-control::-webkit-input-placeholder {\n      color: #cccccc; }\n    .form-group .form-control:-moz-placeholder {\n      color: #cccccc; }\n    .form-group .form-control::-moz-placeholder {\n      color: #cccccc; }\n    .form-group .form-control:-ms-input-placeholder {\n      color: #cccccc; }\n    .form-group .form-control.small {\n      height: 2em;\n      line-height: 2em;\n      font-size: 0.9em; }\n\nform .row {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex; }\n  form .row .column.basic {\n    margin-right: 2em;\n    margin-bottom: 1em;\n    -webkit-box-flex: 0;\n        -ms-flex-positive: 0;\n            flex-grow: 0;\n    -ms-flex-negative: 1;\n        flex-shrink: 1; }\n  form .row .column.extra {\n    -webkit-box-flex: 1;\n        -ms-flex-positive: 1;\n            flex-grow: 1; }\n\n@media (max-width: 1800px) {\n  form {\n    width: 1280px; }\n    form .row {\n      -ms-flex-wrap: wrap;\n          flex-wrap: wrap;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: column;\n              flex-direction: column; }\n      form .row .column {\n        -webkit-box-orient: vertical;\n        -webkit-box-direction: normal;\n            -ms-flex-direction: column;\n                flex-direction: column;\n        -webkit-box-align: start;\n            -ms-flex-align: start;\n                align-items: flex-start;\n        -ms-flex-negative: 1;\n            flex-shrink: 1; }\n        form .row .column.basic {\n          margin-right: 0; } }\n\n.infos {\n  margin-bottom: 1em; }\n  .infos .info {\n    font-size: 0.8em;\n    color: #b3b2b2; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/card/card-edit/card-edit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__card_service__ = __webpack_require__("../../../../../src/app/card/card.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__canvas_canvas_component__ = __webpack_require__("../../../../../src/app/card/card-edit/canvas/canvas.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_api_service__ = __webpack_require__("../../../../../src/app/shared/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_lodash__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardEditComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CardEditComponent = (function () {
    function CardEditComponent(_apiService, _cardService, _route, _router) {
        this._apiService = _apiService;
        this._cardService = _cardService;
        this._route = _route;
        this._router = _router;
    }
    CardEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route.data
            .subscribe(function (data) {
            _this._card = data.card;
            _this.fixUrls();
        });
    };
    CardEditComponent.prototype.onSubmit = function (f) {
        var _this = this;
        if (f.valid) {
            this._canvasComponent.serialize()
                .then(function () {
                _this._cardService.update(_this._card)
                    .subscribe(function () { return _this.backToCardSet(); });
            });
        }
    };
    CardEditComponent.prototype.onBackClick = function () {
        this.backToCardSet();
    };
    CardEditComponent.prototype.onCancelClick = function () {
        this.backToCardSet();
    };
    CardEditComponent.prototype.backToCardSet = function () {
        this._router.navigate(['/card-set/edit/' + this._card.cardSet]);
    };
    CardEditComponent.prototype.fixUrls = function () {
        var _this = this;
        if (this._card.data && this._card.data.objects) {
            __WEBPACK_IMPORTED_MODULE_5_lodash__["forEach"](this._card.data.objects, function (object) {
                if (object.type === 'image') {
                    object.src = _this._apiService.base + object.src;
                }
            });
        }
    };
    return CardEditComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_3__canvas_canvas_component__["a" /* CanvasComponent */]),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__canvas_canvas_component__["a" /* CanvasComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__canvas_canvas_component__["a" /* CanvasComponent */]) === "function" && _a || Object)
], CardEditComponent.prototype, "_canvasComponent", void 0);
CardEditComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-card-edit',
        template: __webpack_require__("../../../../../src/app/card/card-edit/card-edit.component.html"),
        styles: [__webpack_require__("../../../../../src/app/card/card-edit/card-edit.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__shared_api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__shared_api_service__["a" /* ApiService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__card_service__["a" /* CardService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__card_service__["a" /* CardService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* ActivatedRoute */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === "function" && _e || Object])
], CardEditComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=card-edit.component.js.map

/***/ }),

/***/ "../../../../../src/app/card/card-edit/image-addition/image-addition.component.html":
/***/ (function(module, exports) {

module.exports = "<app-input-upload\n  [size]=\"'small'\"\n  [options]=\"_imageUploadOptions\"\n  [single]=\"true\"\n  [fileType]=\"'image'\"\n  [name]=\"'image'\"\n  [(model)]=\"_image.fileName\"\n  [imageUrl]=\"_imageUrl\"\n  (modelChange)=\"onUploadFinished()\"\n></app-input-upload>\n<div class=\"url-paste\">\n  <div class=\"controls\">\n    <app-input\n      [model]=\"_pastedImageUrl\"\n      [size]=\"'small'\"\n      (modelChange)=\"onPastedImageUrlChange($event)\"\n      [name]=\"'pastedUrl'\"\n      [placeholder]=\"'paste URL address'\"\n    ></app-input>\n    <app-button (click)=\"onPastedUrlPreviewClick()\" [icon]=\"'eye'\" title=\"Preview\"></app-button>\n    <app-button (click)=\"onPastedUrlAcceptClick()\" [icon]=\"'check'\" title=\"Accept\"></app-button>\n  </div>\n  <div class=\"preview\" *ngIf=\"_pastedLegitImageUrl && _pastedLegitImageUrl.length > 0\">\n    <img src=\"{{_pastedLegitImageUrl}}\">\n  </div>\n  <div\n    *ngIf=\"_error\"\n    class=\"error\"\n  >\n    {{_error.message}}\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/card/card-edit/image-addition/image-addition.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  display: block;\n  /*\r\n  width: 49%;\r\n  margin-right: 2%;\r\n\r\n  &:nth-child(2n) {\r\n    margin-right: 0;\r\n  }\r\n  */ }\n\n.controls {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: end;\n      -ms-flex-pack: end;\n          justify-content: flex-end;\n  margin-bottom: 0.4em; }\n  .controls app-button {\n    margin-right: 0.4em; }\n\napp-input-upload {\n  margin: 0; }\n\n.url-paste .controls {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  height: 36px; }\n  .url-paste .controls app-input {\n    height: 100%;\n    -webkit-box-flex: 1;\n        -ms-flex-positive: 1;\n            flex-grow: 1;\n    margin: 0 1em 0 0; }\n  .url-paste .controls app-button {\n    height: 100%; }\n\n.url-paste .preview {\n  margin-top: 0.4em;\n  text-align: center; }\n  .url-paste .preview img {\n    max-width: 100%;\n    max-height: 18em; }\n\n.url-paste .error {\n  font-size: 0.8em; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/card/card-edit/image-addition/image-addition.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_api_service__ = __webpack_require__("../../../../../src/app/shared/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__card__ = __webpack_require__("../../../../../src/app/card/card.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__image__ = __webpack_require__("../../../../../src/app/card/image.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__card_service__ = __webpack_require__("../../../../../src/app/card/card.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_http_tools_service__ = __webpack_require__("../../../../../src/app/shared/http-tools.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageAdditionComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ImageAdditionComponent = (function () {
    function ImageAdditionComponent(_apiService, _cardService) {
        this._apiService = _apiService;
        this._cardService = _cardService;
        this.addToCard = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    ImageAdditionComponent.prototype.ngOnInit = function () {
        this._imageUploadOptions = {
            url: this._apiService.URL + 'cards/upload',
            queueLimit: 1,
            autoUpload: true,
            removeAfterUpload: true,
            //allowedMimeType: ['image/*'],
            //maxFileSize: 5*1024*1024,
            additionalParameter: {
                id: this.card._id
            }
        };
        this._imageUrl = this._apiService.base + '/card/img/' + this.card._id + '/';
        this._image = new __WEBPACK_IMPORTED_MODULE_3__image__["a" /* Image */]();
    };
    ImageAdditionComponent.prototype.onUploadFinished = function () {
        this.addToCard.emit(this._image);
        this._image = new __WEBPACK_IMPORTED_MODULE_3__image__["a" /* Image */]();
    };
    ImageAdditionComponent.prototype.onPastedImageUrlChange = function (url) {
        this._pastedImageUrl = url;
        this._error = null;
    };
    ImageAdditionComponent.prototype.onPastedUrlPreviewClick = function () {
        if (this._pastedImageUrl && __WEBPACK_IMPORTED_MODULE_5__shared_http_tools_service__["a" /* HttpToolsService */].validUrl(this._pastedImageUrl)) {
            this._pastedLegitImageUrl = this._pastedImageUrl;
        }
        else {
            this._error = new Error('Url is incorrect');
        }
    };
    ImageAdditionComponent.prototype.onPastedUrlAcceptClick = function () {
        var _this = this;
        if (this._pastedImageUrl && __WEBPACK_IMPORTED_MODULE_5__shared_http_tools_service__["a" /* HttpToolsService */].validUrl(this._pastedImageUrl)) {
            var info = {
                id: this.card._id,
                url: this._pastedImageUrl
            };
            this._cardService.scrap(info)
                .subscribe(function (response) {
                var image = new __WEBPACK_IMPORTED_MODULE_3__image__["a" /* Image */]();
                image.fileName = response.fileName;
                _this.addToCard.emit(image);
                _this._pastedLegitImageUrl = null;
                _this._pastedImageUrl = null;
            });
        }
        else {
            this._error = new Error('Url is incorrect');
        }
    };
    return ImageAdditionComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__card__["a" /* Card */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__card__["a" /* Card */]) === "function" && _a || Object)
], ImageAdditionComponent.prototype, "card", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _b || Object)
], ImageAdditionComponent.prototype, "addToCard", void 0);
ImageAdditionComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-image-addition',
        template: __webpack_require__("../../../../../src/app/card/card-edit/image-addition/image-addition.component.html"),
        styles: [__webpack_require__("../../../../../src/app/card/card-edit/image-addition/image-addition.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__shared_api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__shared_api_service__["a" /* ApiService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__card_service__["a" /* CardService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__card_service__["a" /* CardService */]) === "function" && _d || Object])
], ImageAdditionComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=image-addition.component.js.map

/***/ }),

/***/ "../../../../../src/app/card/card-preview/card-preview.component.html":
/***/ (function(module, exports) {

module.exports = "<img\n  class=\"preview\"\n  *ngIf=\"!_noPreview\"\n  src=\"{{_url}}\"\n>\n<div\n  class=\"no-preview\"\n  *ngIf=\"_noPreview\"\n>\n  <app-icon [name]=\"'circle-slash'\" [size]=\"_iconSize\"></app-icon>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/card/card-preview/card-preview.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  width: 256px;\n  height: 144px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background: #3d404b; }\n  :host.size-small {\n    width: 128px;\n    height: 72px; }\n\n.preview {\n  max-width: 100%; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/card/card-preview/card-preview.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_api_service__ = __webpack_require__("../../../../../src/app/shared/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__card__ = __webpack_require__("../../../../../src/app/card/card.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__card_set__ = __webpack_require__("../../../../../src/app/card/card-set.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardPreviewComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CardPreviewComponent = (function () {
    function CardPreviewComponent(_apiService) {
        this._apiService = _apiService;
        this.hostClass = '';
    }
    CardPreviewComponent.prototype.ngOnInit = function () {
        this._noPreview = true;
        if (this.card) {
            this._url = this._apiService.base + '/card/img/' + this.card._id + '/' + this.card.preview;
            if (this.card.preview)
                this._noPreview = false;
        }
        if (this.cardSet) {
            this._url = this.cardSet.preview;
            if (this.cardSet.preview)
                this._noPreview = false;
        }
        switch (this.size) {
            case 'small':
                this.hostClass = 'size-small';
                this._iconSize = 'big';
                break;
            default:
                this.hostClass = 'size-default';
                this._iconSize = 'large';
        }
    };
    return CardPreviewComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__card__["a" /* Card */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__card__["a" /* Card */]) === "function" && _a || Object)
], CardPreviewComponent.prototype, "card", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__card_set__["a" /* CardSet */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__card_set__["a" /* CardSet */]) === "function" && _b || Object)
], CardPreviewComponent.prototype, "cardSet", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], CardPreviewComponent.prototype, "size", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostBinding"])('class'),
    __metadata("design:type", String)
], CardPreviewComponent.prototype, "hostClass", void 0);
CardPreviewComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-card-preview',
        template: __webpack_require__("../../../../../src/app/card/card-preview/card-preview.component.html"),
        styles: [__webpack_require__("../../../../../src/app/card/card-preview/card-preview.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__shared_api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__shared_api_service__["a" /* ApiService */]) === "function" && _c || Object])
], CardPreviewComponent);

var _a, _b, _c;
//# sourceMappingURL=card-preview.component.js.map

/***/ }),

/***/ "../../../../../src/app/card/card-resolver.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__card_service__ = __webpack_require__("../../../../../src/app/card/card.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardResolver; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CardResolver = (function () {
    function CardResolver(_cardService, _router) {
        this._cardService = _cardService;
        this._router = _router;
    }
    CardResolver.prototype.resolve = function (route, state) {
        var _this = this;
        var id = route.params['id'];
        return this._cardService.get(id)
            .do(function (card) {
            if (!card) {
                _this._router.navigate(['/card/list']);
            }
        });
    };
    return CardResolver;
}());
CardResolver = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__card_service__["a" /* CardService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__card_service__["a" /* CardService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _b || Object])
], CardResolver);

var _a, _b;
//# sourceMappingURL=card-resolver.service.js.map

/***/ }),

/***/ "../../../../../src/app/card/card-set-edit/card-list/card-list.component.html":
/***/ (function(module, exports) {

module.exports = "<app-title-bar [title]=\"'Cards'\" [size]=\"'small'\">\n  <form #f=\"ngForm\" (ngSubmit)=\"onSubmit(f)\" novalidate>\n    <app-button [type]=\"'submit'\" [icon]=\"'plus'\" [caption]=\"'Create'\"></app-button>\n  </form>\n</app-title-bar>\n\n<div\n  class=\"cards\"\n  dnd-sortable-container\n  [sortableData]=\"cardSet.cards\"\n>\n  <div\n    class=\"card\"\n    *ngFor=\"let card of cardSet.cards; let i = index\"\n    dnd-sortable\n    [sortableIndex]=\"i\"\n    (onDropSuccess)=\"onCardDrop(card, i)\"\n  >\n    <div class=\"bar\">\n      <div class=\"group\" dnd-sortable-handle>\n        <div class=\"drag-indicator\">\n          <app-icon [name]=\"'grabber'\" [size]=\"'small'\"></app-icon>\n        </div>\n        <div class=\"name\">{{card.name}}</div>\n      </div>\n      <div class=\"group\">\n        <app-button (click)=\"onRemoveClick(card);\" [size]=\"'tiny'\" [icon]=\"'x'\" title=\"Remove\"></app-button>\n      </div>\n    </div>\n    <div class=\"preview\" (click)=\"onEditClick(card);\">\n      <div class=\"overlay\">\n        <app-icon [name]=\"'pencil'\" [size]=\"'big'\"></app-icon>\n      </div>\n      <app-card-preview [card]=\"card\"></app-card-preview>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/card/card-set-edit/card-list/card-list.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  display: block; }\n\n.cards {\n  background: #31343d;\n  padding: 1em 1em 0 1em;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap; }\n  .cards .card {\n    margin-right: 1em;\n    margin-bottom: 1em;\n    position: relative;\n    overflow: hidden;\n    -webkit-user-select: none;\n       -moz-user-select: none;\n        -ms-user-select: none;\n            user-select: none;\n    width: 256px; }\n    .cards .card .bar {\n      background: #484c59;\n      width: 100%;\n      height: 27px;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-pack: justify;\n          -ms-flex-pack: justify;\n              justify-content: space-between; }\n      .cards .card .bar .group {\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        padding: 0 0.4em;\n        -webkit-box-align: center;\n            -ms-flex-align: center;\n                align-items: center; }\n        .cards .card .bar .group .name {\n          width: 190px;\n          padding: 0 0.4em;\n          font-size: 0.8em;\n          white-space: nowrap;\n          overflow: hidden;\n          text-overflow: ellipsis; }\n        .cards .card .bar .group app-button {\n          margin-left: 0.4em; }\n    .cards .card .preview {\n      width: 100%;\n      position: relative; }\n      .cards .card .preview .overlay {\n        position: absolute;\n        top: 0;\n        left: 0;\n        right: 0;\n        bottom: 0;\n        background: rgba(0, 0, 0, 0.5);\n        transition: all 200ms ease-in-out;\n        visibility: hidden;\n        opacity: 0;\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-pack: center;\n            -ms-flex-pack: center;\n                justify-content: center;\n        -webkit-box-align: center;\n            -ms-flex-align: center;\n                align-items: center;\n        z-index: 10; }\n      .cards .card .preview:hover .overlay {\n        visibility: visible;\n        opacity: 1; }\n    .cards .card.dnd-drag-start {\n      -webkit-transform: scale(0.8);\n              transform: scale(0.8);\n      opacity: 0.7;\n      border: 2px dashed #000; }\n    .cards .card.dnd-drag-enter {\n      opacity: 0.7;\n      border: 2px dashed #000; }\n    .cards .card.dnd-drag-over {\n      border: 2px dashed #000; }\n    .cards .card.dnd-sortable-drag {\n      -webkit-transform: scale(0.9);\n              transform: scale(0.9);\n      opacity: 0.7;\n      border: 1px dashed #000; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/card/card-set-edit/card-list/card-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__card__ = __webpack_require__("../../../../../src/app/card/card.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__card_service__ = __webpack_require__("../../../../../src/app/card/card.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__card_set__ = __webpack_require__("../../../../../src/app/card/card-set.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_popup_service__ = __webpack_require__("../../../../../src/app/shared/popup.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardListComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var CardListComponent = (function () {
    function CardListComponent(_cardService, _router, _popupService) {
        this._cardService = _cardService;
        this._router = _router;
        this._popupService = _popupService;
        this.onCardCreated = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.onCardRemoved = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    CardListComponent.prototype.ngOnInit = function () {
    };
    CardListComponent.prototype.onCardDrop = function () {
        __WEBPACK_IMPORTED_MODULE_4_lodash__["forEach"](this.cardSet.cards, function (c, i) { return c.order = i; });
        this._cardService.updateMany(this.cardSet.cards)
            .subscribe(function () {
        });
    };
    CardListComponent.prototype.onSubmit = function (f) {
        var _this = this;
        if (f.valid) {
            var card = new __WEBPACK_IMPORTED_MODULE_1__card__["a" /* Card */]();
            card.cardSet = this.cardSet._id;
            this._cardService.create(card)
                .subscribe(function (card) {
                _this.onCardCreated.emit(card);
                _this.editCard(card);
            });
        }
    };
    CardListComponent.prototype.onEditClick = function (card) {
        this.editCard(card);
    };
    CardListComponent.prototype.onRemoveClick = function (card) {
        var _this = this;
        var cardName = __WEBPACK_IMPORTED_MODULE_4_lodash__["truncate"](card.name, {
            'separator': ' ',
            'omission': ' [...]'
        });
        var msg = 'Are you sure you want to remove card &quot;' + cardName + '&quot; ?';
        this._popupService.open(msg)
            .subscribe(function (confirm) {
            if (confirm)
                _this.removeCard(card);
        });
    };
    CardListComponent.prototype.editCard = function (card) {
        if (card) {
            this._router.navigate(['/card/edit', card._id]);
        }
    };
    CardListComponent.prototype.removeCard = function (card) {
        var _this = this;
        this._cardService.delete(card)
            .subscribe(function () { return _this.onCardRemoved.emit(card); });
    };
    return CardListComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_5__card_set__["a" /* CardSet */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__card_set__["a" /* CardSet */]) === "function" && _a || Object)
], CardListComponent.prototype, "cardSet", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _b || Object)
], CardListComponent.prototype, "onCardCreated", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _c || Object)
], CardListComponent.prototype, "onCardRemoved", void 0);
CardListComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-card-list',
        template: __webpack_require__("../../../../../src/app/card/card-set-edit/card-list/card-list.component.html"),
        styles: [__webpack_require__("../../../../../src/app/card/card-set-edit/card-list/card-list.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__card_service__["a" /* CardService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__card_service__["a" /* CardService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_6__shared_popup_service__["a" /* PopupService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__shared_popup_service__["a" /* PopupService */]) === "function" && _f || Object])
], CardListComponent);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=card-list.component.js.map

/***/ }),

/***/ "../../../../../src/app/card/card-set-edit/card-set-edit.component.html":
/***/ (function(module, exports) {

module.exports = "<app-title-bar [title]=\"'Cardset edit'\" [backEnabled]=\"true\" (back)=\"onBackClick()\"></app-title-bar>\n\n<form #f=\"ngForm\" (ngSubmit)=\"onSubmit(f)\" novalidate>\n  <app-input [label]=\"'Name'\" [name]=\"'name'\" [(model)]=\"_cardSet.name\" ngDefaultControl></app-input>\n  <app-tag-list\n    [list]=\"_cardSet.tags\"\n    (onListChange)=\"onTagsChange($event)\"\n  ></app-tag-list>\n  <app-card-list\n    [cardSet]=\"_cardSet\"\n    (onCardCreated)=\"onCardCreated($event)\"\n    (onCardRemoved)=\"onCardRemoved($event)\"\n  ></app-card-list>\n  <app-button [type]=\"'submit'\" [icon]=\"'check'\" [caption]=\"'Save'\"></app-button>\n  <app-button (click)=\"onCancelClick();\" [icon]=\"'x'\" [caption]=\"'Cancel'\"></app-button>\n</form>\n"

/***/ }),

/***/ "../../../../../src/app/card/card-set-edit/card-set-edit.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: stretch;\n      -ms-flex-align: stretch;\n          align-items: stretch;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  padding: 5.2em 2em 2em; }\n  :host.flex-end {\n    -webkit-box-pack: end;\n        -ms-flex-pack: end;\n            justify-content: flex-end; }\n  :host > * {\n    -webkit-box-flex: 1;\n        -ms-flex: 1 1 auto;\n            flex: 1 1 auto; }\n    :host > *.no-flex {\n      -webkit-box-flex: 0;\n          -ms-flex: none;\n              flex: none; }\n\napp-card-list {\n  margin: 1.4em 0; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/card/card-set-edit/card-set-edit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_api_service__ = __webpack_require__("../../../../../src/app/shared/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__card_set_service__ = __webpack_require__("../../../../../src/app/card/card-set.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_lodash__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardSetEditComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CardSetEditComponent = (function () {
    function CardSetEditComponent(_apiService, _cardSetService, _route, _router) {
        this._apiService = _apiService;
        this._cardSetService = _cardSetService;
        this._route = _route;
        this._router = _router;
    }
    CardSetEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route.data
            .subscribe(function (data) {
            _this._cardSet = data.card;
        });
    };
    CardSetEditComponent.prototype.onCardCreated = function (card) {
        var _this = this;
        this._cardSet.cards.push(card);
        this._cardSetService.update(this._cardSet)
            .subscribe(function (cardSet) { return _this._cardSet = cardSet; });
    };
    CardSetEditComponent.prototype.onCardRemoved = function (card) {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_4_lodash__["remove"](this._cardSet.cards, function (c) { return c._id == card._id; });
        this._cardSetService.update(this._cardSet)
            .subscribe(function (cardSet) { return _this._cardSet = cardSet; });
    };
    CardSetEditComponent.prototype.onTagsChange = function (tags) {
        var _this = this;
        this._cardSet.tags = tags;
        this._cardSetService.update(this._cardSet)
            .subscribe(function (cardSet) { return _this._cardSet = cardSet; });
    };
    CardSetEditComponent.prototype.onSubmit = function (f) {
        var _this = this;
        if (f.valid) {
            this._cardSetService.update(this._cardSet)
                .subscribe(function () { return _this.backToList(); });
        }
    };
    CardSetEditComponent.prototype.onBackClick = function () {
        this.backToList();
    };
    CardSetEditComponent.prototype.onCancelClick = function () {
        this.backToList();
    };
    CardSetEditComponent.prototype.backToList = function () {
        this._router.navigate(['/card-set/list']);
    };
    return CardSetEditComponent;
}());
CardSetEditComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-card-set-edit',
        template: __webpack_require__("../../../../../src/app/card/card-set-edit/card-set-edit.component.html"),
        styles: [__webpack_require__("../../../../../src/app/card/card-set-edit/card-set-edit.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__shared_api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__shared_api_service__["a" /* ApiService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__card_set_service__["a" /* CardSetService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__card_set_service__["a" /* CardSetService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* ActivatedRoute */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */]) === "function" && _d || Object])
], CardSetEditComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=card-set-edit.component.js.map

/***/ }),

/***/ "../../../../../src/app/card/card-set-list/card-set-list.component.html":
/***/ (function(module, exports) {

module.exports = "<app-title-bar [title]=\"'Cardsets'\">\n  <form #f=\"ngForm\" (ngSubmit)=\"onSubmit(f)\" novalidate>\n    <input type=\"text\" name=\"name\" placeholder=\"card set name\" [(ngModel)]=\"_newCardSet.name\" autocomplete=\"new-password\" class=\"small\">\n    <app-button [type]=\"'submit'\" [icon]=\"'plus'\" [caption]=\"'Create'\"></app-button>\n  </form>\n</app-title-bar>\n\n<table class=\"list\">\n  <thead>\n  <tr>\n    <th class=\"preview\">&nbsp;</th>\n    <th class=\"name\">Name</th>\n    <th class=\"tags\">Tags <input type=\"text\" class=\"small\" [(ngModel)]=\"_tagFilter\" (keyup)=\"onTagFilterKey()\" placeholder=\"filter\"></th>\n    <th class=\"date\">Created</th>\n    <th class=\"date\">Modified</th>\n    <th class=\"actions\">Actions</th>\n  </tr>\n  </thead>\n  <tbody *ngIf=\"_filteredCardSets\">\n  <tr *ngFor=\"let cardSet of _filteredCardSets\">\n    <td class=\"preview\">\n      <app-card-preview [cardSet]=\"cardSet\" [size]=\"'small'\"></app-card-preview>\n    </td>\n    <td class=\"name\">{{cardSet.name}}</td>\n    <td class=\"tags\">\n      <span *ngFor=\"let tag of cardSet.tags\">{{tag}} </span>\n    </td>\n    <td class=\"date\">\n      <app-info-badge [time]=\"cardSet.created\" [user]=\"cardSet.createdBy\"></app-info-badge>\n    </td>\n    <td class=\"date\">\n      <app-info-badge [time]=\"cardSet.modified\" [user]=\"cardSet.modifiedBy\"></app-info-badge>\n    </td>\n    <td class=\"actions\">\n      <app-button (click)=\"onEditClick(cardSet);\" [icon]=\"'pencil'\" [caption]=\"'Edit'\"></app-button>\n      <app-button (click)=\"onRemoveClick(cardSet);\" [icon]=\"'x'\" [caption]=\"'Remove'\"></app-button>\n    </td>\n  </tr>\n  </tbody>\n</table>\n"

/***/ }),

/***/ "../../../../../src/app/card/card-set-list/card-set-list.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: stretch;\n      -ms-flex-align: stretch;\n          align-items: stretch;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  padding: 5.2em 2em 2em; }\n  :host.flex-end {\n    -webkit-box-pack: end;\n        -ms-flex-pack: end;\n            justify-content: flex-end; }\n  :host > * {\n    -webkit-box-flex: 1;\n        -ms-flex: 1 1 auto;\n            flex: 1 1 auto; }\n    :host > *.no-flex {\n      -webkit-box-flex: 0;\n          -ms-flex: none;\n              flex: none; }\n\n.list {\n  width: 100%;\n  table-layout: fixed;\n  background: #31343d; }\n  .list th, .list td {\n    padding: 0.4em;\n    text-align: left; }\n  .list th {\n    background: #363943;\n    height: 41px; }\n  .list th.date, .list td.date {\n    width: 158px;\n    white-space: nowrap; }\n  .list th.actions, .list td.actions {\n    width: 220px;\n    white-space: nowrap; }\n  .list th.preview, .list td.preview {\n    width: 138px; }\n  .list th.name, .list td.name {\n    width: 310px;\n    min-width: 200px;\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis; }\n  .list th.tags, .list td.tags {\n    min-width: 310px;\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/card/card-set-list/card-set-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__card_set__ = __webpack_require__("../../../../../src/app/card/card-set.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__card_set_service__ = __webpack_require__("../../../../../src/app/card/card-set.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_popup_service__ = __webpack_require__("../../../../../src/app/shared/popup.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardSetListComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CardSetListComponent = (function () {
    function CardSetListComponent(_cardSetService, _router, _popupService) {
        this._cardSetService = _cardSetService;
        this._router = _router;
        this._popupService = _popupService;
    }
    CardSetListComponent.prototype.ngOnInit = function () {
        this.refresh();
        this._newCardSet = new __WEBPACK_IMPORTED_MODULE_1__card_set__["a" /* CardSet */]();
        this._tagFilter = '';
    };
    CardSetListComponent.prototype.refresh = function () {
        var _this = this;
        this._cardSetService.list()
            .subscribe(function (cardSets) {
            _this._cardSets = cardSets;
            _this.filter();
        });
    };
    CardSetListComponent.prototype.onSubmit = function (f) {
        var _this = this;
        if (f.valid) {
            this._cardSetService.create(this._newCardSet)
                .subscribe(function () {
                _this._newCardSet = new __WEBPACK_IMPORTED_MODULE_1__card_set__["a" /* CardSet */]();
                _this.refresh();
            });
        }
    };
    CardSetListComponent.prototype.onEditClick = function (cardSet) {
        this._router.navigate(['/card-set/edit', cardSet._id]);
    };
    CardSetListComponent.prototype.onRemoveClick = function (cardSet) {
        var _this = this;
        var cardSetName = __WEBPACK_IMPORTED_MODULE_4_lodash__["truncate"](cardSet.name, {
            'separator': ' ',
            'omission': ' [...]'
        });
        var msg = 'Are you sure you want to remove cardset &quot;' + cardSetName + '&quot; ?';
        this._popupService.open(msg)
            .subscribe(function (confirm) {
            if (confirm)
                _this.removeCardSet(cardSet);
        });
    };
    CardSetListComponent.prototype.removeCardSet = function (cardSet) {
        var _this = this;
        this._cardSetService.delete(cardSet)
            .subscribe(function () {
            _this.refresh();
        });
    };
    CardSetListComponent.prototype.onTagFilterKey = function () {
        this.filter();
    };
    CardSetListComponent.prototype.filter = function () {
        var _this = this;
        this._filteredCardSets = __WEBPACK_IMPORTED_MODULE_4_lodash__["filter"](this._cardSets, function (cardSet) {
            if (_this._tagFilter.length == 0)
                return true;
            return __WEBPACK_IMPORTED_MODULE_4_lodash__["some"](cardSet.tags, function (tag) { return __WEBPACK_IMPORTED_MODULE_4_lodash__["includes"](tag, _this._tagFilter); });
        });
    };
    return CardSetListComponent;
}());
CardSetListComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-card-set-list',
        template: __webpack_require__("../../../../../src/app/card/card-set-list/card-set-list.component.html"),
        styles: [__webpack_require__("../../../../../src/app/card/card-set-list/card-set-list.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__card_set_service__["a" /* CardSetService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__card_set_service__["a" /* CardSetService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_5__shared_popup_service__["a" /* PopupService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__shared_popup_service__["a" /* PopupService */]) === "function" && _c || Object])
], CardSetListComponent);

var _a, _b, _c;
//# sourceMappingURL=card-set-list.component.js.map

/***/ }),

/***/ "../../../../../src/app/card/card-set-resolver.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__card_set_service__ = __webpack_require__("../../../../../src/app/card/card-set.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardSetResolver; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CardSetResolver = (function () {
    function CardSetResolver(_cardSetService, _router) {
        this._cardSetService = _cardSetService;
        this._router = _router;
    }
    CardSetResolver.prototype.resolve = function (route, state) {
        var _this = this;
        var id = route.params['id'];
        return this._cardSetService.get(id)
            .do(function (cardSet) {
            if (!cardSet) {
                _this._router.navigate(['/card-set/list']);
            }
        });
    };
    return CardSetResolver;
}());
CardSetResolver = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__card_set_service__["a" /* CardSetService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__card_set_service__["a" /* CardSetService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _b || Object])
], CardSetResolver);

var _a, _b;
//# sourceMappingURL=card-set-resolver.service.js.map

/***/ }),

/***/ "../../../../../src/app/card/card-set.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_api_service__ = __webpack_require__("../../../../../src/app/shared/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardSetService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CardSetService = (function () {
    function CardSetService(_apiService) {
        this._apiService = _apiService;
    }
    CardSetService.prototype.get = function (id) {
        return this._apiService.makeRequest('card-sets/' + id, __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* RequestMethod */].Get);
    };
    CardSetService.prototype.create = function (cardSet) {
        return this._apiService.makeRequest('card-sets', __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* RequestMethod */].Post, null, cardSet);
    };
    CardSetService.prototype.update = function (cardSet) {
        return this._apiService.makeRequest('card-sets/' + cardSet._id, __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* RequestMethod */].Put, null, cardSet);
    };
    CardSetService.prototype.delete = function (cardSet) {
        return this._apiService.makeRequest('card-sets/' + cardSet._id, __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* RequestMethod */].Delete, null, cardSet);
    };
    CardSetService.prototype.list = function () {
        return this._apiService.makeRequest('card-sets', __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* RequestMethod */].Get);
    };
    return CardSetService;
}());
CardSetService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__shared_api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__shared_api_service__["a" /* ApiService */]) === "function" && _a || Object])
], CardSetService);

var _a;
//# sourceMappingURL=card-set.service.js.map

/***/ }),

/***/ "../../../../../src/app/card/card-set.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardSet; });
var CardSet = (function () {
    function CardSet() {
    }
    return CardSet;
}());

//# sourceMappingURL=card-set.js.map

/***/ }),

/***/ "../../../../../src/app/card/card.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__card_set_edit_card_list_card_list_component__ = __webpack_require__("../../../../../src/app/card/card-set-edit/card-list/card-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__routes__ = __webpack_require__("../../../../../src/app/card/routes.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_core_module__ = __webpack_require__("../../../../../src/app/core/core.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__card_service__ = __webpack_require__("../../../../../src/app/card/card.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__card_resolver_service__ = __webpack_require__("../../../../../src/app/card/card-resolver.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__card_edit_card_edit_component__ = __webpack_require__("../../../../../src/app/card/card-edit/card-edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__card_edit_image_addition_image_addition_component__ = __webpack_require__("../../../../../src/app/card/card-edit/image-addition/image-addition.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__card_edit_canvas_canvas_component__ = __webpack_require__("../../../../../src/app/card/card-edit/canvas/canvas.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__preview_service__ = __webpack_require__("../../../../../src/app/card/preview.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__card_preview_card_preview_component__ = __webpack_require__("../../../../../src/app/card/card-preview/card-preview.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__card_edit_canvas_fabric_text_service__ = __webpack_require__("../../../../../src/app/card/card-edit/canvas/fabric-text.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__card_set_service__ = __webpack_require__("../../../../../src/app/card/card-set.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__card_set_resolver_service__ = __webpack_require__("../../../../../src/app/card/card-set-resolver.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__card_set_list_card_set_list_component__ = __webpack_require__("../../../../../src/app/card/card-set-list/card-set-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__card_set_edit_card_set_edit_component__ = __webpack_require__("../../../../../src/app/card/card-set-edit/card-set-edit.component.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





















var CardModule = (function () {
    function CardModule() {
    }
    return CardModule;
}());
CardModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_4__routes__["a" /* routes */]),
            __WEBPACK_IMPORTED_MODULE_6__shared_shared_module__["a" /* SharedModule */],
            __WEBPACK_IMPORTED_MODULE_7__core_core_module__["a" /* CoreModule */],
            __WEBPACK_IMPORTED_MODULE_11_ng2_file_upload__["FileUploadModule"]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_8__card_service__["a" /* CardService */],
            __WEBPACK_IMPORTED_MODULE_9__card_resolver_service__["a" /* CardResolver */],
            __WEBPACK_IMPORTED_MODULE_17__card_set_service__["a" /* CardSetService */],
            __WEBPACK_IMPORTED_MODULE_18__card_set_resolver_service__["a" /* CardSetResolver */],
            __WEBPACK_IMPORTED_MODULE_14__preview_service__["a" /* PreviewService */],
            __WEBPACK_IMPORTED_MODULE_16__card_edit_canvas_fabric_text_service__["a" /* FabricTextService */]
        ],
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__card_set_edit_card_list_card_list_component__["a" /* CardListComponent */],
            __WEBPACK_IMPORTED_MODULE_10__card_edit_card_edit_component__["a" /* CardEditComponent */],
            __WEBPACK_IMPORTED_MODULE_12__card_edit_image_addition_image_addition_component__["a" /* ImageAdditionComponent */],
            __WEBPACK_IMPORTED_MODULE_13__card_edit_canvas_canvas_component__["a" /* CanvasComponent */],
            __WEBPACK_IMPORTED_MODULE_15__card_preview_card_preview_component__["a" /* CardPreviewComponent */],
            __WEBPACK_IMPORTED_MODULE_19__card_set_list_card_set_list_component__["a" /* CardSetListComponent */],
            __WEBPACK_IMPORTED_MODULE_20__card_set_edit_card_set_edit_component__["a" /* CardSetEditComponent */]
        ]
    })
], CardModule);

//# sourceMappingURL=card.module.js.map

/***/ }),

/***/ "../../../../../src/app/card/card.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_api_service__ = __webpack_require__("../../../../../src/app/shared/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CardService = (function () {
    function CardService(_apiService) {
        this._apiService = _apiService;
    }
    CardService.prototype.get = function (id) {
        return this._apiService.makeRequest('cards/' + id, __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* RequestMethod */].Get);
    };
    CardService.prototype.create = function (card) {
        return this._apiService.makeRequest('cards', __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* RequestMethod */].Post, null, card);
    };
    CardService.prototype.update = function (card) {
        return this._apiService.makeRequest('cards/' + card._id, __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* RequestMethod */].Put, null, card);
    };
    CardService.prototype.updateMany = function (cards) {
        return this._apiService.makeRequest('cards', __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* RequestMethod */].Put, null, cards);
    };
    CardService.prototype.delete = function (card) {
        return this._apiService.makeRequest('cards/' + card._id, __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* RequestMethod */].Delete, null, card);
    };
    CardService.prototype.list = function () {
        return this._apiService.makeRequest('cards', __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* RequestMethod */].Get);
    };
    CardService.prototype.scrap = function (info) {
        return this._apiService.makeRequest('cards/scrap', __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* RequestMethod */].Post, null, info);
    };
    return CardService;
}());
CardService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__shared_api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__shared_api_service__["a" /* ApiService */]) === "function" && _a || Object])
], CardService);

var _a;
//# sourceMappingURL=card.service.js.map

/***/ }),

/***/ "../../../../../src/app/card/card.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Card; });
var Card = (function () {
    function Card() {
    }
    return Card;
}());

//# sourceMappingURL=card.js.map

/***/ }),

/***/ "../../../../../src/app/card/image.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Image; });
var Image = (function () {
    function Image() {
    }
    return Image;
}());

//# sourceMappingURL=image.js.map

/***/ }),

/***/ "../../../../../src/app/card/preview.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_api_service__ = __webpack_require__("../../../../../src/app/shared/api.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PreviewService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PreviewService = (function () {
    function PreviewService(_apiService, _ngZone) {
        this._apiService = _apiService;
        this._ngZone = _ngZone;
        this._previewUploadOptions = {
            url: this._apiService.URL + 'cards/upload',
            autoUpload: true,
            removeAfterUpload: true,
            additionalParameter: {
                id: null
            }
        };
    }
    PreviewService.prototype.save = function (dataUrl, card) {
        var _this = this;
        return this.dataUrlToFile(dataUrl)
            .then(function (file) { return _this.upload(file, card); });
    };
    PreviewService.prototype.dataUrlToFile = function (dataUrl) {
        return fetch(dataUrl)
            .then(function (response) { return response.arrayBuffer(); })
            .then(function (buffer) { return new File([buffer], 'preview.png', { type: 'image/png' }); });
    };
    PreviewService.prototype.upload = function (file, card) {
        var _this = this;
        return new Promise(function (resolve) {
            _this._previewUploadOptions.additionalParameter.id = card._id;
            var uploader = new __WEBPACK_IMPORTED_MODULE_1_ng2_file_upload__["FileUploader"](_this._previewUploadOptions);
            uploader.onCompleteItem = function (item, responseString) {
                var response = JSON.parse(responseString);
                _this._ngZone.run(function () { return resolve(response.fileName); });
            };
            uploader.addToQueue([file]);
        });
    };
    return PreviewService;
}());
PreviewService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__shared_api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__shared_api_service__["a" /* ApiService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"]) === "function" && _b || Object])
], PreviewService);

var _a, _b;
//# sourceMappingURL=preview.service.js.map

/***/ }),

/***/ "../../../../../src/app/card/routes.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__user_auth_guard_service__ = __webpack_require__("../../../../../src/app/user/auth-guard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__card_resolver_service__ = __webpack_require__("../../../../../src/app/card/card-resolver.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__card_edit_card_edit_component__ = __webpack_require__("../../../../../src/app/card/card-edit/card-edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__card_set_list_card_set_list_component__ = __webpack_require__("../../../../../src/app/card/card-set-list/card-set-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__card_set_edit_card_set_edit_component__ = __webpack_require__("../../../../../src/app/card/card-set-edit/card-set-edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__card_set_resolver_service__ = __webpack_require__("../../../../../src/app/card/card-set-resolver.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routes; });






var routes = [
    {
        path: 'card-set',
        canActivate: [__WEBPACK_IMPORTED_MODULE_0__user_auth_guard_service__["a" /* AuthGuard */]],
        children: [
            {
                path: 'list',
                component: __WEBPACK_IMPORTED_MODULE_3__card_set_list_card_set_list_component__["a" /* CardSetListComponent */]
            },
            {
                path: 'edit/:id',
                component: __WEBPACK_IMPORTED_MODULE_4__card_set_edit_card_set_edit_component__["a" /* CardSetEditComponent */],
                resolve: {
                    card: __WEBPACK_IMPORTED_MODULE_5__card_set_resolver_service__["a" /* CardSetResolver */]
                }
            }
        ]
    },
    {
        path: 'card',
        canActivate: [__WEBPACK_IMPORTED_MODULE_0__user_auth_guard_service__["a" /* AuthGuard */]],
        children: [
            {
                path: 'edit/:id',
                component: __WEBPACK_IMPORTED_MODULE_2__card_edit_card_edit_component__["a" /* CardEditComponent */],
                resolve: {
                    card: __WEBPACK_IMPORTED_MODULE_1__card_resolver_service__["a" /* CardResolver */]
                }
            }
        ]
    }
];
//# sourceMappingURL=routes.js.map

/***/ }),

/***/ "../../../../../src/app/core/core.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__top_bar_top_bar_component__ = __webpack_require__("../../../../../src/app/core/top-bar/top-bar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__icon_set_icon_set_component__ = __webpack_require__("../../../../../src/app/core/icon-set/icon-set.component.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CoreModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var CoreModule = (function () {
    function CoreModule() {
    }
    return CoreModule;
}());
CoreModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* RouterModule */]
        ],
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__top_bar_top_bar_component__["a" /* TopBarComponent */],
            __WEBPACK_IMPORTED_MODULE_4__icon_set_icon_set_component__["a" /* IconSetComponent */]
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_3__top_bar_top_bar_component__["a" /* TopBarComponent */],
            __WEBPACK_IMPORTED_MODULE_4__icon_set_icon_set_component__["a" /* IconSetComponent */]
        ]
    })
], CoreModule);

//# sourceMappingURL=core.module.js.map

/***/ }),

/***/ "../../../../../src/app/core/icon-set/icon-set.component.html":
/***/ (function(module, exports) {

module.exports = "<svg xmlns=\"http://www.w3.org/2000/svg\">\n  <symbol viewBox=\"0 0 16 16\" id=\"alert\">\n    <path fill-rule=\"evenodd\"\n          d=\"M8.865 1.52c-.18-.31-.51-.5-.87-.5s-.69.19-.87.5L.275 13.5c-.18.31-.18.69 0 1 .19.31.52.5.87.5h13.7c.36 0 .69-.19.86-.5.17-.31.18-.69.01-1L8.865 1.52zM8.995 13h-2v-2h2v2zm0-3h-2V6h2v4z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 10 16\" id=\"arrow-down\">\n    <path fill-rule=\"evenodd\" d=\"M7 7V3H3v4H0l5 6 5-6z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 10 16\" id=\"arrow-left\">\n    <path fill-rule=\"evenodd\" d=\"M6 3L0 8l6 5v-3h4V6H6z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 10 16\" id=\"arrow-right\">\n    <path fill-rule=\"evenodd\" d=\"M10 8L4 3v3H0v4h4v3z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 6 16\" id=\"arrow-small-down\">\n    <path fill-rule=\"evenodd\" d=\"M4 7V5H2v2H0l3 4 3-4z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 6 16\" id=\"arrow-small-left\">\n    <path fill-rule=\"evenodd\" d=\"M4 7V5L0 8l4 3V9h2V7z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 6 16\" id=\"arrow-small-right\">\n    <path fill-rule=\"evenodd\" d=\"M6 8L2 5v2H0v2h2v2z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 6 16\" id=\"arrow-small-up\">\n    <path fill-rule=\"evenodd\" d=\"M3 5L0 9h2v2h2V9h2z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 10 16\" id=\"arrow-up\">\n    <path fill-rule=\"evenodd\" d=\"M5 3L0 9h3v4h4V9h3z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"beaker\">\n    <path fill-rule=\"evenodd\"\n          d=\"M14.38 14.59L11 7V3h1V2H3v1h1v4L.63 14.59A1 1 0 0 0 1.54 16h11.94c.72 0 1.2-.75.91-1.41h-.01zM3.75 10L5 7V3h5v4l1.25 3h-7.5zM8 8h1v1H8V8zM7 7H6V6h1v1zm0-3h1v1H7V4zm0-3H6V0h1v1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"bell\">\n    <path fill-rule=\"evenodd\"\n          d=\"M14 12v1H0v-1l.73-.58c.77-.77.81-2.55 1.19-4.42C2.69 3.23 6 2 6 2c0-.55.45-1 1-1s1 .45 1 1c0 0 3.39 1.23 4.16 5 .38 1.88.42 3.66 1.19 4.42l.66.58H14zm-7 4c1.11 0 2-.89 2-2H5c0 1.11.89 2 2 2z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 10 16\" id=\"bold\">\n    <path fill-rule=\"evenodd\"\n          d=\"M1 2h3.83c2.48 0 4.3.75 4.3 2.95 0 1.14-.63 2.23-1.67 2.61v.06c1.33.3 2.3 1.23 2.3 2.86 0 2.39-1.97 3.52-4.61 3.52H1V2zm3.66 4.95c1.67 0 2.38-.66 2.38-1.69 0-1.17-.78-1.61-2.34-1.61H3.13v3.3h1.53zm.27 5.39c1.77 0 2.75-.64 2.75-1.98 0-1.27-.95-1.81-2.75-1.81h-1.8v3.8h1.8v-.01z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"book\">\n    <path fill-rule=\"evenodd\"\n          d=\"M3 5h4v1H3V5zm0 3h4V7H3v1zm0 2h4V9H3v1zm11-5h-4v1h4V5zm0 2h-4v1h4V7zm0 2h-4v1h4V9zm2-6v9c0 .55-.45 1-1 1H9.5l-1 1-1-1H2c-.55 0-1-.45-1-1V3c0-.55.45-1 1-1h5.5l1 1 1-1H15c.55 0 1 .45 1 1zm-8 .5L7.5 3H2v9h6V3.5zm7-.5H9.5l-.5.5V12h6V3z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 10 16\" id=\"bookmark\">\n    <path fill-rule=\"evenodd\"\n          d=\"M9 0H1C.27 0 0 .27 0 1v15l5-3.09L10 16V1c0-.73-.27-1-1-1zm-.78 4.25L6.36 5.61l.72 2.16c.06.22-.02.28-.2.17L5 6.6 3.12 7.94c-.19.11-.25.05-.2-.17l.72-2.16-1.86-1.36c-.17-.16-.14-.23.09-.23l2.3-.03.7-2.16h.25l.7 2.16 2.3.03c.23 0 .27.08.09.23h.01z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"briefcase\">\n    <path fill-rule=\"evenodd\"\n          d=\"M9 4V3c0-.55-.45-1-1-1H6c-.55 0-1 .45-1 1v1H1c-.55 0-1 .45-1 1v8c0 .55.45 1 1 1h12c.55 0 1-.45 1-1V5c0-.55-.45-1-1-1H9zM6 3h2v1H6V3zm7 6H8v1H6V9H1V5h1v3h10V5h1v4z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"broadcast\">\n    <path fill-rule=\"evenodd\"\n          d=\"M9 9H8c.55 0 1-.45 1-1V7c0-.55-.45-1-1-1H7c-.55 0-1 .45-1 1v1c0 .55.45 1 1 1H6c-.55 0-1 .45-1 1v2h1v3c0 .55.45 1 1 1h1c.55 0 1-.45 1-1v-3h1v-2c0-.55-.45-1-1-1zM7 7h1v1H7V7zm2 4H8v4H7v-4H6v-1h3v1zm2.09-3.5c0-1.98-1.61-3.59-3.59-3.59A3.593 3.593 0 0 0 4 8.31v1.98c-.61-.77-1-1.73-1-2.8 0-2.48 2.02-4.5 4.5-4.5S12 5.01 12 7.49c0 1.06-.39 2.03-1 2.8V8.31c.06-.27.09-.53.09-.81zm3.91 0c0 2.88-1.63 5.38-4 6.63v-1.05a6.553 6.553 0 0 0 3.09-5.58A6.59 6.59 0 0 0 7.5.91 6.59 6.59 0 0 0 .91 7.5c0 2.36 1.23 4.42 3.09 5.58v1.05A7.497 7.497 0 0 1 7.5 0C11.64 0 15 3.36 15 7.5z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"browser\">\n    <path fill-rule=\"evenodd\"\n          d=\"M5 3h1v1H5V3zM3 3h1v1H3V3zM1 3h1v1H1V3zm12 10H1V5h12v8zm0-9H7V3h6v1zm1-1c0-.55-.45-1-1-1H1c-.55 0-1 .45-1 1v10c0 .55.45 1 1 1h12c.55 0 1-.45 1-1V3z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"bug\">\n    <path fill-rule=\"evenodd\"\n          d=\"M11 10h3V9h-3V8l3.17-1.03-.34-.94L11 7V6c0-.55-.45-1-1-1V4c0-.48-.36-.88-.83-.97L10.2 2H12V1H9.8l-2 2h-.59L5.2 1H3v1h1.8l1.03 1.03C5.36 3.12 5 3.51 5 4v1c-.55 0-1 .45-1 1v1l-2.83-.97-.34.94L4 8v1H1v1h3v1L.83 12.03l.34.94L4 12v1c0 .55.45 1 1 1h1l1-1V6h1v7l1 1h1c.55 0 1-.45 1-1v-1l2.83.97.34-.94L11 11v-1zM9 5H6V4h3v1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"calendar\">\n    <path fill-rule=\"evenodd\"\n          d=\"M13 2h-1v1.5c0 .28-.22.5-.5.5h-2c-.28 0-.5-.22-.5-.5V2H6v1.5c0 .28-.22.5-.5.5h-2c-.28 0-.5-.22-.5-.5V2H2c-.55 0-1 .45-1 1v11c0 .55.45 1 1 1h11c.55 0 1-.45 1-1V3c0-.55-.45-1-1-1zm0 12H2V5h11v9zM5 3H4V1h1v2zm6 0h-1V1h1v2zM6 7H5V6h1v1zm2 0H7V6h1v1zm2 0H9V6h1v1zm2 0h-1V6h1v1zM4 9H3V8h1v1zm2 0H5V8h1v1zm2 0H7V8h1v1zm2 0H9V8h1v1zm2 0h-1V8h1v1zm-8 2H3v-1h1v1zm2 0H5v-1h1v1zm2 0H7v-1h1v1zm2 0H9v-1h1v1zm2 0h-1v-1h1v1zm-8 2H3v-1h1v1zm2 0H5v-1h1v1zm2 0H7v-1h1v1zm2 0H9v-1h1v1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"check\">\n    <path fill-rule=\"evenodd\" d=\"M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"checklist\">\n    <path fill-rule=\"evenodd\"\n          d=\"M16 8.5l-6 6-3-3L8.5 10l1.5 1.5L14.5 7 16 8.5zM5.7 12.2l.8.8H2c-.55 0-1-.45-1-1V3c0-.55.45-1 1-1h7c.55 0 1 .45 1 1v6.5l-.8-.8c-.39-.39-1.03-.39-1.42 0L5.7 10.8a.996.996 0 0 0 0 1.41v-.01zM4 4h5V3H4v1zm0 2h5V5H4v1zm0 2h3V7H4v1zM3 9H2v1h1V9zm0-2H2v1h1V7zm0-2H2v1h1V5zm0-2H2v1h1V3z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 10 16\" id=\"chevron-down\">\n    <path fill-rule=\"evenodd\" d=\"M5 11L0 6l1.5-1.5L5 8.25 8.5 4.5 10 6z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 8 16\" id=\"chevron-left\">\n    <path fill-rule=\"evenodd\" d=\"M5.5 3L7 4.5 3.25 8 7 11.5 5.5 13l-5-5z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 8 16\" id=\"chevron-right\">\n    <path fill-rule=\"evenodd\" d=\"M7.5 8l-5 5L1 11.5 4.75 8 1 4.5 2.5 3z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 10 16\" id=\"chevron-up\">\n    <path fill-rule=\"evenodd\" d=\"M10 10l-1.5 1.5L5 7.75 1.5 11.5 0 10l5-5z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"circle-slash\">\n    <path fill-rule=\"evenodd\"\n          d=\"M7 1C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7-3.14-7-7-7zm0 1.3c1.3 0 2.5.44 3.47 1.17l-8 8A5.755 5.755 0 0 1 1.3 8c0-3.14 2.56-5.7 5.7-5.7zm0 11.41c-1.3 0-2.5-.44-3.47-1.17l8-8c.73.97 1.17 2.17 1.17 3.47 0 3.14-2.56 5.7-5.7 5.7z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"circuit-board\">\n    <path fill-rule=\"evenodd\"\n          d=\"M3 5c0-.55.45-1 1-1s1 .45 1 1-.45 1-1 1-1-.45-1-1zm8 0c0-.55-.45-1-1-1s-1 .45-1 1 .45 1 1 1 1-.45 1-1zm0 6c0-.55-.45-1-1-1s-1 .45-1 1 .45 1 1 1 1-.45 1-1zm2-10H5v2.17c.36.19.64.47.83.83h2.34c.42-.78 1.33-1.28 2.34-1.05.75.19 1.36.8 1.53 1.55.31 1.38-.72 2.59-2.05 2.59-.8 0-1.48-.44-1.83-1.09H5.83c-.42.8-1.33 1.28-2.34 1.03-.73-.17-1.34-.78-1.52-1.52C1.72 4.49 2.2 3.59 3 3.17V1H1c-.55 0-1 .45-1 1v12c0 .55.45 1 1 1l5-5h2.17c.42-.78 1.33-1.28 2.34-1.05.75.19 1.36.8 1.53 1.55.31 1.38-.72 2.59-2.05 2.59-.8 0-1.48-.44-1.83-1.09H6.99L4 15h9c.55 0 1-.45 1-1V2c0-.55-.45-1-1-1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"clippy\">\n    <path fill-rule=\"evenodd\"\n          d=\"M2 13h4v1H2v-1zm5-6H2v1h5V7zm2 3V8l-3 3 3 3v-2h5v-2H9zM4.5 9H2v1h2.5V9zM2 12h2.5v-1H2v1zm9 1h1v2c-.02.28-.11.52-.3.7-.19.18-.42.28-.7.3H1c-.55 0-1-.45-1-1V4c0-.55.45-1 1-1h3c0-1.11.89-2 2-2 1.11 0 2 .89 2 2h3c.55 0 1 .45 1 1v5h-1V6H1v9h10v-2zM2 5h8c0-.55-.45-1-1-1H8c-.55 0-1-.45-1-1s-.45-1-1-1-1 .45-1 1-.45 1-1 1H3c-.55 0-1 .45-1 1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"clock\">\n    <path fill-rule=\"evenodd\"\n          d=\"M8 8h3v2H7c-.55 0-1-.45-1-1V4h2v4zM7 2.3c3.14 0 5.7 2.56 5.7 5.7s-2.56 5.7-5.7 5.7A5.71 5.71 0 0 1 1.3 8c0-3.14 2.56-5.7 5.7-5.7zM7 1C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7-3.14-7-7-7z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"cloud-download\">\n    <path fill-rule=\"evenodd\"\n          d=\"M9 12h2l-3 3-3-3h2V7h2v5zm3-8c0-.44-.91-3-4.5-3C5.08 1 3 2.92 3 5 1.02 5 0 6.52 0 8c0 1.53 1 3 3 3h3V9.7H3C1.38 9.7 1.3 8.28 1.3 8c0-.17.05-1.7 1.7-1.7h1.3V5c0-1.39 1.56-2.7 3.2-2.7 2.55 0 3.13 1.55 3.2 1.8v1.2H12c.81 0 2.7.22 2.7 2.2 0 2.09-2.25 2.2-2.7 2.2h-2V11h2c2.08 0 4-1.16 4-3.5C16 5.06 14.08 4 12 4z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"cloud-upload\">\n    <path fill-rule=\"evenodd\"\n          d=\"M7 9H5l3-3 3 3H9v5H7V9zm5-4c0-.44-.91-3-4.5-3C5.08 2 3 3.92 3 6 1.02 6 0 7.52 0 9c0 1.53 1 3 3 3h3v-1.3H3c-1.62 0-1.7-1.42-1.7-1.7 0-.17.05-1.7 1.7-1.7h1.3V6c0-1.39 1.56-2.7 3.2-2.7 2.55 0 3.13 1.55 3.2 1.8v1.2H12c.81 0 2.7.22 2.7 2.2 0 2.09-2.25 2.2-2.7 2.2h-2V12h2c2.08 0 4-1.16 4-3.5C16 6.06 14.08 5 12 5z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"code\">\n    <path fill-rule=\"evenodd\"\n          d=\"M9.5 3L8 4.5 11.5 8 8 11.5 9.5 13 14 8 9.5 3zm-5 0L0 8l4.5 5L6 11.5 2.5 8 6 4.5 4.5 3z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"comment-discussion\">\n    <path fill-rule=\"evenodd\"\n          d=\"M15 1H6c-.55 0-1 .45-1 1v2H1c-.55 0-1 .45-1 1v6c0 .55.45 1 1 1h1v3l3-3h4c.55 0 1-.45 1-1V9h1l3 3V9h1c.55 0 1-.45 1-1V2c0-.55-.45-1-1-1zM9 11H4.5L3 12.5V11H1V5h4v3c0 .55.45 1 1 1h3v2zm6-3h-2v1.5L11.5 8H6V2h9v6z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"comment\">\n    <path fill-rule=\"evenodd\"\n          d=\"M14 1H2c-.55 0-1 .45-1 1v8c0 .55.45 1 1 1h2v3.5L7.5 11H14c.55 0 1-.45 1-1V2c0-.55-.45-1-1-1zm0 9H7l-2 2v-2H2V2h12v8z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"credit-card\">\n    <path fill-rule=\"evenodd\"\n          d=\"M12 9H2V8h10v1zm4-6v9c0 .55-.45 1-1 1H1c-.55 0-1-.45-1-1V3c0-.55.45-1 1-1h14c.55 0 1 .45 1 1zm-1 3H1v6h14V6zm0-3H1v1h14V3zm-9 7H2v1h4v-1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 8 16\" id=\"dash\">\n    <path fill-rule=\"evenodd\" d=\"M0 7v2h8V7z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"dashboard\">\n    <path fill-rule=\"evenodd\"\n          d=\"M9 5H8V4h1v1zm4 3h-1v1h1V8zM6 5H5v1h1V5zM5 8H4v1h1V8zm11-5.5l-.5-.5L9 7c-.06-.02-1 0-1 0-.55 0-1 .45-1 1v1c0 .55.45 1 1 1h1c.55 0 1-.45 1-1v-.92l6-5.58zm-1.59 4.09c.19.61.3 1.25.3 1.91 0 3.42-2.78 6.2-6.2 6.2-3.42 0-6.21-2.78-6.21-6.2 0-3.42 2.78-6.2 6.2-6.2 1.2 0 2.31.34 3.27.94l.94-.94A7.459 7.459 0 0 0 8.51 1C4.36 1 1 4.36 1 8.5 1 12.64 4.36 16 8.5 16c4.14 0 7.5-3.36 7.5-7.5 0-1.03-.2-2.02-.59-2.91l-1 1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"database\">\n    <path fill-rule=\"evenodd\"\n          d=\"M6 15c-3.31 0-6-.9-6-2v-2c0-.17.09-.34.21-.5.67.86 3 1.5 5.79 1.5s5.12-.64 5.79-1.5c.13.16.21.33.21.5v2c0 1.1-2.69 2-6 2zm0-4c-3.31 0-6-.9-6-2V7c0-.11.04-.21.09-.31.03-.06.07-.13.12-.19C.88 7.36 3.21 8 6 8s5.12-.64 5.79-1.5c.05.06.09.13.12.19.05.1.09.21.09.31v2c0 1.1-2.69 2-6 2zm0-4c-3.31 0-6-.9-6-2V3c0-1.1 2.69-2 6-2s6 .9 6 2v2c0 1.1-2.69 2-6 2zm0-5c-2.21 0-4 .45-4 1s1.79 1 4 1 4-.45 4-1-1.79-1-4-1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"desktop-download\">\n    <path fill-rule=\"evenodd\"\n          d=\"M4 6h3V0h2v6h3l-4 4-4-4zm11-4h-4v1h4v8H1V3h4V2H1c-.55 0-1 .45-1 1v9c0 .55.45 1 1 1h5.34c-.25.61-.86 1.39-2.34 2h8c-1.48-.61-2.09-1.39-2.34-2H15c.55 0 1-.45 1-1V3c0-.55-.45-1-1-1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"device-camera-video\">\n    <path fill-rule=\"evenodd\"\n          d=\"M15.2 2.09L10 5.72V3c0-.55-.45-1-1-1H1c-.55 0-1 .45-1 1v9c0 .55.45 1 1 1h8c.55 0 1-.45 1-1V9.28l5.2 3.63c.33.23.8 0 .8-.41v-10c0-.41-.47-.64-.8-.41z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"device-camera\">\n    <path fill-rule=\"evenodd\"\n          d=\"M15 3H7c0-.55-.45-1-1-1H2c-.55 0-1 .45-1 1-.55 0-1 .45-1 1v9c0 .55.45 1 1 1h14c.55 0 1-.45 1-1V4c0-.55-.45-1-1-1zM6 5H2V4h4v1zm4.5 7C8.56 12 7 10.44 7 8.5S8.56 5 10.5 5 14 6.56 14 8.5 12.44 12 10.5 12zM13 8.5c0 1.38-1.13 2.5-2.5 2.5S8 9.87 8 8.5 9.13 6 10.5 6 13 7.13 13 8.5z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"device-desktop\">\n    <path fill-rule=\"evenodd\"\n          d=\"M15 2H1c-.55 0-1 .45-1 1v9c0 .55.45 1 1 1h5.34c-.25.61-.86 1.39-2.34 2h8c-1.48-.61-2.09-1.39-2.34-2H15c.55 0 1-.45 1-1V3c0-.55-.45-1-1-1zm0 9H1V3h14v8z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 10 16\" id=\"device-mobile\">\n    <path fill-rule=\"evenodd\"\n          d=\"M9 0H1C.45 0 0 .45 0 1v14c0 .55.45 1 1 1h8c.55 0 1-.45 1-1V1c0-.55-.45-1-1-1zM5 15.3c-.72 0-1.3-.58-1.3-1.3 0-.72.58-1.3 1.3-1.3.72 0 1.3.58 1.3 1.3 0 .72-.58 1.3-1.3 1.3zM9 12H1V2h8v10z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"diff-added\">\n    <path fill-rule=\"evenodd\"\n          d=\"M13 1H1c-.55 0-1 .45-1 1v12c0 .55.45 1 1 1h12c.55 0 1-.45 1-1V2c0-.55-.45-1-1-1zm0 13H1V2h12v12zM6 9H3V7h3V4h2v3h3v2H8v3H6V9z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"diff-ignored\">\n    <path fill-rule=\"evenodd\"\n          d=\"M13 1H1c-.55 0-1 .45-1 1v12c0 .55.45 1 1 1h12c.55 0 1-.45 1-1V2c0-.55-.45-1-1-1zm0 13H1V2h12v12zm-8.5-2H3v-1.5L9.5 4H11v1.5L4.5 12z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"diff-modified\">\n    <path fill-rule=\"evenodd\"\n          d=\"M13 1H1c-.55 0-1 .45-1 1v12c0 .55.45 1 1 1h12c.55 0 1-.45 1-1V2c0-.55-.45-1-1-1zm0 13H1V2h12v12zM4 8c0-1.66 1.34-3 3-3s3 1.34 3 3-1.34 3-3 3-3-1.34-3-3z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"diff-removed\">\n    <path fill-rule=\"evenodd\"\n          d=\"M13 1H1c-.55 0-1 .45-1 1v12c0 .55.45 1 1 1h12c.55 0 1-.45 1-1V2c0-.55-.45-1-1-1zm0 13H1V2h12v12zm-2-5H3V7h8v2z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"diff-renamed\">\n    <path fill-rule=\"evenodd\"\n          d=\"M6 9H3V7h3V4l5 4-5 4V9zm8-7v12c0 .55-.45 1-1 1H1c-.55 0-1-.45-1-1V2c0-.55.45-1 1-1h12c.55 0 1 .45 1 1zm-1 0H1v12h12V2z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 13 16\" id=\"diff\">\n    <path fill-rule=\"evenodd\"\n          d=\"M6 7h2v1H6v2H5V8H3V7h2V5h1v2zm-3 6h5v-1H3v1zM7.5 2L11 5.5V15c0 .55-.45 1-1 1H1c-.55 0-1-.45-1-1V3c0-.55.45-1 1-1h6.5zM10 6L7 3H1v12h9V6zM8.5 0H3v1h5l4 4v8h1V4.5L8.5 0z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"ellipses\">\n    <path fill-rule=\"evenodd\"\n          d=\"M11 5H1c-.55 0-1 .45-1 1v4c0 .55.45 1 1 1h10c.55 0 1-.45 1-1V6c0-.55-.45-1-1-1zM4 9H2V7h2v2zm3 0H5V7h2v2zm3 0H8V7h2v2z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"ellipsis\">\n    <path\n      d=\"M11 5H1c-.55 0-1 .45-1 1v4c0 .55.45 1 1 1h10c.55 0 1-.45 1-1V6c0-.55-.45-1-1-1zM4 9H2V7h2v2zm3 0H5V7h2v2zm3 0H8V7h2v2z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"eye\">\n    <path fill-rule=\"evenodd\"\n          d=\"M8.06 2C3 2 0 8 0 8s3 6 8.06 6C13 14 16 8 16 8s-3-6-7.94-6zM8 12c-2.2 0-4-1.78-4-4 0-2.2 1.8-4 4-4 2.22 0 4 1.8 4 4 0 2.22-1.78 4-4 4zm2-4c0 1.11-.89 2-2 2-1.11 0-2-.89-2-2 0-1.11.89-2 2-2 1.11 0 2 .89 2 2z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"file-binary\">\n    <path fill-rule=\"evenodd\"\n          d=\"M4 12h1v1H2v-1h1v-2H2V9h2v3zm8-7.5V14c0 .55-.45 1-1 1H1c-.55 0-1-.45-1-1V2c0-.55.45-1 1-1h7.5L12 4.5zM11 5L8 2H1v12h10V5zM8 4H6v1h1v2H6v1h3V7H8V4zM2 4h3v4H2V4zm1 3h1V5H3v2zm3 2h3v4H6V9zm1 3h1v-2H7v2z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"file-code\">\n    <path fill-rule=\"evenodd\"\n          d=\"M8.5 1H1c-.55 0-1 .45-1 1v12c0 .55.45 1 1 1h10c.55 0 1-.45 1-1V4.5L8.5 1zM11 14H1V2h7l3 3v9zM5 6.98L3.5 8.5 5 10l-.5 1L2 8.5 4.5 6l.5.98zM7.5 6L10 8.5 7.5 11l-.5-.98L8.5 8.5 7 7l.5-1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"file-directory\">\n    <path fill-rule=\"evenodd\"\n          d=\"M13 4H7V3c0-.66-.31-1-1-1H1c-.55 0-1 .45-1 1v10c0 .55.45 1 1 1h12c.55 0 1-.45 1-1V5c0-.55-.45-1-1-1zM6 4H1V3h5v1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"file-media\">\n    <path fill-rule=\"evenodd\"\n          d=\"M6 5h2v2H6V5zm6-.5V14c0 .55-.45 1-1 1H1c-.55 0-1-.45-1-1V2c0-.55.45-1 1-1h7.5L12 4.5zM11 5L8 2H1v11l3-5 2 4 2-2 3 3V5z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"file-pdf\">\n    <path fill-rule=\"evenodd\"\n          d=\"M8.5 1H1a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V4.5L8.5 1zM1 2h4a.68.68 0 0 0-.31.2 1.08 1.08 0 0 0-.23.47 4.22 4.22 0 0 0-.09 1.47c.06.609.173 1.211.34 1.8A21.78 21.78 0 0 1 3.6 8.6c-.5 1-.8 1.66-.91 1.84a7.16 7.16 0 0 0-.69.3 4.19 4.19 0 0 0-1 .64V2zm4.42 4.8a5.65 5.65 0 0 0 1.17 2.09c.275.237.595.417.94.53-.64.09-1.23.2-1.81.33a12.22 12.22 0 0 0-1.81.59c-.587.243.22-.44.61-1.25.365-.74.67-1.51.91-2.3l-.01.01zM11 14H1.5a.74.74 0 0 1-.17 0 2.12 2.12 0 0 0 .73-.44 10.14 10.14 0 0 0 1.78-2.38c.31-.13.58-.23.81-.31l.42-.14c.45-.13.94-.23 1.44-.33s1-.16 1.48-.2a8.65 8.65 0 0 0 1.39.53c.403.11.814.188 1.23.23h.38V14H11zm0-4.86a3.74 3.74 0 0 0-.64-.28 4.22 4.22 0 0 0-.75-.11c-.411.003-.822.03-1.23.08a3 3 0 0 1-1-.64 6.07 6.07 0 0 1-1.29-2.33c.111-.661.178-1.33.2-2 .02-.25.02-.5 0-.75a1.05 1.05 0 0 0-.2-.88.82.82 0 0 0-.61-.23H8l3 3v4.14z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"file-submodule\">\n    <path fill-rule=\"evenodd\"\n          d=\"M10 7H4v7h9c.55 0 1-.45 1-1V8h-4V7zM9 9H5V8h4v1zm4-5H7V3c0-.66-.31-1-1-1H1c-.55 0-1 .45-1 1v10c0 .55.45 1 1 1h2V7c0-.55.45-1 1-1h6c.55 0 1 .45 1 1h3V5c0-.55-.45-1-1-1zM6 4H1V3h5v1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"file-symlink-directory\">\n    <path fill-rule=\"evenodd\"\n          d=\"M13 4H7V3c0-.66-.31-1-1-1H1c-.55 0-1 .45-1 1v10c0 .55.45 1 1 1h12c.55 0 1-.45 1-1V5c0-.55-.45-1-1-1zM1 3h5v1H1V3zm6 9v-2c-.98-.02-1.84.22-2.55.7-.71.48-1.19 1.25-1.45 2.3.02-1.64.39-2.88 1.13-3.73C4.86 8.43 5.82 8 7.01 8V6l4 3-4 3H7z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"file-symlink-file\">\n    <path fill-rule=\"evenodd\"\n          d=\"M8.5 1H1c-.55 0-1 .45-1 1v12c0 .55.45 1 1 1h10c.55 0 1-.45 1-1V4.5L8.5 1zM11 14H1V2h7l3 3v9zM6 4.5l4 3-4 3v-2c-.98-.02-1.84.22-2.55.7-.71.48-1.19 1.25-1.45 2.3.02-1.64.39-2.88 1.13-3.73.73-.84 1.69-1.27 2.88-1.27v-2H6z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"file-text\">\n    <path\n      d=\"M6 5H2V4h4v1zM2 8h7V7H2v1zm0 2h7V9H2v1zm0 2h7v-1H2v1zm10-7.5V14c0 .55-.45 1-1 1H1c-.55 0-1-.45-1-1V2c0-.55.45-1 1-1h7.5L12 4.5zM11 5L8 2H1v12h10V5z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"file-zip\">\n    <path fill-rule=\"evenodd\"\n          d=\"M8.5 1H1a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V4.5L8.5 1zM11 14H1V2h3v1h1V2h3l3 3v9zM5 4V3h1v1H5zM4 4h1v1H4V4zm1 2V5h1v1H5zM4 6h1v1H4V6zm1 2V7h1v1H5zM4 9.28A2 2 0 0 0 3 11v1h4v-1a2 2 0 0 0-2-2V8H4v1.28zM6 10v1H4v-1h2z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"file\">\n    <path fill-rule=\"evenodd\"\n          d=\"M6 5H2V4h4v1zM2 8h7V7H2v1zm0 2h7V9H2v1zm0 2h7v-1H2v1zm10-7.5V14c0 .55-.45 1-1 1H1c-.55 0-1-.45-1-1V2c0-.55.45-1 1-1h7.5L12 4.5zM11 5L8 2H1v12h10V5z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"flame\">\n    <path fill-rule=\"evenodd\"\n          d=\"M5.05.31c.81 2.17.41 3.38-.52 4.31C3.55 5.67 1.98 6.45.9 7.98c-1.45 2.05-1.7 6.53 3.53 7.7-2.2-1.16-2.67-4.52-.3-6.61-.61 2.03.53 3.33 1.94 2.86 1.39-.47 2.3.53 2.27 1.67-.02.78-.31 1.44-1.13 1.81 3.42-.59 4.78-3.42 4.78-5.56 0-2.84-2.53-3.22-1.25-5.61-1.52.13-2.03 1.13-1.89 2.75.09 1.08-1.02 1.8-1.86 1.33-.67-.41-.66-1.19-.06-1.78C8.18 5.31 8.68 2.45 5.05.32L5.03.3l.02.01z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"fold\">\n    <path fill-rule=\"evenodd\"\n          d=\"M7 9l3 3H8v3H6v-3H4l3-3zm3-6H8V0H6v3H4l3 3 3-3zm4 2c0-.55-.45-1-1-1h-2.5l-1 1h3l-2 2h-7l-2-2h3l-1-1H1c-.55 0-1 .45-1 1l2.5 2.5L0 10c0 .55.45 1 1 1h2.5l1-1h-3l2-2h7l2 2h-3l1 1H13c.55 0 1-.45 1-1l-2.5-2.5L14 5z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"gear\">\n    <path fill-rule=\"evenodd\"\n          d=\"M14 8.77v-1.6l-1.94-.64-.45-1.09.88-1.84-1.13-1.13-1.81.91-1.09-.45-.69-1.92h-1.6l-.63 1.94-1.11.45-1.84-.88-1.13 1.13.91 1.81-.45 1.09L0 7.23v1.59l1.94.64.45 1.09-.88 1.84 1.13 1.13 1.81-.91 1.09.45.69 1.92h1.59l.63-1.94 1.11-.45 1.84.88 1.13-1.13-.92-1.81.47-1.09L14 8.75v.02zM7 11c-1.66 0-3-1.34-3-3s1.34-3 3-3 3 1.34 3 3-1.34 3-3 3z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"gift\">\n    <path fill-rule=\"evenodd\"\n          d=\"M13 4h-1.38c.19-.33.33-.67.36-.91.06-.67-.11-1.22-.52-1.61C11.1 1.1 10.65 1 10.1 1h-.11c-.53.02-1.11.25-1.53.58-.42.33-.73.72-.97 1.2-.23-.48-.55-.88-.97-1.2-.42-.32-1-.58-1.53-.58h-.03c-.56 0-1.06.09-1.44.48-.41.39-.58.94-.52 1.61.03.23.17.58.36.91H1.98c-.55 0-1 .45-1 1v3h1v5c0 .55.45 1 1 1h9c.55 0 1-.45 1-1V8h1V5c0-.55-.45-1-1-1H13zm-4.78-.88c.17-.36.42-.67.75-.92.3-.23.72-.39 1.05-.41h.09c.45 0 .66.11.8.25s.33.39.3.95c-.05.19-.25.61-.5 1h-2.9l.41-.88v.01zM4.09 2.04c.13-.13.31-.25.91-.25.31 0 .72.17 1.03.41.33.25.58.55.75.92L7.2 4H4.3c-.25-.39-.45-.81-.5-1-.03-.56.16-.81.3-.95l-.01-.01zM7 12.99H3V8h4v5-.01zm0-6H2V5h5v2-.01zm5 6H8V8h4v5-.01zm1-6H8V5h5v2-.01z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"gist-secret\">\n    <path fill-rule=\"evenodd\"\n          d=\"M8 10.5L9 14H5l1-3.5L5.25 9h3.5L8 10.5zM10 6H4L2 7h10l-2-1zM9 2L7 3 5 2 4 5h6L9 2zm4.03 7.75L10 9l1 2-2 3h3.22c.45 0 .86-.31.97-.75l.56-2.28c.14-.53-.19-1.08-.72-1.22zM4 9l-3.03.75c-.53.14-.86.69-.72 1.22l.56 2.28c.11.44.52.75.97.75H5l-2-3 1-2z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"gist\">\n    <path fill-rule=\"evenodd\"\n          d=\"M7.5 5L10 7.5 7.5 10l-.75-.75L8.5 7.5 6.75 5.75 7.5 5zm-3 0L2 7.5 4.5 10l.75-.75L3.5 7.5l1.75-1.75L4.5 5zM0 13V2c0-.55.45-1 1-1h10c.55 0 1 .45 1 1v11c0 .55-.45 1-1 1H1c-.55 0-1-.45-1-1zm1 0h10V2H1v11z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 10 16\" id=\"git-branch\">\n    <path fill-rule=\"evenodd\"\n          d=\"M10 5c0-1.11-.89-2-2-2a1.993 1.993 0 0 0-1 3.72v.3c-.02.52-.23.98-.63 1.38-.4.4-.86.61-1.38.63-.83.02-1.48.16-2 .45V4.72a1.993 1.993 0 0 0-1-3.72C.88 1 0 1.89 0 3a2 2 0 0 0 1 1.72v6.56c-.59.35-1 .99-1 1.72 0 1.11.89 2 2 2 1.11 0 2-.89 2-2 0-.53-.2-1-.53-1.36.09-.06.48-.41.59-.47.25-.11.56-.17.94-.17 1.05-.05 1.95-.45 2.75-1.25S8.95 7.77 9 6.73h-.02C9.59 6.37 10 5.73 10 5zM2 1.8c.66 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2C1.35 4.2.8 3.65.8 3c0-.65.55-1.2 1.2-1.2zm0 12.41c-.66 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2zm6-8c-.66 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"git-commit\">\n    <path fill-rule=\"evenodd\"\n          d=\"M10.86 7c-.45-1.72-2-3-3.86-3-1.86 0-3.41 1.28-3.86 3H0v2h3.14c.45 1.72 2 3 3.86 3 1.86 0 3.41-1.28 3.86-3H14V7h-3.14zM7 10.2c-1.22 0-2.2-.98-2.2-2.2 0-1.22.98-2.2 2.2-2.2 1.22 0 2.2.98 2.2 2.2 0 1.22-.98 2.2-2.2 2.2z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"git-compare\">\n    <path fill-rule=\"evenodd\"\n          d=\"M5 12H4c-.27-.02-.48-.11-.69-.31-.21-.2-.3-.42-.31-.69V4.72A1.993 1.993 0 0 0 2 1a1.993 1.993 0 0 0-1 3.72V11c.03.78.34 1.47.94 2.06.6.59 1.28.91 2.06.94h1v2l3-3-3-3v2zM2 1.8c.66 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2C1.35 4.2.8 3.65.8 3c0-.65.55-1.2 1.2-1.2zm11 9.48V5c-.03-.78-.34-1.47-.94-2.06-.6-.59-1.28-.91-2.06-.94H9V0L6 3l3 3V4h1c.27.02.48.11.69.31.21.2.3.42.31.69v6.28A1.993 1.993 0 0 0 12 15a1.993 1.993 0 0 0 1-3.72zm-1 2.92c-.66 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"git-merge\">\n    <path fill-rule=\"evenodd\"\n          d=\"M10 7c-.73 0-1.38.41-1.73 1.02V8C7.22 7.98 6 7.64 5.14 6.98c-.75-.58-1.5-1.61-1.89-2.44A1.993 1.993 0 0 0 2 .99C.89.99 0 1.89 0 3a2 2 0 0 0 1 1.72v6.56c-.59.35-1 .99-1 1.72 0 1.11.89 2 2 2a1.993 1.993 0 0 0 1-3.72V7.67c.67.7 1.44 1.27 2.3 1.69.86.42 2.03.63 2.97.64v-.02c.36.61 1 1.02 1.73 1.02 1.11 0 2-.89 2-2 0-1.11-.89-2-2-2zm-6.8 6c0 .66-.55 1.2-1.2 1.2-.65 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2zM2 4.2C1.34 4.2.8 3.65.8 3c0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2zm8 6c-.66 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"git-pull-request\">\n    <path fill-rule=\"evenodd\"\n          d=\"M11 11.28V5c-.03-.78-.34-1.47-.94-2.06C9.46 2.35 8.78 2.03 8 2H7V0L4 3l3 3V4h1c.27.02.48.11.69.31.21.2.3.42.31.69v6.28A1.993 1.993 0 0 0 10 15a1.993 1.993 0 0 0 1-3.72zm-1 2.92c-.66 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2zM4 3c0-1.11-.89-2-2-2a1.993 1.993 0 0 0-1 3.72v6.56A1.993 1.993 0 0 0 2 15a1.993 1.993 0 0 0 1-3.72V4.72c.59-.34 1-.98 1-1.72zm-.8 10c0 .66-.55 1.2-1.2 1.2-.65 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2zM2 4.2C1.34 4.2.8 3.65.8 3c0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"globe\">\n    <path fill-rule=\"evenodd\"\n          d=\"M7 1C3.14 1 0 4.14 0 8s3.14 7 7 7c.48 0 .94-.05 1.38-.14-.17-.08-.2-.73-.02-1.09.19-.41.81-1.45.2-1.8-.61-.35-.44-.5-.81-.91-.37-.41-.22-.47-.25-.58-.08-.34.36-.89.39-.94.02-.06.02-.27 0-.33 0-.08-.27-.22-.34-.23-.06 0-.11.11-.2.13-.09.02-.5-.25-.59-.33-.09-.08-.14-.23-.27-.34-.13-.13-.14-.03-.33-.11s-.8-.31-1.28-.48c-.48-.19-.52-.47-.52-.66-.02-.2-.3-.47-.42-.67-.14-.2-.16-.47-.2-.41-.04.06.25.78.2.81-.05.02-.16-.2-.3-.38-.14-.19.14-.09-.3-.95s.14-1.3.17-1.75c.03-.45.38.17.19-.13-.19-.3 0-.89-.14-1.11-.13-.22-.88.25-.88.25.02-.22.69-.58 1.16-.92.47-.34.78-.06 1.16.05.39.13.41.09.28-.05-.13-.13.06-.17.36-.13.28.05.38.41.83.36.47-.03.05.09.11.22s-.06.11-.38.3c-.3.2.02.22.55.61s.38-.25.31-.55c-.07-.3.39-.06.39-.06.33.22.27.02.5.08.23.06.91.64.91.64-.83.44-.31.48-.17.59.14.11-.28.3-.28.3-.17-.17-.19.02-.3.08-.11.06-.02.22-.02.22-.56.09-.44.69-.42.83 0 .14-.38.36-.47.58-.09.2.25.64.06.66-.19.03-.34-.66-1.31-.41-.3.08-.94.41-.59 1.08.36.69.92-.19 1.11-.09.19.1-.06.53-.02.55.04.02.53.02.56.61.03.59.77.53.92.55.17 0 .7-.44.77-.45.06-.03.38-.28 1.03.09.66.36.98.31 1.2.47.22.16.08.47.28.58.2.11 1.06-.03 1.28.31.22.34-.88 2.09-1.22 2.28-.34.19-.48.64-.84.92s-.81.64-1.27.91c-.41.23-.47.66-.66.8 3.14-.7 5.48-3.5 5.48-6.84 0-3.86-3.14-7-7-7L7 1zm1.64 6.56c-.09.03-.28.22-.78-.08-.48-.3-.81-.23-.86-.28 0 0-.05-.11.17-.14.44-.05.98.41 1.11.41.13 0 .19-.13.41-.05.22.08.05.13-.05.14zM6.34 1.7c-.05-.03.03-.08.09-.14.03-.03.02-.11.05-.14.11-.11.61-.25.52.03-.11.27-.58.3-.66.25zm1.23.89c-.19-.02-.58-.05-.52-.14.3-.28-.09-.38-.34-.38-.25-.02-.34-.16-.22-.19.12-.03.61.02.7.08.08.06.52.25.55.38.02.13 0 .25-.17.25zm1.47-.05c-.14.09-.83-.41-.95-.52-.56-.48-.89-.31-1-.41-.11-.1-.08-.19.11-.34.19-.15.69.06 1 .09.3.03.66.27.66.55.02.25.33.5.19.63h-.01z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 8 16\" id=\"grabber\">\n    <path fill-rule=\"evenodd\" d=\"M8 4v1H0V4h8zM0 8h8V7H0v1zm0 3h8v-1H0v1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"graph\">\n    <path fill-rule=\"evenodd\" d=\"M16 14v1H0V0h1v14h15zM5 13H3V8h2v5zm4 0H7V3h2v10zm4 0h-2V6h2v7z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"heart\">\n    <path fill-rule=\"evenodd\"\n          d=\"M11.2 3c-.52-.63-1.25-.95-2.2-1-.97 0-1.69.42-2.2 1-.51.58-.78.92-.8 1-.02-.08-.28-.42-.8-1-.52-.58-1.17-1-2.2-1-.95.05-1.69.38-2.2 1-.52.61-.78 1.28-.8 2 0 .52.09 1.52.67 2.67C1.25 8.82 3.01 10.61 6 13c2.98-2.39 4.77-4.17 5.34-5.33C11.91 6.51 12 5.5 12 5c-.02-.72-.28-1.39-.8-2.02V3z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"history\">\n    <path fill-rule=\"evenodd\"\n          d=\"M8 13H6V6h5v2H8v5zM7 1C4.81 1 2.87 2.02 1.59 3.59L0 2v4h4L2.5 4.5C3.55 3.17 5.17 2.3 7 2.3c3.14 0 5.7 2.56 5.7 5.7s-2.56 5.7-5.7 5.7A5.71 5.71 0 0 1 1.3 8c0-.34.03-.67.09-1H.08C.03 7.33 0 7.66 0 8c0 3.86 3.14 7 7 7s7-3.14 7-7-3.14-7-7-7z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"home\">\n    <path fill-rule=\"evenodd\"\n          d=\"M16 9l-3-3V2h-2v2L8 1 0 9h2l1 5c0 .55.45 1 1 1h8c.55 0 1-.45 1-1l1-5h2zm-4 5H9v-4H7v4H4L2.81 7.69 8 2.5l5.19 5.19L12 14z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 10 16\" id=\"horizontal-rule\">\n    <path fill-rule=\"evenodd\"\n          d=\"M1 7h2v2h1V3H3v3H1V3H0v6h1V7zm9 2V7H9v2h1zm0-3V4H9v2h1zM7 6V4h2V3H6v6h1V7h2V6H7zm-7 7h10v-2H0v2z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"hubot\">\n    <path fill-rule=\"evenodd\"\n          d=\"M3 6c-.55 0-1 .45-1 1v2c0 .55.45 1 1 1h8c.55 0 1-.45 1-1V7c0-.55-.45-1-1-1H3zm8 1.75L9.75 9h-1.5L7 7.75 5.75 9h-1.5L3 7.75V7h.75L5 8.25 6.25 7h1.5L9 8.25 10.25 7H11v.75zM5 11h4v1H5v-1zm2-9C3.14 2 0 4.91 0 8.5V13c0 .55.45 1 1 1h12c.55 0 1-.45 1-1V8.5C14 4.91 10.86 2 7 2zm6 11H1V8.5c0-3.09 2.64-5.59 6-5.59s6 2.5 6 5.59V13z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"inbox\">\n    <path fill-rule=\"evenodd\"\n          d=\"M14 9l-1.13-7.14c-.08-.48-.5-.86-1-.86H2.13c-.5 0-.92.38-1 .86L0 9v5c0 .55.45 1 1 1h12c.55 0 1-.45 1-1V9zm-3.28.55l-.44.89c-.17.34-.52.56-.91.56H4.61c-.38 0-.72-.22-.89-.55l-.44-.91c-.17-.33-.52-.55-.89-.55H1l1-7h10l1 7h-1.38c-.39 0-.73.22-.91.55l.01.01z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"info\">\n    <path fill-rule=\"evenodd\"\n          d=\"M6.3 5.69a.942.942 0 0 1-.28-.7c0-.28.09-.52.28-.7.19-.18.42-.28.7-.28.28 0 .52.09.7.28.18.19.28.42.28.7 0 .28-.09.52-.28.7a1 1 0 0 1-.7.3c-.28 0-.52-.11-.7-.3zM8 7.99c-.02-.25-.11-.48-.31-.69-.2-.19-.42-.3-.69-.31H6c-.27.02-.48.13-.69.31-.2.2-.3.44-.31.69h1v3c.02.27.11.5.31.69.2.2.42.31.69.31h1c.27 0 .48-.11.69-.31.2-.19.3-.42.31-.69H8V7.98v.01zM7 2.3c-3.14 0-5.7 2.54-5.7 5.68 0 3.14 2.56 5.7 5.7 5.7s5.7-2.55 5.7-5.7c0-3.15-2.56-5.69-5.7-5.69v.01zM7 .98c3.86 0 7 3.14 7 7s-3.14 7-7 7-7-3.12-7-7 3.14-7 7-7z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"issue-closed\">\n    <path fill-rule=\"evenodd\"\n          d=\"M7 10h2v2H7v-2zm2-6H7v5h2V4zm1.5 1.5l-1 1L12 9l4-4.5-1-1L12 7l-1.5-1.5zM8 13.7A5.71 5.71 0 0 1 2.3 8c0-3.14 2.56-5.7 5.7-5.7 1.83 0 3.45.88 4.5 2.2l.92-.92A6.947 6.947 0 0 0 8 1C4.14 1 1 4.14 1 8s3.14 7 7 7 7-3.14 7-7l-1.52 1.52c-.66 2.41-2.86 4.19-5.48 4.19v-.01z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"issue-opened\">\n    <path fill-rule=\"evenodd\"\n          d=\"M7 2.3c3.14 0 5.7 2.56 5.7 5.7s-2.56 5.7-5.7 5.7A5.71 5.71 0 0 1 1.3 8c0-3.14 2.56-5.7 5.7-5.7zM7 1C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7-3.14-7-7-7zm1 3H6v5h2V4zm0 6H6v2h2v-2z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"issue-reopened\">\n    <path fill-rule=\"evenodd\"\n          d=\"M8 9H6V4h2v5zm-2 3h2v-2H6v2zm6.33-2H10l1.5 1.5c-1.05 1.33-2.67 2.2-4.5 2.2A5.71 5.71 0 0 1 1.3 8c0-.34.03-.67.09-1H.08C.03 7.33 0 7.66 0 8c0 3.86 3.14 7 7 7 2.19 0 4.13-1.02 5.41-2.59L14 14v-4h-1.67zM1.67 6H4L2.5 4.5C3.55 3.17 5.17 2.3 7 2.3c3.14 0 5.7 2.56 5.7 5.7 0 .34-.03.67-.09 1h1.31c.05-.33.08-.66.08-1 0-3.86-3.14-7-7-7-2.19 0-4.13 1.02-5.41 2.59L0 2v4h1.67z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 6 16\" id=\"italic\">\n    <path fill-rule=\"evenodd\"\n          d=\"M2.81 5h1.98L3 14H1l1.81-9zm.36-2.7c0-.7.58-1.3 1.33-1.3.56 0 1.13.38 1.13 1.03 0 .75-.59 1.3-1.33 1.3-.58 0-1.13-.38-1.13-1.03z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"jersey\">\n    <path fill-rule=\"evenodd\"\n          d=\"M4.5 6l-.5.5v5l.5.5h2l.5-.5v-5L6.5 6h-2zM6 11H5V7h1v4zm6.27-7.25C12.05 2.37 11.96 1.12 12 0H9.02c0 .27-.13.48-.39.69-.25.2-.63.3-1.13.3-.5 0-.88-.09-1.13-.3-.23-.2-.36-.42-.36-.69H3c.05 1.13-.03 2.38-.25 3.75C2.55 5.13 1.95 5.88 1 6v9c.02.27.11.48.31.69.2.21.42.3.69.31h11c.27-.02.48-.11.69-.31.21-.2.3-.42.31-.69V6c-.95-.13-1.53-.88-1.75-2.25h.02zM13 15H2V7c.89-.5 1.48-1.25 1.72-2.25S4.03 2.5 4 1h1c-.02.78.16 1.47.52 2.06.36.58 1.02.89 2 .94.98-.02 1.64-.33 2-.94.36-.59.5-1.28.48-2.06h1c.02 1.42.13 2.55.33 3.38.2.81.69 2 1.67 2.63v8V15zM8.5 6l-.5.5v5l.5.5h2l.5-.5v-5l-.5-.5h-2zm1.5 5H9V7h1v4z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"key\">\n    <path fill-rule=\"evenodd\"\n          d=\"M12.83 2.17C12.08 1.42 11.14 1.03 10 1c-1.13.03-2.08.42-2.83 1.17S6.04 3.86 6.01 5c0 .3.03.59.09.89L0 12v1l1 1h2l1-1v-1h1v-1h1v-1h2l1.09-1.11c.3.08.59.11.91.11 1.14-.03 2.08-.42 2.83-1.17S13.97 6.14 14 5c-.03-1.14-.42-2.08-1.17-2.83zM11 5.38c-.77 0-1.38-.61-1.38-1.38 0-.77.61-1.38 1.38-1.38.77 0 1.38.61 1.38 1.38 0 .77-.61 1.38-1.38 1.38z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"keyboard\">\n    <path fill-rule=\"evenodd\"\n          d=\"M10 5H9V4h1v1zM3 6H2v1h1V6zm5-2H7v1h1V4zM4 4H2v1h2V4zm8 7h2v-1h-2v1zM8 7h1V6H8v1zm-4 3H2v1h2v-1zm8-6h-1v1h1V4zm2 0h-1v1h1V4zm-2 5h2V6h-2v3zm4-6v9c0 .55-.45 1-1 1H1c-.55 0-1-.45-1-1V3c0-.55.45-1 1-1h14c.55 0 1 .45 1 1zm-1 0H1v9h14V3zM6 7h1V6H6v1zm0-3H5v1h1V4zM4 7h1V6H4v1zm1 4h6v-1H5v1zm5-4h1V6h-1v1zM3 8H2v1h1V8zm5 0v1h1V8H8zM6 8v1h1V8H6zM5 8H4v1h1V8zm5 1h1V8h-1v1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"law\">\n    <path fill-rule=\"evenodd\"\n          d=\"M7 4c-.83 0-1.5-.67-1.5-1.5S6.17 1 7 1s1.5.67 1.5 1.5S7.83 4 7 4zm7 6c0 1.11-.89 2-2 2h-1c-1.11 0-2-.89-2-2l2-4h-1c-.55 0-1-.45-1-1H8v8c.42 0 1 .45 1 1h1c.42 0 1 .45 1 1H3c0-.55.58-1 1-1h1c0-.55.58-1 1-1h.03L6 5H5c0 .55-.45 1-1 1H3l2 4c0 1.11-.89 2-2 2H2c-1.11 0-2-.89-2-2l2-4H1V5h3c0-.55.45-1 1-1h4c.55 0 1 .45 1 1h3v1h-1l2 4zM2.5 7L1 10h3L2.5 7zM13 10l-1.5-3-1.5 3h3z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"light-bulb\">\n    <path fill-rule=\"evenodd\"\n          d=\"M6.5 0C3.48 0 1 2.19 1 5c0 .92.55 2.25 1 3 1.34 2.25 1.78 2.78 2 4v1h5v-1c.22-1.22.66-1.75 2-4 .45-.75 1-2.08 1-3 0-2.81-2.48-5-5.5-5zm3.64 7.48c-.25.44-.47.8-.67 1.11-.86 1.41-1.25 2.06-1.45 3.23-.02.05-.02.11-.02.17H5c0-.06 0-.13-.02-.17-.2-1.17-.59-1.83-1.45-3.23-.2-.31-.42-.67-.67-1.11C2.44 6.78 2 5.65 2 5c0-2.2 2.02-4 4.5-4 1.22 0 2.36.42 3.22 1.19C10.55 2.94 11 3.94 11 5c0 .66-.44 1.78-.86 2.48zM4 14h5c-.23 1.14-1.3 2-2.5 2s-2.27-.86-2.5-2z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"link-external\">\n    <path fill-rule=\"evenodd\"\n          d=\"M11 10h1v3c0 .55-.45 1-1 1H1c-.55 0-1-.45-1-1V3c0-.55.45-1 1-1h3v1H1v10h10v-3zM6 2l2.25 2.25L5 7.5 6.5 9l3.25-3.25L12 8V2H6z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"link\">\n    <path fill-rule=\"evenodd\"\n          d=\"M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"list-ordered\">\n    <path fill-rule=\"evenodd\"\n          d=\"M12 13c0 .59 0 1-.59 1H4.59C4 14 4 13.59 4 13c0-.59 0-1 .59-1h6.81c.59 0 .59.41.59 1H12zM4.59 4h6.81c.59 0 .59-.41.59-1 0-.59 0-1-.59-1H4.59C4 2 4 2.41 4 3c0 .59 0 1 .59 1zm6.81 3H4.59C4 7 4 7.41 4 8c0 .59 0 1 .59 1h6.81c.59 0 .59-.41.59-1 0-.59 0-1-.59-1zM2 1h-.72c-.3.19-.58.25-1.03.34V2H1v2.14H.16V5H3v-.86H2V1zm.25 8.13c-.17 0-.45.03-.66.06.53-.56 1.14-1.25 1.14-1.89C2.71 6.52 2.17 6 1.37 6c-.59 0-.97.2-1.38.64l.58.58c.19-.19.38-.38.64-.38.28 0 .48.16.48.52 0 .53-.77 1.2-1.7 2.06V10h3l-.09-.88h-.66l.01.01zm-.08 3.78v-.03c.44-.19.64-.47.64-.86 0-.7-.56-1.11-1.44-1.11-.48 0-.89.19-1.28.52l.55.64c.25-.2.44-.31.69-.31.27 0 .42.13.42.36 0 .27-.2.44-.86.44v.75c.83 0 .98.17.98.47 0 .25-.23.38-.58.38-.28 0-.56-.14-.81-.38l-.48.66c.3.36.77.56 1.41.56.83 0 1.53-.41 1.53-1.16 0-.5-.31-.81-.77-.94v.01z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"list-unordered\">\n    <path fill-rule=\"evenodd\"\n          d=\"M2 13c0 .59 0 1-.59 1H.59C0 14 0 13.59 0 13c0-.59 0-1 .59-1h.81c.59 0 .59.41.59 1H2zm2.59-9h6.81c.59 0 .59-.41.59-1 0-.59 0-1-.59-1H4.59C4 2 4 2.41 4 3c0 .59 0 1 .59 1zM1.41 7H.59C0 7 0 7.41 0 8c0 .59 0 1 .59 1h.81c.59 0 .59-.41.59-1 0-.59 0-1-.59-1h.01zm0-5H.59C0 2 0 2.41 0 3c0 .59 0 1 .59 1h.81c.59 0 .59-.41.59-1 0-.59 0-1-.59-1h.01zm10 5H4.59C4 7 4 7.41 4 8c0 .59 0 1 .59 1h6.81c.59 0 .59-.41.59-1 0-.59 0-1-.59-1h.01zm0 5H4.59C4 12 4 12.41 4 13c0 .59 0 1 .59 1h6.81c.59 0 .59-.41.59-1 0-.59 0-1-.59-1h.01z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"location\">\n    <path fill-rule=\"evenodd\"\n          d=\"M6 0C2.69 0 0 2.5 0 5.5 0 10.02 6 16 6 16s6-5.98 6-10.5C12 2.5 9.31 0 6 0zm0 14.55C4.14 12.52 1 8.44 1 5.5 1 3.02 3.25 1 6 1c1.34 0 2.61.48 3.56 1.36.92.86 1.44 1.97 1.44 3.14 0 2.94-3.14 7.02-5 9.05zM8 5.5c0 1.11-.89 2-2 2-1.11 0-2-.89-2-2 0-1.11.89-2 2-2 1.11 0 2 .89 2 2z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"lock\">\n    <path fill-rule=\"evenodd\"\n          d=\"M4 13H3v-1h1v1zm8-6v7c0 .55-.45 1-1 1H1c-.55 0-1-.45-1-1V7c0-.55.45-1 1-1h1V4c0-2.2 1.8-4 4-4s4 1.8 4 4v2h1c.55 0 1 .45 1 1zM3.8 6h4.41V4c0-1.22-.98-2.2-2.2-2.2-1.22 0-2.2.98-2.2 2.2v2H3.8zM11 7H2v7h9V7zM4 8H3v1h1V8zm0 2H3v1h1v-1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 25 16\" id=\"logo-gist\">\n    <path fill-rule=\"evenodd\"\n          d=\"M4.7 8.73h2.45v4.02c-.55.27-1.64.34-2.53.34-2.56 0-3.47-2.2-3.47-5.05 0-2.85.91-5.06 3.48-5.06 1.28 0 2.06.23 3.28.73V2.66C7.27 2.33 6.25 2 4.63 2 1.13 2 0 4.69 0 8.03c0 3.34 1.11 6.03 4.63 6.03 1.64 0 2.81-.27 3.59-.64V7.73H4.7v1zm6.39 3.72V6.06h-1.05v6.28c0 1.25.58 1.72 1.72 1.72v-.89c-.48 0-.67-.16-.67-.7v-.02zm.25-8.72c0-.44-.33-.78-.78-.78s-.77.34-.77.78.33.78.77.78.78-.34.78-.78zm4.34 5.69c-1.5-.13-1.78-.48-1.78-1.17 0-.77.33-1.34 1.88-1.34 1.05 0 1.66.16 2.27.36v-.94c-.69-.3-1.52-.39-2.25-.39-2.2 0-2.92 1.2-2.92 2.31 0 1.08.47 1.88 2.73 2.08 1.55.13 1.77.63 1.77 1.34 0 .73-.44 1.42-2.06 1.42-1.11 0-1.86-.19-2.33-.36v.94c.5.2 1.58.39 2.33.39 2.38 0 3.14-1.2 3.14-2.41 0-1.28-.53-2.03-2.75-2.23h-.03zm8.58-2.47v-.86h-2.42v-2.5l-1.08.31v2.11l-1.56.44v.48h1.56v5c0 1.53 1.19 2.13 2.5 2.13.19 0 .52-.02.69-.05v-.89c-.19.03-.41.03-.61.03-.97 0-1.5-.39-1.5-1.34V6.94h2.42v.02-.01z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 45 16\" id=\"logo-github\">\n    <path fill-rule=\"evenodd\"\n          d=\"M18.53 12.03h-.02c.009 0 .015.01.024.011h.006l-.01-.01zm.004.011c-.093.001-.327.05-.574.05-.78 0-1.05-.36-1.05-.83V8.13h1.59c.09 0 .16-.08.16-.19v-1.7c0-.09-.08-.17-.16-.17h-1.59V3.96c0-.08-.05-.13-.14-.13h-2.16c-.09 0-.14.05-.14.13v2.17s-1.09.27-1.16.28c-.08.02-.13.09-.13.17v1.36c0 .11.08.19.17.19h1.11v3.28c0 2.44 1.7 2.69 2.86 2.69.53 0 1.17-.17 1.27-.22.06-.02.09-.09.09-.16v-1.5a.177.177 0 0 0-.146-.18zm23.696-2.2c0-1.81-.73-2.05-1.5-1.97-.6.04-1.08.34-1.08.34v3.52s.49.34 1.22.36c1.03.03 1.36-.34 1.36-2.25zm2.43-.16c0 3.43-1.11 4.41-3.05 4.41-1.64 0-2.52-.83-2.52-.83s-.04.46-.09.52c-.03.06-.08.08-.14.08h-1.48c-.1 0-.19-.08-.19-.17l.02-11.11c0-.09.08-.17.17-.17h2.13c.09 0 .17.08.17.17v3.77s.82-.53 2.02-.53l-.01-.02c1.2 0 2.97.45 2.97 3.88zm-8.72-3.61H33.84c-.11 0-.17.08-.17.19v5.44s-.55.39-1.3.39-.97-.34-.97-1.09V6.25c0-.09-.08-.17-.17-.17h-2.14c-.09 0-.17.08-.17.17v5.11c0 2.2 1.23 2.75 2.92 2.75 1.39 0 2.52-.77 2.52-.77s.05.39.08.45c.02.05.09.09.16.09h1.34c.11 0 .17-.08.17-.17l.02-7.47c0-.09-.08-.17-.19-.17zm-23.7-.01h-2.13c-.09 0-.17.09-.17.2v7.34c0 .2.13.27.3.27h1.92c.2 0 .25-.09.25-.27V6.23c0-.09-.08-.17-.17-.17zm-1.05-3.38c-.77 0-1.38.61-1.38 1.38 0 .77.61 1.38 1.38 1.38.75 0 1.36-.61 1.36-1.38 0-.77-.61-1.38-1.36-1.38zm16.49-.25h-2.11c-.09 0-.17.08-.17.17v4.09h-3.31V2.6c0-.09-.08-.17-.17-.17h-2.13c-.09 0-.17.08-.17.17v11.11c0 .09.09.17.17.17h2.13c.09 0 .17-.08.17-.17V8.96h3.31l-.02 4.75c0 .09.08.17.17.17h2.13c.09 0 .17-.08.17-.17V2.6c0-.09-.08-.17-.17-.17zM8.81 7.35v5.74c0 .04-.01.11-.06.13 0 0-1.25.89-3.31.89-2.49 0-5.44-.78-5.44-5.92S2.58 1.99 5.1 2c2.18 0 3.06.49 3.2.58.04.05.06.09.06.14L7.94 4.5c0 .09-.09.2-.2.17-.36-.11-.9-.33-2.17-.33-1.47 0-3.05.42-3.05 3.73s1.5 3.7 2.58 3.7c.92 0 1.25-.11 1.25-.11v-2.3H4.88c-.11 0-.19-.08-.19-.17V7.35c0-.09.08-.17.19-.17h3.74c.11 0 .19.08.19.17z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"mail-read\">\n    <path fill-rule=\"evenodd\"\n          d=\"M6 5H4V4h2v1zm3 1H4v1h5V6zm5-.48V14c0 .55-.45 1-1 1H1c-.55 0-1-.45-1-1V5.52c0-.33.16-.63.42-.81L2 3.58V3c0-.55.45-1 1-1h1.2L7 0l2.8 2H11c.55 0 1 .45 1 1v.58l1.58 1.13c.27.19.42.48.42.81zM3 7.5L7 10l4-2.5V3H3v4.5zm-2 6l4.5-3-4.5-3v6zm11 .5l-5-3-5 3h10zm1-6.5l-4.5 3 4.5 3v-6z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"mail-reply\">\n    <path fill-rule=\"evenodd\" d=\"M6 2.5L0 7l6 4.5v-3c1.73 0 5.14.95 6 4.38 0-4.55-3.06-7.05-6-7.38v-3z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"mail\">\n    <path fill-rule=\"evenodd\"\n          d=\"M0 4v8c0 .55.45 1 1 1h12c.55 0 1-.45 1-1V4c0-.55-.45-1-1-1H1c-.55 0-1 .45-1 1zm13 0L7 9 1 4h12zM1 5.5l4 3-4 3v-6zM2 12l3.5-3L7 10.5 8.5 9l3.5 3H2zm11-.5l-4-3 4-3v6z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"mark-github\">\n    <path fill-rule=\"evenodd\"\n          d=\"M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0 0 16 8c0-4.42-3.58-8-8-8z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"markdown\">\n    <path fill-rule=\"evenodd\"\n          d=\"M14.85 3H1.15C.52 3 0 3.52 0 4.15v7.69C0 12.48.52 13 1.15 13h13.69c.64 0 1.15-.52 1.15-1.15v-7.7C16 3.52 15.48 3 14.85 3zM9 11H7V8L5.5 9.92 4 8v3H2V5h2l1.5 2L7 5h2v6zm2.99.5L9.5 8H11V5h2v3h1.5l-2.51 3.5z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"megaphone\">\n    <path fill-rule=\"evenodd\"\n          d=\"M10 1c-.17 0-.36.05-.52.14C8.04 2.02 4.5 4.58 3 5c-1.38 0-3 .67-3 2.5S1.63 10 3 10c.3.08.64.23 1 .41V15h2v-3.45c1.34.86 2.69 1.83 3.48 2.31.16.09.34.14.52.14.52 0 1-.42 1-1V2c0-.58-.48-1-1-1zm0 12c-.38-.23-.89-.58-1.5-1-.16-.11-.33-.22-.5-.34V3.31c.16-.11.31-.2.47-.31.61-.41 1.16-.77 1.53-1v11zm2-6h4v1h-4V7zm0 2l4 2v1l-4-2V9zm4-6v1l-4 2V5l4-2z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"mention\">\n    <path fill-rule=\"evenodd\"\n          d=\"M6.58 15c1.25 0 2.52-.31 3.56-.94l-.42-.94c-.84.52-1.89.83-3.03.83-3.23 0-5.64-2.08-5.64-5.72 0-4.37 3.23-7.18 6.58-7.18 3.45 0 5.22 2.19 5.22 5.2 0 2.39-1.34 3.86-2.5 3.86-1.05 0-1.36-.73-1.05-2.19l.73-3.75H8.98l-.11.72c-.41-.63-.94-.83-1.56-.83-2.19 0-3.66 2.39-3.66 4.38 0 1.67.94 2.61 2.3 2.61.84 0 1.67-.53 2.3-1.25.11.94.94 1.45 1.98 1.45 1.67 0 3.77-1.67 3.77-5C14 2.61 11.59 0 7.83 0 3.66 0 0 3.33 0 8.33 0 12.71 2.92 15 6.58 15zm-.31-5c-.73 0-1.36-.52-1.36-1.67 0-1.45.94-3.22 2.41-3.22.52 0 .84.2 1.25.83l-.52 3.02c-.63.73-1.25 1.05-1.78 1.05V10z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"milestone\">\n    <path fill-rule=\"evenodd\"\n          d=\"M8 2H6V0h2v2zm4 5H2c-.55 0-1-.45-1-1V4c0-.55.45-1 1-1h10l2 2-2 2zM8 4H6v2h2V4zM6 16h2V8H6v8z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"mirror\">\n    <path fill-rule=\"evenodd\"\n          d=\"M15.5 4.7L8.5 0l-7 4.7c-.3.19-.5.45-.5.8V16l7.5-4 7.5 4V5.5c0-.34-.2-.61-.5-.8zm-.5 9.8l-6-3.25V10H8v1.25L2 14.5v-9l6-4V6h1V1.5l6 4v9zM6 7h5V5l3 3-3 3V9H6v2L3 8l3-3v2z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"mortar-board\">\n    <path fill-rule=\"evenodd\"\n          d=\"M7.83 9.19L4 8c-4-8 0 1.5 0 2.5S5.8 12 8 12s4-.5 4-1.5V8L8.17 9.19a.73.73 0 0 1-.36 0h.02zm.28-6.39a.34.34 0 0 0-.2 0L.27 5.18a.35.35 0 0 0 0 .67L2 6.4v1.77c-.3.17-.5.5-.5.86 0 .19.05.36.14.5-.08.14-.14.31-.14.5v2.58c0 .55 2 .55 2 0v-2.58c0-.19-.05-.36-.14-.5.08-.14.14-.31.14-.5 0-.38-.2-.69-.5-.86V6.72l4.89 1.53c.06.02.14.02.2 0l7.64-2.38a.35.35 0 0 0 0-.67L8.1 2.81l.01-.01zM8.02 6c-.55 0-1-.22-1-.5s.45-.5 1-.5 1 .22 1 .5-.45.5-1 .5z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"mute\">\n    <path fill-rule=\"evenodd\"\n          d=\"M8 2.81v10.38c0 .67-.81 1-1.28.53L3 10H1c-.55 0-1-.45-1-1V7c0-.55.45-1 1-1h2l3.72-3.72C7.19 1.81 8 2.14 8 2.81zm7.53 3.22l-1.06-1.06-1.97 1.97-1.97-1.97-1.06 1.06L11.44 8 9.47 9.97l1.06 1.06 1.97-1.97 1.97 1.97 1.06-1.06L13.56 8l1.97-1.97z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"no-newline\">\n    <path fill-rule=\"evenodd\"\n          d=\"M16 5v3c0 .55-.45 1-1 1h-3v2L9 8l3-3v2h2V5h2zM8 8c0 2.2-1.8 4-4 4s-4-1.8-4-4 1.8-4 4-4 4 1.8 4 4zM1.5 9.66L5.66 5.5C5.18 5.19 4.61 5 4 5 2.34 5 1 6.34 1 8c0 .61.19 1.17.5 1.66zM7 8c0-.61-.19-1.17-.5-1.66L2.34 10.5c.48.31 1.05.5 1.66.5 1.66 0 3-1.34 3-3z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"note\">\n    <path fill-rule=\"evenodd\"\n          d=\"M3 10h4V9H3v1zm0-2h6V7H3v1zm0-2h8V5H3v1zm10 6H1V3h12v9zM1 2c-.55 0-1 .45-1 1v9c0 .55.45 1 1 1h12c.55 0 1-.45 1-1V3c0-.55-.45-1-1-1H1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"octoface\">\n    <path fill-rule=\"evenodd\"\n          d=\"M14.7 5.34c.13-.32.55-1.59-.13-3.31 0 0-1.05-.33-3.44 1.3-1-.28-2.07-.32-3.13-.32s-2.13.04-3.13.32c-2.39-1.64-3.44-1.3-3.44-1.3-.68 1.72-.26 2.99-.13 3.31C.49 6.21 0 7.33 0 8.69 0 13.84 3.33 15 7.98 15S16 13.84 16 8.69c0-1.36-.49-2.48-1.3-3.35zM8 14.02c-3.3 0-5.98-.15-5.98-3.35 0-.76.38-1.48 1.02-2.07 1.07-.98 2.9-.46 4.96-.46 2.07 0 3.88-.52 4.96.46.65.59 1.02 1.3 1.02 2.07 0 3.19-2.68 3.35-5.98 3.35zM5.49 9.01c-.66 0-1.2.8-1.2 1.78s.54 1.79 1.2 1.79c.66 0 1.2-.8 1.2-1.79s-.54-1.78-1.2-1.78zm5.02 0c-.66 0-1.2.79-1.2 1.78s.54 1.79 1.2 1.79c.66 0 1.2-.8 1.2-1.79s-.53-1.78-1.2-1.78z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"organization\">\n    <path fill-rule=\"evenodd\"\n          d=\"M16 12.999c0 .439-.45 1-1 1H7.995c-.539 0-.994-.447-.995-.999H1c-.54 0-1-.561-1-1 0-2.634 3-4 3-4s.229-.409 0-1c-.841-.621-1.058-.59-1-3 .058-2.419 1.367-3 2.5-3s2.442.58 2.5 3c.058 2.41-.159 2.379-1 3-.229.59 0 1 0 1s1.549.711 2.42 2.088C9.196 9.369 10 8.999 10 8.999s.229-.409 0-1c-.841-.62-1.058-.59-1-3 .058-2.419 1.367-3 2.5-3s2.437.581 2.495 3c.059 2.41-.158 2.38-1 3-.229.59 0 1 0 1s3.005 1.366 3.005 4\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"package\">\n    <path fill-rule=\"evenodd\"\n          d=\"M1 4.27v7.47c0 .45.3.84.75.97l6.5 1.73c.16.05.34.05.5 0l6.5-1.73c.45-.13.75-.52.75-.97V4.27c0-.45-.3-.84-.75-.97l-6.5-1.74a1.4 1.4 0 0 0-.5 0L1.75 3.3c-.45.13-.75.52-.75.97zm7 9.09l-6-1.59V5l6 1.61v6.75zM2 4l2.5-.67L11 5.06l-2.5.67L2 4zm13 7.77l-6 1.59V6.61l2-.55V8.5l2-.53V5.53L15 5v6.77zm-2-7.24L6.5 2.8l2-.53L15 4l-2 .53z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"paintcan\">\n    <path fill-rule=\"evenodd\"\n          d=\"M6 0C2.69 0 0 2.69 0 6v1c0 .55.45 1 1 1v5c0 1.1 2.24 2 5 2s5-.9 5-2V8c.55 0 1-.45 1-1V6c0-3.31-2.69-6-6-6zm3 10v.5c0 .28-.22.5-.5.5s-.5-.22-.5-.5V10c0-.28-.22-.5-.5-.5s-.5.22-.5.5v2.5c0 .28-.22.5-.5.5s-.5-.22-.5-.5v-2c0-.28-.22-.5-.5-.5s-.5.22-.5.5v.5c0 .55-.45 1-1 1s-1-.45-1-1v-1c-.55 0-1-.45-1-1V7.2c.91.49 2.36.8 4 .8 1.64 0 3.09-.31 4-.8V9c0 .55-.45 1-1 1zM6 7c-1.68 0-3.12-.41-3.71-1C2.88 5.41 4.32 5 6 5c1.68 0 3.12.41 3.71 1-.59.59-2.03 1-3.71 1zm0-3c-2.76 0-5 .89-5 2 0-2.76 2.24-5 5-5s5 2.24 5 5c0-1.1-2.24-2-5-2z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"pencil\">\n    <path fill-rule=\"evenodd\"\n          d=\"M0 12v3h3l8-8-3-3-8 8zm3 2H1v-2h1v1h1v1zm10.3-9.3L12 6 9 3l1.3-1.3a.996.996 0 0 1 1.41 0l1.59 1.59c.39.39.39 1.02 0 1.41z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"person\">\n    <path fill-rule=\"evenodd\"\n          d=\"M12 14.002a.998.998 0 0 1-.998.998H1.001A1 1 0 0 1 0 13.999V13c0-2.633 4-4 4-4s.229-.409 0-1c-.841-.62-.944-1.59-1-4 .173-2.413 1.867-3 3-3s2.827.586 3 3c-.056 2.41-.159 3.38-1 4-.229.59 0 1 0 1s4 1.367 4 4v1.002z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"pin\">\n    <path fill-rule=\"evenodd\"\n          d=\"M10 1.2V2l.5 1L6 6H2.2c-.44 0-.67.53-.34.86L5 10l-4 5 5-4 3.14 3.14a.5.5 0 0 0 .86-.34V10l3-4.5 1 .5h.8c.44 0 .67-.53.34-.86L10.86.86a.5.5 0 0 0-.86.34z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"plug\">\n    <path fill-rule=\"evenodd\"\n          d=\"M14 6V5h-4V3H8v1H6c-1.03 0-1.77.81-2 2L3 7c-1.66 0-3 1.34-3 3v2h1v-2c0-1.11.89-2 2-2l1 1c.25 1.16.98 2 2 2h2v1h2v-2h4V9h-4V6h4z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 7 16\" id=\"plus-small\">\n    <path fill-rule=\"evenodd\" d=\"M4 7V4H3v3H0v1h3v3h1V8h3V7H4z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"plus\">\n    <path fill-rule=\"evenodd\" d=\"M12 9H7v5H5V9H0V7h5V2h2v5h5z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 8 16\" id=\"primitive-dot\">\n    <path fill-rule=\"evenodd\" d=\"M0 8c0-2.2 1.8-4 4-4s4 1.8 4 4-1.8 4-4 4-4-1.8-4-4z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 8 16\" id=\"primitive-square\">\n    <path fill-rule=\"evenodd\" d=\"M8 12H0V4h8z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 15 16\" id=\"project\">\n    <path fill-rule=\"evenodd\"\n          d=\"M10 12h3V2h-3v10zm-4-2h3V2H6v8zm-4 4h3V2H2v12zm-1 1h13V1H1v14zM14 0H1a1 1 0 0 0-1 1v14a1 1 0 0 0 1 1h13a1 1 0 0 0 1-1V1a1 1 0 0 0-1-1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"pulse\">\n    <path fill-rule=\"evenodd\" d=\"M11.5 8L8.8 5.4 6.6 8.5 5.5 1.6 2.38 8H0v2h3.6l.9-1.8.9 5.4L9 8.5l1.6 1.5H14V8z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"question\">\n    <path fill-rule=\"evenodd\"\n          d=\"M6 10h2v2H6v-2zm4-3.5C10 8.64 8 9 8 9H6c0-.55.45-1 1-1h.5c.28 0 .5-.22.5-.5v-1c0-.28-.22-.5-.5-.5h-1c-.28 0-.5.22-.5.5V7H4c0-1.5 1.5-3 3-3s3 1 3 2.5zM7 2.3c3.14 0 5.7 2.56 5.7 5.7s-2.56 5.7-5.7 5.7A5.71 5.71 0 0 1 1.3 8c0-3.14 2.56-5.7 5.7-5.7zM7 1C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7-3.14-7-7-7z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"quote\">\n    <path fill-rule=\"evenodd\"\n          d=\"M6.16 3.5C3.73 5.06 2.55 6.67 2.55 9.36c.16-.05.3-.05.44-.05 1.27 0 2.5.86 2.5 2.41 0 1.61-1.03 2.61-2.5 2.61-1.9 0-2.99-1.52-2.99-4.25 0-3.8 1.75-6.53 5.02-8.42L6.16 3.5zm7 0c-2.43 1.56-3.61 3.17-3.61 5.86.16-.05.3-.05.44-.05 1.27 0 2.5.86 2.5 2.41 0 1.61-1.03 2.61-2.5 2.61-1.89 0-2.98-1.52-2.98-4.25 0-3.8 1.75-6.53 5.02-8.42l1.14 1.84h-.01z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"radio-tower\">\n    <path fill-rule=\"evenodd\"\n          d=\"M4.79 6.11c.25-.25.25-.67 0-.92-.32-.33-.48-.76-.48-1.19 0-.43.16-.86.48-1.19.25-.26.25-.67 0-.92a.613.613 0 0 0-.45-.19c-.16 0-.33.06-.45.19-.57.58-.85 1.35-.85 2.11 0 .76.29 1.53.85 2.11.25.25.66.25.9 0zM2.33.52a.651.651 0 0 0-.92 0C.48 1.48.01 2.74.01 3.99c0 1.26.47 2.52 1.4 3.48.25.26.66.26.91 0s.25-.68 0-.94c-.68-.7-1.02-1.62-1.02-2.54 0-.92.34-1.84 1.02-2.54a.66.66 0 0 0 .01-.93zm5.69 5.1A1.62 1.62 0 1 0 6.4 4c-.01.89.72 1.62 1.62 1.62zM14.59.53a.628.628 0 0 0-.91 0c-.25.26-.25.68 0 .94.68.7 1.02 1.62 1.02 2.54 0 .92-.34 1.83-1.02 2.54-.25.26-.25.68 0 .94a.651.651 0 0 0 .92 0c.93-.96 1.4-2.22 1.4-3.48A5.048 5.048 0 0 0 14.59.53zM8.02 6.92c-.41 0-.83-.1-1.2-.3l-3.15 8.37h1.49l.86-1h4l.84 1h1.49L9.21 6.62c-.38.2-.78.3-1.19.3zm-.01.48L9.02 11h-2l.99-3.6zm-1.99 5.59l1-1h2l1 1h-4zm5.19-11.1c-.25.25-.25.67 0 .92.32.33.48.76.48 1.19 0 .43-.16.86-.48 1.19-.25.26-.25.67 0 .92a.63.63 0 0 0 .9 0c.57-.58.85-1.35.85-2.11 0-.76-.28-1.53-.85-2.11a.634.634 0 0 0-.9 0z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"reply\">\n    <path fill-rule=\"evenodd\" d=\"M6 3.5c3.92.44 8 3.125 8 10-2.312-5.062-4.75-6-8-6V11L.5 5.5 6 0v3.5z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"repo-clone\">\n    <path fill-rule=\"evenodd\"\n          d=\"M15 0H9v7c0 .55.45 1 1 1h1v1h1V8h3c.55 0 1-.45 1-1V1c0-.55-.45-1-1-1zm-4 7h-1V6h1v1zm4 0h-3V6h3v1zm0-2h-4V1h4v4zM4 5H3V4h1v1zm0-2H3V2h1v1zM2 1h6V0H1C.45 0 0 .45 0 1v12c0 .55.45 1 1 1h2v2l1.5-1.5L6 16v-2h5c.55 0 1-.45 1-1v-3H2V1zm9 10v2H6v-1H3v1H1v-2h10zM3 8h1v1H3V8zm1-1H3V6h1v1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"repo-force-push\">\n    <path fill-rule=\"evenodd\"\n          d=\"M10 9H8v7H6V9H4l2.25-3H4l3-4 3 4H7.75L10 9zm1-9H1C.45 0 0 .45 0 1v12c0 .55.45 1 1 1h4v-1H1v-2h4v-1H2V1h9v9H9v1h2v2H9v1h2c.55 0 1-.45 1-1V1c0-.55-.45-1-1-1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 10 16\" id=\"repo-forked\">\n    <path fill-rule=\"evenodd\"\n          d=\"M8 1a1.993 1.993 0 0 0-1 3.72V6L5 8 3 6V4.72A1.993 1.993 0 0 0 2 1a1.993 1.993 0 0 0-1 3.72V6.5l3 3v1.78A1.993 1.993 0 0 0 5 15a1.993 1.993 0 0 0 1-3.72V9.5l3-3V4.72A1.993 1.993 0 0 0 8 1zM2 4.2C1.34 4.2.8 3.65.8 3c0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2zm3 10c-.66 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2zm3-10c-.66 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"repo-pull\">\n    <path fill-rule=\"evenodd\"\n          d=\"M13 8V6H7V4h6V2l3 3-3 3zM4 2H3v1h1V2zm7 5h1v6c0 .55-.45 1-1 1H6v2l-1.5-1.5L3 16v-2H1c-.55 0-1-.45-1-1V1c0-.55.45-1 1-1h10c.55 0 1 .45 1 1v2h-1V1H2v9h9V7zm0 4H1v2h2v-1h3v1h5v-2zM4 6H3v1h1V6zm0-2H3v1h1V4zM3 9h1V8H3v1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"repo-push\">\n    <path fill-rule=\"evenodd\"\n          d=\"M4 3H3V2h1v1zM3 5h1V4H3v1zm4 0L4 9h2v7h2V9h2L7 5zm4-5H1C.45 0 0 .45 0 1v12c0 .55.45 1 1 1h4v-1H1v-2h4v-1H2V1h9.02L11 10H9v1h2v2H9v1h2c.55 0 1-.45 1-1V1c0-.55-.45-1-1-1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"repo\">\n    <path fill-rule=\"evenodd\"\n          d=\"M4 9H3V8h1v1zm0-3H3v1h1V6zm0-2H3v1h1V4zm0-2H3v1h1V2zm8-1v12c0 .55-.45 1-1 1H6v2l-1.5-1.5L3 16v-2H1c-.55 0-1-.45-1-1V1c0-.55.45-1 1-1h10c.55 0 1 .45 1 1zm-1 10H1v2h2v-1h3v1h5v-2zm0-10H2v9h9V1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"rocket\">\n    <path fill-rule=\"evenodd\"\n          d=\"M12.17 3.83c-.27-.27-.47-.55-.63-.88-.16-.31-.27-.66-.34-1.02-.58.33-1.16.7-1.73 1.13-.58.44-1.14.94-1.69 1.48-.7.7-1.33 1.81-1.78 2.45H3L0 10h3l2-2c-.34.77-1.02 2.98-1 3l1 1c.02.02 2.23-.64 3-1l-2 2v3l3-3v-3c.64-.45 1.75-1.09 2.45-1.78.55-.55 1.05-1.13 1.47-1.7.44-.58.81-1.16 1.14-1.72-.36-.08-.7-.19-1.03-.34a3.39 3.39 0 0 1-.86-.63M16 0s-.09.38-.3 1.06c-.2.7-.55 1.58-1.06 2.66-.7-.08-1.27-.33-1.66-.72-.39-.39-.63-.94-.7-1.64C13.36.84 14.23.48 14.92.28 15.62.08 16 0 16 0\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 10 16\" id=\"rss\">\n    <path fill-rule=\"evenodd\"\n          d=\"M2 13H0v-2c1.11 0 2 .89 2 2zM0 3v1a9 9 0 0 1 9 9h1C10 7.48 5.52 3 0 3zm0 4v1c2.75 0 5 2.25 5 5h1c0-3.31-2.69-6-6-6z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"ruby\">\n    <path fill-rule=\"evenodd\" d=\"M13 6l-5 5V4h3l2 2zm3 0l-8 8-8-8 4-4h8l4 4zm-8 6.5L14.5 6l-3-3h-7l-3 3L8 12.5z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"screen-full\">\n    <path fill-rule=\"evenodd\"\n          d=\"M13 10h1v3c0 .547-.453 1-1 1h-3v-1h3v-3zM1 10H0v3c0 .547.453 1 1 1h3v-1H1v-3zm0-7h3V2H1c-.547 0-1 .453-1 1v3h1V3zm1 1h10v8H2V4zm2 6h6V6H4v4zm6-8v1h3v3h1V3c0-.547-.453-1-1-1h-3z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"screen-normal\">\n    <path fill-rule=\"evenodd\"\n          d=\"M2 4H0V3h2V1h1v2c0 .547-.453 1-1 1zm0 8H0v1h2v2h1v-2c0-.547-.453-1-1-1zm9-2c0 .547-.453 1-1 1H4c-.547 0-1-.453-1-1V6c0-.547.453-1 1-1h6c.547 0 1 .453 1 1v4zM9 7H5v2h4V7zm2 6v2h1v-2h2v-1h-2c-.547 0-1 .453-1 1zm1-10V1h-1v2c0 .547.453 1 1 1h2V3h-2z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"search\">\n    <path fill-rule=\"evenodd\"\n          d=\"M15.7 13.3l-3.81-3.83A5.93 5.93 0 0 0 13 6c0-3.31-2.69-6-6-6S1 2.69 1 6s2.69 6 6 6c1.3 0 2.48-.41 3.47-1.11l3.83 3.81c.19.2.45.3.7.3.25 0 .52-.09.7-.3a.996.996 0 0 0 0-1.41v.01zM7 10.7c-2.59 0-4.7-2.11-4.7-4.7 0-2.59 2.11-4.7 4.7-4.7 2.59 0 4.7 2.11 4.7 4.7 0 2.59-2.11 4.7-4.7 4.7z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"server\">\n    <path fill-rule=\"evenodd\"\n          d=\"M11 6H1c-.55 0-1 .45-1 1v2c0 .55.45 1 1 1h10c.55 0 1-.45 1-1V7c0-.55-.45-1-1-1zM2 9H1V7h1v2zm2 0H3V7h1v2zm2 0H5V7h1v2zm2 0H7V7h1v2zm3-8H1c-.55 0-1 .45-1 1v2c0 .55.45 1 1 1h10c.55 0 1-.45 1-1V2c0-.55-.45-1-1-1zM2 4H1V2h1v2zm2 0H3V2h1v2zm2 0H5V2h1v2zm2 0H7V2h1v2zm3-1h-1V2h1v1zm0 8H1c-.55 0-1 .45-1 1v2c0 .55.45 1 1 1h10c.55 0 1-.45 1-1v-2c0-.55-.45-1-1-1zm-9 3H1v-2h1v2zm2 0H3v-2h1v2zm2 0H5v-2h1v2zm2 0H7v-2h1v2z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"settings\">\n    <path fill-rule=\"evenodd\"\n          d=\"M4 7H3V2h1v5zm-1 7h1v-3H3v3zm5 0h1V8H8v6zm5 0h1v-2h-1v2zm1-12h-1v6h1V2zM9 2H8v2h1V2zM5 8H2c-.55 0-1 .45-1 1s.45 1 1 1h3c.55 0 1-.45 1-1s-.45-1-1-1zm5-3H7c-.55 0-1 .45-1 1s.45 1 1 1h3c.55 0 1-.45 1-1s-.45-1-1-1zm5 4h-3c-.55 0-1 .45-1 1s.45 1 1 1h3c.55 0 1-.45 1-1s-.45-1-1-1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"shield\">\n    <path fill-rule=\"evenodd\"\n          d=\"M7 0L0 2v6.02C0 12.69 5.31 16 7 16c1.69 0 7-3.31 7-7.98V2L7 0zM5 11l1.14-2.8a.568.568 0 0 0-.25-.59C5.33 7.25 5 6.66 5 6c0-1.09.89-2 1.98-2C8.06 4 9 4.91 9 6c0 .66-.33 1.25-.89 1.61-.19.13-.3.36-.25.59L9 11H5z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"sign-in\">\n    <path fill-rule=\"evenodd\"\n          d=\"M7 6.75V12h4V8h1v4c0 .55-.45 1-1 1H7v3l-5.45-2.72c-.33-.17-.55-.52-.55-.91V1c0-.55.45-1 1-1h9c.55 0 1 .45 1 1v3h-1V1H3l4 2v2.25L10 3v2h4v2h-4v2L7 6.75z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"sign-out\">\n    <path fill-rule=\"evenodd\"\n          d=\"M12 9V7H8V5h4V3l4 3-4 3zm-2 3H6V3L2 1h8v3h1V1c0-.55-.45-1-1-1H1C.45 0 0 .45 0 1v11.38c0 .39.22.73.55.91L6 16.01V13h4c.55 0 1-.45 1-1V8h-1v4z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"smiley\">\n    <path fill-rule=\"evenodd\"\n          d=\"M8 0C3.58 0 0 3.58 0 8s3.58 8 8 8 8-3.58 8-8-3.58-8-8-8zm4.81 12.81a6.72 6.72 0 0 1-2.17 1.45c-.83.36-1.72.53-2.64.53-.92 0-1.81-.17-2.64-.53-.81-.34-1.55-.83-2.17-1.45a6.773 6.773 0 0 1-1.45-2.17A6.59 6.59 0 0 1 1.21 8c0-.92.17-1.81.53-2.64.34-.81.83-1.55 1.45-2.17.62-.62 1.36-1.11 2.17-1.45A6.59 6.59 0 0 1 8 1.21c.92 0 1.81.17 2.64.53.81.34 1.55.83 2.17 1.45.62.62 1.11 1.36 1.45 2.17.36.83.53 1.72.53 2.64 0 .92-.17 1.81-.53 2.64-.34.81-.83 1.55-1.45 2.17zM4 6.8v-.59c0-.66.53-1.19 1.2-1.19h.59c.66 0 1.19.53 1.19 1.19v.59c0 .67-.53 1.2-1.19 1.2H5.2C4.53 8 4 7.47 4 6.8zm5 0v-.59c0-.66.53-1.19 1.2-1.19h.59c.66 0 1.19.53 1.19 1.19v.59c0 .67-.53 1.2-1.19 1.2h-.59C9.53 8 9 7.47 9 6.8zm4 3.2c-.72 1.88-2.91 3-5 3s-4.28-1.13-5-3c-.14-.39.23-1 .66-1h8.59c.41 0 .89.61.75 1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"squirrel\">\n    <path fill-rule=\"evenodd\"\n          d=\"M12 1C9.79 1 8 2.31 8 3.92c0 1.94.5 3.03 0 6.08 0-4.5-2.77-6.34-4-6.34.05-.5-.48-.66-.48-.66s-.22.11-.3.34c-.27-.31-.56-.27-.56-.27l-.13.58S.7 4.29.68 6.87c.2.33 1.53.6 2.47.43.89.05.67.79.47.99C2.78 9.13 2 8 1 8S0 9 1 9s1 1 3 1c-3.09 1.2 0 4 0 4H3c-1 0-1 1-1 1h6c3 0 5-1 5-3.47 0-.85-.43-1.79-1-2.53-1.11-1.46.23-2.68 1-2 .77.68 3 1 3-2 0-2.21-1.79-4-4-4zM2.5 6c-.28 0-.5-.22-.5-.5s.22-.5.5-.5.5.22.5.5-.22.5-.5.5z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"star\">\n    <path fill-rule=\"evenodd\" d=\"M14 6l-4.9-.64L7 1 4.9 5.36 0 6l3.6 3.26L2.67 14 7 11.67 11.33 14l-.93-4.74z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"stop\">\n    <path fill-rule=\"evenodd\"\n          d=\"M10 1H4L0 5v6l4 4h6l4-4V5l-4-4zm3 9.5L9.5 14h-5L1 10.5v-5L4.5 2h5L13 5.5v5zM6 4h2v5H6V4zm0 6h2v2H6v-2z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"sync\">\n    <path fill-rule=\"evenodd\"\n          d=\"M10.24 7.4a4.15 4.15 0 0 1-1.2 3.6 4.346 4.346 0 0 1-5.41.54L4.8 10.4.5 9.8l.6 4.2 1.31-1.26c2.36 1.74 5.7 1.57 7.84-.54a5.876 5.876 0 0 0 1.74-4.46l-1.75-.34zM2.96 5a4.346 4.346 0 0 1 5.41-.54L7.2 5.6l4.3.6-.6-4.2-1.31 1.26c-2.36-1.74-5.7-1.57-7.85.54C.5 5.03-.06 6.65.01 8.26l1.75.35A4.17 4.17 0 0 1 2.96 5z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"tag\">\n    <path fill-rule=\"evenodd\"\n          d=\"M7.73 1.73C7.26 1.26 6.62 1 5.96 1H3.5C2.13 1 1 2.13 1 3.5v2.47c0 .66.27 1.3.73 1.77l6.06 6.06c.39.39 1.02.39 1.41 0l4.59-4.59a.996.996 0 0 0 0-1.41L7.73 1.73zM2.38 7.09c-.31-.3-.47-.7-.47-1.13V3.5c0-.88.72-1.59 1.59-1.59h2.47c.42 0 .83.16 1.13.47l6.14 6.13-4.73 4.73-6.13-6.15zM3.01 3h2v2H3V3h.01z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"tasklist\">\n    <path fill-rule=\"evenodd\"\n          d=\"M15.41 9H7.59C7 9 7 8.59 7 8c0-.59 0-1 .59-1h7.81c.59 0 .59.41.59 1 0 .59 0 1-.59 1h.01zM9.59 4C9 4 9 3.59 9 3c0-.59 0-1 .59-1h5.81c.59 0 .59.41.59 1 0 .59 0 1-.59 1H9.59zM0 3.91l1.41-1.3L3 4.2 7.09 0 8.5 1.41 3 6.91l-3-3zM7.59 12h7.81c.59 0 .59.41.59 1 0 .59 0 1-.59 1H7.59C7 14 7 13.59 7 13c0-.59 0-1 .59-1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"telescope\">\n    <path fill-rule=\"evenodd\"\n          d=\"M8 9l3 6h-1l-2-4v5H7v-6l-2 5H4l2-5 2-1zM7 0H6v1h1V0zM5 3H4v1h1V3zM2 1H1v1h1V1zM.63 9a.52.52 0 0 0-.16.67l.55.92c.13.23.41.31.64.2l1.39-.66-1.16-2-1.27.86.01.01zm7.89-5.39l-5.8 3.95L3.95 9.7l6.33-3.03-1.77-3.06h.01zm4.22 1.28l-1.47-2.52a.51.51 0 0 0-.72-.17l-1.2.83 1.84 3.2 1.33-.64c.27-.13.36-.44.22-.7z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"terminal\">\n    <path fill-rule=\"evenodd\"\n          d=\"M7 10h4v1H7v-1zm-3 1l3-3-3-3-.75.75L5.5 8l-2.25 2.25L4 11zm10-8v10c0 .55-.45 1-1 1H1c-.55 0-1-.45-1-1V3c0-.55.45-1 1-1h12c.55 0 1 .45 1 1zm-1 0H1v10h12V3z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 18 16\" id=\"text-size\">\n    <path fill-rule=\"evenodd\"\n          d=\"M13.62 9.08L12.1 3.66h-.06l-1.5 5.42h3.08zM5.7 10.13S4.68 6.52 4.53 6.02h-.08l-1.13 4.11H5.7zM17.31 14h-2.25l-.95-3.25h-4.07L9.09 14H6.84l-.69-2.33H2.87L2.17 14H0l3.3-9.59h2.5l2.17 6.34L10.86 2h2.52l3.94 12h-.01z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"three-bars\">\n    <path fill-rule=\"evenodd\"\n          d=\"M11.41 9H.59C0 9 0 8.59 0 8c0-.59 0-1 .59-1H11.4c.59 0 .59.41.59 1 0 .59 0 1-.59 1h.01zm0-4H.59C0 5 0 4.59 0 4c0-.59 0-1 .59-1H11.4c.59 0 .59.41.59 1 0 .59 0 1-.59 1h.01zM.59 11H11.4c.59 0 .59.41.59 1 0 .59 0 1-.59 1H.59C0 13 0 12.59 0 12c0-.59 0-1 .59-1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"thumbsdown\">\n    <path fill-rule=\"evenodd\"\n          d=\"M15.98 7.83l-.97-5.95C14.84.5 13.13 0 12 0H5.69c-.2 0-.38.05-.53.14L3.72 1H2C.94 1 0 1.94 0 3v4c0 1.06.94 2.02 2 2h2c.91 0 1.39.45 2.39 1.55.91 1 .88 1.8.63 3.27-.08.5.06 1 .42 1.42.39.47.98.77 1.56.77 1.83 0 3-3.72 3-5.02l-.02-.98h2.04c1.16 0 1.95-.8 1.98-1.97 0-.06.02-.13-.02-.2v-.01zm-1.97 1.19h-1.99c-.7 0-1.03.28-1.03.97l.03 1.03c0 1.27-1.17 4-2 4-.5 0-1.08-.5-1-1 .25-1.58.34-2.78-.89-4.14C6.11 8.75 5.36 8 4 8V2l1.67-1H12c.73 0 1.95.31 2 1l.02.02 1 6c-.03.64-.38 1-1 1h-.01z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"thumbsup\">\n    <path fill-rule=\"evenodd\"\n          d=\"M14 14c-.05.69-1.27 1-2 1H5.67L4 14V8c1.36 0 2.11-.75 3.13-1.88 1.23-1.36 1.14-2.56.88-4.13-.08-.5.5-1 1-1 .83 0 2 2.73 2 4l-.02 1.03c0 .69.33.97 1.02.97h2c.63 0 .98.36 1 1l-1 6L14 14zm0-8h-2.02l.02-.98C12 3.72 10.83 0 9 0c-.58 0-1.17.3-1.56.77-.36.41-.5.91-.42 1.41.25 1.48.28 2.28-.63 3.28-1 1.09-1.48 1.55-2.39 1.55H2C.94 7 0 7.94 0 9v4c0 1.06.94 2 2 2h1.72l1.44.86c.16.09.33.14.52.14h6.33c1.13 0 2.84-.5 3-1.88l.98-5.95c.02-.08.02-.14.02-.2-.03-1.17-.84-1.97-2-1.97H14z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"tools\">\n    <path fill-rule=\"evenodd\"\n          d=\"M4.48 7.27c.26.26 1.28 1.33 1.28 1.33l.56-.58-.88-.91 1.69-1.8s-.76-.74-.43-.45c.32-1.19.03-2.51-.87-3.44C4.93.5 3.66.2 2.52.51l1.93 2-.51 1.96-1.89.52-1.93-2C-.19 4.17.1 5.48 1 6.4c.94.98 2.29 1.26 3.48.87zm6.44 1.94l-2.33 2.3 3.84 3.98c.31.33.73.49 1.14.49.41 0 .82-.16 1.14-.49.63-.65.63-1.7 0-2.35l-3.79-3.93zM16 2.53L13.55 0 6.33 7.46l.88.91-4.31 4.46-.99.53-1.39 2.27.35.37 2.2-1.44.51-1.02L7.9 9.08l.88.91L16 2.53z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"trashcan\">\n    <path fill-rule=\"evenodd\"\n          d=\"M11 2H9c0-.55-.45-1-1-1H5c-.55 0-1 .45-1 1H2c-.55 0-1 .45-1 1v1c0 .55.45 1 1 1v9c0 .55.45 1 1 1h7c.55 0 1-.45 1-1V5c.55 0 1-.45 1-1V3c0-.55-.45-1-1-1zm-1 12H3V5h1v8h1V5h1v8h1V5h1v8h1V5h1v9zm1-10H2V3h9v1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"triangle-down\">\n    <path fill-rule=\"evenodd\" d=\"M0 5l6 6 6-6z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 6 16\" id=\"triangle-left\">\n    <path fill-rule=\"evenodd\" d=\"M6 2L0 8l6 6z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 6 16\" id=\"triangle-right\">\n    <path fill-rule=\"evenodd\" d=\"M0 14l6-6-6-6z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"triangle-up\">\n    <path fill-rule=\"evenodd\" d=\"M12 11L6 5l-6 6z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"unfold\">\n    <path fill-rule=\"evenodd\"\n          d=\"M11.5 7.5L14 10c0 .55-.45 1-1 1H9v-1h3.5l-2-2h-7l-2 2H5v1H1c-.55 0-1-.45-1-1l2.5-2.5L0 5c0-.55.45-1 1-1h4v1H1.5l2 2h7l2-2H9V4h4c.55 0 1 .45 1 1l-2.5 2.5zM6 6h2V3h2L7 0 4 3h2v3zm2 3H6v3H4l3 3 3-3H8V9z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"unmute\">\n    <path fill-rule=\"evenodd\"\n          d=\"M12 8.02c0 1.09-.45 2.09-1.17 2.83l-.67-.67c.55-.56.89-1.31.89-2.16 0-.85-.34-1.61-.89-2.16l.67-.67A3.99 3.99 0 0 1 12 8.02zM7.72 2.28L4 6H2c-.55 0-1 .45-1 1v2c0 .55.45 1 1 1h2l3.72 3.72c.47.47 1.28.14 1.28-.53V2.81c0-.67-.81-1-1.28-.53zm5.94.08l-.67.67a6.996 6.996 0 0 1 2.06 4.98c0 1.94-.78 3.7-2.06 4.98l.67.67A7.973 7.973 0 0 0 16 8c0-2.22-.89-4.22-2.34-5.66v.02zm-1.41 1.41l-.69.67a5.05 5.05 0 0 1 1.48 3.58c0 1.39-.56 2.66-1.48 3.56l.69.67A5.971 5.971 0 0 0 14 8.02c0-1.65-.67-3.16-1.75-4.25z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"unverified\">\n    <path fill-rule=\"evenodd\"\n          d=\"M15.67 7.06l-1.08-1.34c-.17-.22-.28-.48-.31-.77l-.19-1.7a1.51 1.51 0 0 0-1.33-1.33l-1.7-.19c-.3-.03-.56-.16-.78-.33L8.94.32c-.55-.44-1.33-.44-1.88 0L5.72 1.4c-.22.17-.48.28-.77.31l-1.7.19c-.7.08-1.25.63-1.33 1.33l-.19 1.7c-.03.3-.16.56-.33.78L.32 7.05c-.44.55-.44 1.33 0 1.88l1.08 1.34c.17.22.28.48.31.77l.19 1.7c.08.7.63 1.25 1.33 1.33l1.7.19c.3.03.56.16.78.33l1.34 1.08c.55.44 1.33.44 1.88 0l1.34-1.08c.22-.17.48-.28.77-.31l1.7-.19c.7-.08 1.25-.63 1.33-1.33l.19-1.7c.03-.3.16-.56.33-.78l1.08-1.34c.44-.55.44-1.33 0-1.88zM9 11.5c0 .28-.22.5-.5.5h-1c-.27 0-.5-.22-.5-.5v-1c0-.28.23-.5.5-.5h1c.28 0 .5.22.5.5v1zm1.56-4.89c-.06.17-.17.33-.3.47-.13.16-.14.19-.33.38-.16.17-.31.3-.52.45-.11.09-.2.19-.28.27-.08.08-.14.17-.19.27-.05.1-.08.19-.11.3-.03.11-.03.13-.03.25H7.13c0-.22 0-.31.03-.48.03-.19.08-.36.14-.52.06-.14.14-.28.25-.42.11-.13.23-.25.41-.38.27-.19.36-.3.48-.52.12-.22.2-.38.2-.59 0-.27-.06-.45-.2-.58-.13-.13-.31-.19-.58-.19-.09 0-.19.02-.3.05-.11.03-.17.09-.25.16-.08.07-.14.11-.2.2a.41.41 0 0 0-.09.28h-2c0-.38.13-.56.27-.83.16-.27.36-.5.61-.67.25-.17.55-.3.88-.38.33-.08.7-.13 1.09-.13.44 0 .83.05 1.17.13.34.09.63.22.88.39.23.17.41.38.55.63.13.25.19.55.19.88 0 .22 0 .42-.08.59l-.02-.01z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 16 16\" id=\"verified\">\n    <path fill-rule=\"evenodd\"\n          d=\"M15.67 7.06l-1.08-1.34c-.17-.22-.28-.48-.31-.77l-.19-1.7a1.51 1.51 0 0 0-1.33-1.33l-1.7-.19c-.3-.03-.56-.16-.78-.33L8.94.32c-.55-.44-1.33-.44-1.88 0L5.72 1.4c-.22.17-.48.28-.77.31l-1.7.19c-.7.08-1.25.63-1.33 1.33l-.19 1.7c-.03.3-.16.56-.33.78L.32 7.05c-.44.55-.44 1.33 0 1.88l1.08 1.34c.17.22.28.48.31.77l.19 1.7c.08.7.63 1.25 1.33 1.33l1.7.19c.3.03.56.16.78.33l1.34 1.08c.55.44 1.33.44 1.88 0l1.34-1.08c.22-.17.48-.28.77-.31l1.7-.19c.7-.08 1.25-.63 1.33-1.33l.19-1.7c.03-.3.16-.56.33-.78l1.08-1.34c.44-.55.44-1.33 0-1.88zM6.5 12L3 8.5 4.5 7l2 2 5-5L13 5.55 6.5 12z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 14 16\" id=\"versions\">\n    <path fill-rule=\"evenodd\"\n          d=\"M13 3H7c-.55 0-1 .45-1 1v8c0 .55.45 1 1 1h6c.55 0 1-.45 1-1V4c0-.55-.45-1-1-1zm-1 8H8V5h4v6zM4 4h1v1H4v6h1v1H4c-.55 0-1-.45-1-1V5c0-.55.45-1 1-1zM1 5h1v1H1v4h1v1H1c-.55 0-1-.45-1-1V6c0-.55.45-1 1-1z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"watch\">\n    <path fill-rule=\"evenodd\"\n          d=\"M6 8h2v1H5V5h1v3zm6 0c0 2.22-1.2 4.16-3 5.19V15c0 .55-.45 1-1 1H4c-.55 0-1-.45-1-1v-1.81C1.2 12.16 0 10.22 0 8s1.2-4.16 3-5.19V1c0-.55.45-1 1-1h4c.55 0 1 .45 1 1v1.81c1.8 1.03 3 2.97 3 5.19zm-1 0c0-2.77-2.23-5-5-5S1 5.23 1 8s2.23 5 5 5 5-2.23 5-5z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 12 16\" id=\"x\">\n    <path fill-rule=\"evenodd\"\n          d=\"M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48z\"/>\n  </symbol>\n  <symbol viewBox=\"0 0 10 16\" id=\"zap\">\n    <path fill-rule=\"evenodd\" d=\"M10 7H6l3-7-9 9h4l-3 7z\"/>\n  </symbol>\n</svg>\n"

/***/ }),

/***/ "../../../../../src/app/core/icon-set/icon-set.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  display: none; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/core/icon-set/icon-set.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IconSetComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var IconSetComponent = (function () {
    function IconSetComponent() {
    }
    IconSetComponent.prototype.ngOnInit = function () {
    };
    return IconSetComponent;
}());
IconSetComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-icon-set',
        template: __webpack_require__("../../../../../src/app/core/icon-set/icon-set.component.html"),
        styles: [__webpack_require__("../../../../../src/app/core/icon-set/icon-set.component.scss")]
    }),
    __metadata("design:paramtypes", [])
], IconSetComponent);

//# sourceMappingURL=icon-set.component.js.map

/***/ }),

/***/ "../../../../../src/app/core/top-bar/top-bar.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"group\">\n  <div class=\"logo\" (click)=\"onLogoClick();\">\n    <div class=\"image\">\n      <img src=\"assets/images/logo.svg\">\n    </div>\n    <div class=\"name\">Tempest</div>\n  </div>\n  <nav>\n    <a routerLink=\"dashboard\" routerLinkActive=\"active\">Dashboard</a>\n    <a routerLink=\"user/list\" routerLinkActive=\"active\">Users</a>\n    <a routerLink=\"card-set/list\" routerLinkActive=\"active\">Cardsets</a>\n  </nav>\n</div>\n<div class=\"group\">\n  <div class=\"user\" *ngIf=\"_user\">\n    <div class=\"name\">{{_user.displayName}}</div>\n    <div class=\"controls\">\n      <button (click)=\"onLogoutClick();\">Log out</button>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/core/top-bar/top-bar.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: stretch;\n      -ms-flex-align: stretch;\n          align-items: stretch;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  position: fixed;\n  z-index: 100;\n  top: 0;\n  left: 0;\n  right: 0;\n  background: #444644;\n  height: 3.2em;\n  box-shadow: 0 0 5px 1px #1a1919; }\n  :host.flex-end {\n    -webkit-box-pack: end;\n        -ms-flex-pack: end;\n            justify-content: flex-end; }\n  :host > * {\n    -webkit-box-flex: 1;\n        -ms-flex: 1 1 auto;\n            flex: 1 1 auto; }\n    :host > *.no-flex {\n      -webkit-box-flex: 0;\n          -ms-flex: none;\n              flex: none; }\n\n.group {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: stretch;\n      -ms-flex-align: stretch;\n          align-items: stretch;\n  -webkit-box-flex: 0;\n      -ms-flex-positive: 0;\n          flex-grow: 0;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n  .group.flex-end {\n    -webkit-box-pack: end;\n        -ms-flex-pack: end;\n            justify-content: flex-end; }\n  .group > * {\n    -webkit-box-flex: 1;\n        -ms-flex: 1 1 auto;\n            flex: 1 1 auto; }\n    .group > *.no-flex {\n      -webkit-box-flex: 0;\n          -ms-flex: none;\n              flex: none; }\n  .group .logo {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: stretch;\n        -ms-flex-align: stretch;\n            align-items: stretch;\n    height: 100%;\n    padding: 0 0.6em;\n    margin-right: 2em;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    cursor: pointer;\n    -webkit-user-select: none;\n       -moz-user-select: none;\n        -ms-user-select: none;\n            user-select: none; }\n    .group .logo.flex-end {\n      -webkit-box-pack: end;\n          -ms-flex-pack: end;\n              justify-content: flex-end; }\n    .group .logo > * {\n      -webkit-box-flex: 1;\n          -ms-flex: 1 1 auto;\n              flex: 1 1 auto; }\n      .group .logo > *.no-flex {\n        -webkit-box-flex: 0;\n            -ms-flex: none;\n                flex: none; }\n    .group .logo .image {\n      margin-right: 0.4em; }\n      .group .logo .image img {\n        display: block;\n        height: 1.8em; }\n    .group .logo .name {\n      font-size: 1.2em; }\n  .group nav {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: stretch;\n        -ms-flex-align: stretch;\n            align-items: stretch;\n    height: 100%; }\n    .group nav.flex-end {\n      -webkit-box-pack: end;\n          -ms-flex-pack: end;\n              justify-content: flex-end; }\n    .group nav > * {\n      -webkit-box-flex: 1;\n          -ms-flex: 1 1 auto;\n              flex: 1 1 auto; }\n      .group nav > *.no-flex {\n        -webkit-box-flex: 0;\n            -ms-flex: none;\n                flex: none; }\n    .group nav a {\n      height: 100%;\n      line-height: 3.2em;\n      padding: 0 1em;\n      background-color: #4c4e4c; }\n      .group nav a.active {\n        color: #f2f2f2;\n        background-color: #5d605d; }\n      .group nav a:hover {\n        color: #e6e5e5;\n        background-color: #6a6d6a; }\n  .group .user {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: stretch;\n        -ms-flex-align: stretch;\n            align-items: stretch;\n    margin: 0 1em; }\n    .group .user.flex-end {\n      -webkit-box-pack: end;\n          -ms-flex-pack: end;\n              justify-content: flex-end; }\n    .group .user > * {\n      -webkit-box-flex: 1;\n          -ms-flex: 1 1 auto;\n              flex: 1 1 auto; }\n      .group .user > *.no-flex {\n        -webkit-box-flex: 0;\n            -ms-flex: none;\n                flex: none; }\n    .group .user .name {\n      margin: 0 1em; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/core/top-bar/top-bar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user_user_service__ = __webpack_require__("../../../../../src/app/user/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TopBarComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TopBarComponent = (function () {
    function TopBarComponent(_router, _userService) {
        var _this = this;
        this._router = _router;
        this._userService = _userService;
        this._userService.user$
            .subscribe(function (user) { return _this._user = user; });
    }
    TopBarComponent.prototype.ngOnInit = function () {
    };
    TopBarComponent.prototype.onLogoClick = function () {
        this._router.navigate(['/dashboard']);
    };
    TopBarComponent.prototype.onLogoutClick = function () {
        this._userService.logout();
    };
    return TopBarComponent;
}());
TopBarComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-top-bar',
        template: __webpack_require__("../../../../../src/app/core/top-bar/top-bar.component.html"),
        styles: [__webpack_require__("../../../../../src/app/core/top-bar/top-bar.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__user_user_service__["a" /* UserService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__user_user_service__["a" /* UserService */]) === "function" && _b || Object])
], TopBarComponent);

var _a, _b;
//# sourceMappingURL=top-bar.component.js.map

/***/ }),

/***/ "../../../../../src/app/dashboard/dashboard.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__dashboard_dashboard_component__ = __webpack_require__("../../../../../src/app/dashboard/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__routes__ = __webpack_require__("../../../../../src/app/dashboard/routes.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_core_module__ = __webpack_require__("../../../../../src/app/core/core.module.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var DashboardModule = (function () {
    function DashboardModule() {
    }
    return DashboardModule;
}());
DashboardModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_4__routes__["a" /* routes */]),
            __WEBPACK_IMPORTED_MODULE_6__shared_shared_module__["a" /* SharedModule */],
            __WEBPACK_IMPORTED_MODULE_7__core_core_module__["a" /* CoreModule */]
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_3__dashboard_dashboard_component__["a" /* DashboardComponent */]]
    })
], DashboardModule);

//# sourceMappingURL=dashboard.module.js.map

/***/ }),

/***/ "../../../../../src/app/dashboard/dashboard/dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<app-title-bar [title]=\"'Dashboard'\"></app-title-bar>\n"

/***/ }),

/***/ "../../../../../src/app/dashboard/dashboard/dashboard.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: stretch;\n      -ms-flex-align: stretch;\n          align-items: stretch;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  padding: 5.2em 2em 2em; }\n  :host.flex-end {\n    -webkit-box-pack: end;\n        -ms-flex-pack: end;\n            justify-content: flex-end; }\n  :host > * {\n    -webkit-box-flex: 1;\n        -ms-flex: 1 1 auto;\n            flex: 1 1 auto; }\n    :host > *.no-flex {\n      -webkit-box-flex: 0;\n          -ms-flex: none;\n              flex: none; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/dashboard/dashboard/dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DashboardComponent = (function () {
    function DashboardComponent() {
    }
    DashboardComponent.prototype.ngOnInit = function () {
    };
    return DashboardComponent;
}());
DashboardComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-dashboard',
        template: __webpack_require__("../../../../../src/app/dashboard/dashboard/dashboard.component.html"),
        styles: [__webpack_require__("../../../../../src/app/dashboard/dashboard/dashboard.component.scss")]
    }),
    __metadata("design:paramtypes", [])
], DashboardComponent);

//# sourceMappingURL=dashboard.component.js.map

/***/ }),

/***/ "../../../../../src/app/dashboard/routes.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__dashboard_dashboard_component__ = __webpack_require__("../../../../../src/app/dashboard/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user_auth_guard_service__ = __webpack_require__("../../../../../src/app/user/auth-guard.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routes; });


var routes = [
    {
        path: 'dashboard',
        component: __WEBPACK_IMPORTED_MODULE_0__dashboard_dashboard_component__["a" /* DashboardComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_1__user_auth_guard_service__["a" /* AuthGuard */]]
    }
];
//# sourceMappingURL=routes.js.map

/***/ }),

/***/ "../../../../../src/app/routes.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routes; });
var routes = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
    }
];
//# sourceMappingURL=routes.js.map

/***/ }),

/***/ "../../../../../src/app/shared/api.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ApiService = (function () {
    function ApiService(_http) {
        this._http = _http;
        this.base = 'http://91.219.209.131:3002';
        //public base: string = 'http://localhost:3002';
        this.URL = this.base + '/api/';
    }
    ApiService.prototype.makeRequest = function (path, type, params, body) {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]();
        options.withCredentials = true;
        if (params)
            options.params = params;
        var observable;
        switch (type) {
            case __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* RequestMethod */].Get:
                observable = this._http.get(this.URL + path, options);
                break;
            case __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* RequestMethod */].Delete:
                observable = this._http.delete(this.URL + path, options);
                break;
            case __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* RequestMethod */].Post:
                observable = this._http.post(this.URL + path, body, options);
                break;
            case __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* RequestMethod */].Put:
                observable = this._http.put(this.URL + path, body, options);
                break;
        }
        return observable
            .map(function (res) { return _this.onResponse(res); })
            .catch(function (error) { return _this.onError(error); });
    };
    ApiService.prototype.onResponse = function (res) {
        if (res.ok)
            return res.json();
        return this.onError(res);
    };
    ApiService.prototype.onError = function (error) {
        var message = 'An error occurred.';
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(message);
    };
    return ApiService;
}());
ApiService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object])
], ApiService);

var _a;
//# sourceMappingURL=api.service.js.map

/***/ }),

/***/ "../../../../../src/app/shared/button/button.component.html":
/***/ (function(module, exports) {

module.exports = "<button [type]=\"type\" [ngClass]=\"cssClass\">\r\n  <app-icon [name]=\"icon\" [size]=\"size\"></app-icon>\r\n  <div class=\"caption\">{{caption}}</div>\r\n</button>\r\n"

/***/ }),

/***/ "../../../../../src/app/shared/button/button.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  display: -webkit-inline-box;\n  display: -ms-inline-flexbox;\n  display: inline-flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n  :host button {\n    background-color: #FFB855;\n    color: #FFFFFF;\n    font-size: 0.95em;\n    padding: 0.4em 1.2em;\n    border-radius: 0.2em;\n    text-transform: uppercase;\n    font-weight: bold;\n    border: 0 none;\n    cursor: pointer;\n    outline: 0 none;\n    -webkit-user-select: none;\n       -moz-user-select: none;\n        -ms-user-select: none;\n            user-select: none;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: justify;\n        -ms-flex-pack: justify;\n            justify-content: space-between;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    padding: 0.4em 0.8em; }\n    :host button:hover {\n      background-color: #ffad3c; }\n    :host button:active {\n      background-color: #ffa322;\n      -webkit-transform: translate(0, 1px);\n              transform: translate(0, 1px); }\n    :host button.no-background {\n      background: none; }\n      :host button.no-background:hover {\n        background: none; }\n      :host button.no-background:active {\n        background: none;\n        -webkit-transform: translate(0, 1px);\n                transform: translate(0, 1px); }\n    :host button .caption {\n      margin-left: 0.2em; }\n  :host.size-tiny button {\n    padding: 0.1em 0.2em; }\n  :host.size-tiny .caption {\n    display: none; }\n  :host.size-small button {\n    padding: 0.2em 0.6em; }\n    :host.size-small button .caption {\n      margin-top: 0.1em;\n      margin-left: 0.2em;\n      font-size: 0.8em; }\n  :host.size-full button {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    width: 100%;\n    height: 100%;\n    padding: 1em; }\n    :host.size-full button .caption {\n      margin-left: 0.8em;\n      font-size: 1.2em; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shared/button/button.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ButtonComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ButtonComponent = (function () {
    function ButtonComponent() {
        this.hostClass = '';
    }
    ButtonComponent.prototype.ngOnInit = function () {
        if (!this.type) {
            this.type = 'button';
        }
        switch (this.size) {
            case 'tiny':
                this.hostClass = 'size-tiny';
                break;
            case 'small':
                this.hostClass = 'size-small';
                break;
            case 'big':
                this.hostClass = 'size-big';
                break;
            case 'full':
                this.hostClass = 'size-full';
                break;
            default:
                this.hostClass = 'size-default';
        }
    };
    return ButtonComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], ButtonComponent.prototype, "icon", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], ButtonComponent.prototype, "caption", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], ButtonComponent.prototype, "type", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], ButtonComponent.prototype, "size", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], ButtonComponent.prototype, "cssClass", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostBinding"])('class'),
    __metadata("design:type", String)
], ButtonComponent.prototype, "hostClass", void 0);
ButtonComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-button',
        template: __webpack_require__("../../../../../src/app/shared/button/button.component.html"),
        styles: [__webpack_require__("../../../../../src/app/shared/button/button.component.scss")]
    }),
    __metadata("design:paramtypes", [])
], ButtonComponent);

//# sourceMappingURL=button.component.js.map

/***/ }),

/***/ "../../../../../src/app/shared/color-badge/color-badge.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"color\" *ngIf=\"color\">\n  <div class=\"box\" [ngStyle]=\"{'background-color': color}\"></div>\n  <div class=\"name\">{{color}}</div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/shared/color-badge/color-badge.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".color {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: stretch;\n      -ms-flex-align: stretch;\n          align-items: stretch;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n  .color.flex-end {\n    -webkit-box-pack: end;\n        -ms-flex-pack: end;\n            justify-content: flex-end; }\n  .color > * {\n    -webkit-box-flex: 1;\n        -ms-flex: 1 1 auto;\n            flex: 1 1 auto; }\n    .color > *.no-flex {\n      -webkit-box-flex: 0;\n          -ms-flex: none;\n              flex: none; }\n  .color .box {\n    -webkit-box-flex: 0;\n        -ms-flex-positive: 0;\n            flex-grow: 0;\n    width: 1em;\n    height: 1em;\n    margin-right: 0.6em; }\n  .color .name {\n    -webkit-box-flex: 0;\n        -ms-flex-positive: 0;\n            flex-grow: 0; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shared/color-badge/color-badge.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ColorBadgeComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ColorBadgeComponent = (function () {
    function ColorBadgeComponent() {
    }
    ColorBadgeComponent.prototype.ngOnInit = function () {
    };
    return ColorBadgeComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], ColorBadgeComponent.prototype, "color", void 0);
ColorBadgeComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-color-badge',
        template: __webpack_require__("../../../../../src/app/shared/color-badge/color-badge.component.html"),
        styles: [__webpack_require__("../../../../../src/app/shared/color-badge/color-badge.component.scss")]
    }),
    __metadata("design:paramtypes", [])
], ColorBadgeComponent);

//# sourceMappingURL=color-badge.component.js.map

/***/ }),

/***/ "../../../../../src/app/shared/enum.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EnumPipe; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var EnumPipe = (function () {
    function EnumPipe() {
    }
    EnumPipe.prototype.transform = function (enumObject) {
        var keys = Object.keys(enumObject);
        return keys.slice(keys.length / 2, keys.length);
    };
    return EnumPipe;
}());
EnumPipe = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
        name: 'enum'
    })
], EnumPipe);

//# sourceMappingURL=enum.pipe.js.map

/***/ }),

/***/ "../../../../../src/app/shared/http-tools.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpToolsService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var HttpToolsService = (function () {
    function HttpToolsService() {
    }
    HttpToolsService.validUrl = function (url) {
        var urlregex = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;
        return urlregex.test(url);
    };
    return HttpToolsService;
}());
HttpToolsService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
], HttpToolsService);

//# sourceMappingURL=http-tools.service.js.map

/***/ }),

/***/ "../../../../../src/app/shared/icon/icon.component.html":
/***/ (function(module, exports) {

module.exports = "<svg class=\"icon\" [ngClass]=\"name\">\r\n  <use [attr.xlink:href]=\"'#' + name\"></use>\r\n</svg>\r\n"

/***/ }),

/***/ "../../../../../src/app/shared/icon/icon.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  display: block;\n  width: 20px;\n  height: 20px; }\n  :host .icon {\n    display: block;\n    fill: currentColor;\n    width: 20px;\n    height: 20px; }\n  :host.size-tiny {\n    width: 12px;\n    height: 12px; }\n    :host.size-tiny .icon {\n      width: 12px;\n      height: 12px; }\n  :host.size-small {\n    width: 16px;\n    height: 16px; }\n    :host.size-small .icon {\n      width: 16px;\n      height: 16px; }\n  :host.size-big {\n    width: 32px;\n    height: 32px; }\n    :host.size-big .icon {\n      width: 32px;\n      height: 32px; }\n  :host.size-large {\n    width: 48px;\n    height: 48px; }\n    :host.size-large .icon {\n      width: 48px;\n      height: 48px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shared/icon/icon.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IconComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var IconComponent = (function () {
    function IconComponent() {
        this.hostClass = '';
    }
    IconComponent.prototype.ngOnInit = function () {
        switch (this.size) {
            case 'tiny':
                this.hostClass = 'size-tiny';
                break;
            case 'small':
                this.hostClass = 'size-small';
                break;
            case 'big':
                this.hostClass = 'size-big';
                break;
            case 'large':
                this.hostClass = 'size-large';
                break;
            default:
                this.hostClass = 'size-default';
        }
    };
    return IconComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], IconComponent.prototype, "name", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], IconComponent.prototype, "size", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostBinding"])('class'),
    __metadata("design:type", String)
], IconComponent.prototype, "hostClass", void 0);
IconComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-icon',
        template: __webpack_require__("../../../../../src/app/shared/icon/icon.component.html"),
        styles: [__webpack_require__("../../../../../src/app/shared/icon/icon.component.scss")]
    }),
    __metadata("design:paramtypes", [])
], IconComponent);

//# sourceMappingURL=icon.component.js.map

/***/ }),

/***/ "../../../../../src/app/shared/info-badge/info-badge.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"time\" *ngIf=\"time\">{{time | date:'medium'}}</div>\n<div class=\"user\" *ngIf=\"user\">by {{user.displayName}}</div>\n"

/***/ }),

/***/ "../../../../../src/app/shared/info-badge/info-badge.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  display: block;\n  font-size: 0.8em; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shared/info-badge/info-badge.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user_user__ = __webpack_require__("../../../../../src/app/user/user.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InfoBadgeComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var InfoBadgeComponent = (function () {
    function InfoBadgeComponent() {
    }
    InfoBadgeComponent.prototype.ngOnInit = function () {
    };
    return InfoBadgeComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], InfoBadgeComponent.prototype, "time", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__user_user__["a" /* User */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__user_user__["a" /* User */]) === "function" && _a || Object)
], InfoBadgeComponent.prototype, "user", void 0);
InfoBadgeComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-info-badge',
        template: __webpack_require__("../../../../../src/app/shared/info-badge/info-badge.component.html"),
        styles: [__webpack_require__("../../../../../src/app/shared/info-badge/info-badge.component.scss")]
    }),
    __metadata("design:paramtypes", [])
], InfoBadgeComponent);

var _a;
//# sourceMappingURL=info-badge.component.js.map

/***/ }),

/***/ "../../../../../src/app/shared/input-upload/input-upload.component.html":
/***/ (function(module, exports) {

module.exports = "<label>{{label}}</label>\r\n<div class=\"container\">\r\n  <div\r\n    ng2FileDrop\r\n    class=\"drop-zone\"\r\n    [ngClass]=\"{'file-over': _isFileOverDropZone}\"\r\n    (fileOver)=\"onFileOverDropZone($event)\"\r\n    [uploader]=\"_uploader\"\r\n  >\r\n    <div\r\n      class=\"current\"\r\n      *ngIf=\"model\"\r\n    >\r\n      <img\r\n        *ngIf=\"imageUrl\"\r\n        src=\"{{imageUrl + model}}\"\r\n        crossorigin=\"use-credentials\"\r\n      >\r\n      <div class=\"filename\">Current file: {{model}}</div>\r\n    </div>\r\n    <div class=\"info\">Drag & drop file here or click below</div>\r\n  </div>\r\n  <input type=\"file\" ng2FileSelect [uploader]=\"_uploader\">\r\n  <div class=\"overlay\" *ngIf=\"_uploader.isUploading\">\r\n    <div class=\"progress\">\r\n      <div\r\n        class=\"progress-bar\"\r\n        role=\"progressbar\"\r\n        [ngStyle]=\"{ 'width': _progress + '%' }\"\r\n      ></div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/shared/input-upload/input-upload.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: stretch;\n      -ms-flex-align: stretch;\n          align-items: stretch;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-flex: 0;\n      -ms-flex-positive: 0;\n          flex-grow: 0;\n  margin-bottom: 1em; }\n  :host.flex-end {\n    -webkit-box-pack: end;\n        -ms-flex-pack: end;\n            justify-content: flex-end; }\n  :host > * {\n    -webkit-box-flex: 1;\n        -ms-flex: 1 1 auto;\n            flex: 1 1 auto; }\n    :host > *.no-flex {\n      -webkit-box-flex: 0;\n          -ms-flex: none;\n              flex: none; }\n  :host label {\n    text-transform: uppercase; }\n  :host .container {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: stretch;\n        -ms-flex-align: stretch;\n            align-items: stretch;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    position: relative; }\n    :host .container.flex-end {\n      -webkit-box-pack: end;\n          -ms-flex-pack: end;\n              justify-content: flex-end; }\n    :host .container > * {\n      -webkit-box-flex: 1;\n          -ms-flex: 1 1 auto;\n              flex: 1 1 auto; }\n      :host .container > *.no-flex {\n        -webkit-box-flex: 0;\n            -ms-flex: none;\n                flex: none; }\n    :host .container .drop-zone {\n      background: #5d605d;\n      padding: 1em;\n      min-height: 4em;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: column;\n              flex-direction: column;\n      -webkit-box-pack: center;\n          -ms-flex-pack: center;\n              justify-content: center;\n      -webkit-box-align: center;\n          -ms-flex-align: center;\n              align-items: center;\n      -webkit-user-select: none;\n         -moz-user-select: none;\n          -ms-user-select: none;\n              user-select: none;\n      cursor: default; }\n      :host .container .drop-zone.file-over {\n        background: #838783;\n        outline: 2px dashed #FFFFFF; }\n      :host .container .drop-zone .current {\n        text-align: center; }\n        :host .container .drop-zone .current img {\n          max-height: 300px;\n          max-width: 100%; }\n      :host .container .drop-zone .info {\n        text-align: center; }\n    :host .container input[type=file] {\n      height: auto;\n      margin: 0.6em 0;\n      line-height: 1.4em;\n      padding: 0.4em;\n      cursor: pointer;\n      outline: 0 none; }\n      :host .container input[type=file]:hover {\n        background: #838783; }\n    :host .container .overlay {\n      background: rgba(255, 255, 255, 0.6);\n      position: absolute;\n      top: 0;\n      left: 0;\n      right: 0;\n      bottom: 0;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: column;\n              flex-direction: column;\n      -webkit-box-pack: center;\n          -ms-flex-pack: center;\n              justify-content: center;\n      -webkit-box-align: center;\n          -ms-flex-align: center;\n              align-items: center; }\n      :host .container .overlay .progress {\n        width: 40%;\n        height: 2em;\n        margin: 0.6em 0;\n        background: #444644; }\n        :host .container .overlay .progress .progress-bar {\n          height: 100%;\n          transition: width 100ms linear;\n          background: #FFB855; }\n  :host.size-small {\n    font-size: 0.75em; }\n    :host.size-small .container .drop-zone .current img {\n      max-height: 160px; }\n    :host.size-small .container .drop-zone .current .filename {\n      font-size: 0.8em; }\n    :host.size-small .container .drop-zone .info {\n      font-size: 0.8em; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shared/input-upload/input-upload.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ng2_file_upload__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputUploadComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var InputUploadComponent = (function () {
    function InputUploadComponent(_ngZone) {
        this._ngZone = _ngZone;
        this.modelChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.hostClass = '';
        this._isFileOverDropZone = false;
        this._progress = 0;
    }
    InputUploadComponent.prototype.ngOnInit = function () {
        this.initUploader();
        if (this.single != true)
            this.single = false;
        switch (this.size) {
            case 'small':
                this.hostClass = 'size-small';
                break;
            case 'big':
                this.hostClass = 'size-big';
                break;
            default:
                this.hostClass = 'size-default';
        }
    };
    InputUploadComponent.prototype.initUploader = function () {
        var _this = this;
        this._uploader = new __WEBPACK_IMPORTED_MODULE_1_ng2_file_upload__["FileUploader"](this.options);
        this._uploader.onProgressAll = function (progress) {
            _this._ngZone.run(function () { return _this._progress = progress; });
        };
        this._uploader.onCompleteItem = function (item, responseString) {
            _this._ngZone.run(function () {
                _this._progress = 0;
                var response = JSON.parse(responseString);
                _this.model = response.fileName;
                _this.modelChange.emit(_this.model);
                if (_this.single) {
                    _this.model = null;
                }
            });
        };
    };
    InputUploadComponent.prototype.onFileOverDropZone = function (isOver) {
        this._isFileOverDropZone = isOver;
    };
    return InputUploadComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], InputUploadComponent.prototype, "model", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _a || Object)
], InputUploadComponent.prototype, "modelChange", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], InputUploadComponent.prototype, "name", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], InputUploadComponent.prototype, "label", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_file_upload__["FileUploaderOptions"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ng2_file_upload__["FileUploaderOptions"]) === "function" && _b || Object)
], InputUploadComponent.prototype, "options", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], InputUploadComponent.prototype, "imageUrl", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], InputUploadComponent.prototype, "size", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], InputUploadComponent.prototype, "fileType", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], InputUploadComponent.prototype, "single", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostBinding"])('class'),
    __metadata("design:type", String)
], InputUploadComponent.prototype, "hostClass", void 0);
InputUploadComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-input-upload',
        template: __webpack_require__("../../../../../src/app/shared/input-upload/input-upload.component.html"),
        styles: [__webpack_require__("../../../../../src/app/shared/input-upload/input-upload.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"]) === "function" && _c || Object])
], InputUploadComponent);

var _a, _b, _c;
//# sourceMappingURL=input-upload.component.js.map

/***/ }),

/***/ "../../../../../src/app/shared/input/input.component.html":
/***/ (function(module, exports) {

module.exports = "<label *ngIf=\"label\">{{label}}<span *ngIf=\"locale\"> [{{_currentLocale}}]</span></label>\r\n<input\r\n  *ngIf=\"type === 'text'\"\r\n  type=\"text\"\r\n  [name]=\"name\"\r\n  [placeholder]=\"placeholder\"\r\n  [(ngModel)]=\"ngModel\"\r\n  (change)=\"onChange();\"\r\n>\r\n<input\r\n  *ngIf=\"type === 'password'\"\r\n  type=\"password\"\r\n  [name]=\"name\"\r\n  [placeholder]=\"placeholder\"\r\n  [(ngModel)]=\"ngModel\"\r\n  (change)=\"onChange();\"\r\n>\r\n<input\r\n  *ngIf=\"type === 'number'\"\r\n  type=\"number\"\r\n  [name]=\"name\"\r\n  [(ngModel)]=\"ngModel\"\r\n  (change)=\"onChange();\"\r\n>\r\n<div\r\n  class=\"color\"\r\n  *ngIf=\"type === 'color'\"\r\n>\r\n  <input\r\n    type=\"color\"\r\n    [name]=\"name\"\r\n    [(ngModel)]=\"ngModel\"\r\n    (change)=\"onChange();\"\r\n  >\r\n  <div class=\"hex\">{{ngModel}}</div>\r\n</div>\r\n<textarea\r\n  *ngIf=\"type === 'wysiwyg'\"\r\n  [name]=\"name\"\r\n  [(ngModel)]=\"ngModel\"\r\n  (change)=\"onChange();\"\r\n>\r\n</textarea>\r\n"

/***/ }),

/***/ "../../../../../src/app/shared/input/input.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: stretch;\n      -ms-flex-align: stretch;\n          align-items: stretch;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-flex: 0;\n      -ms-flex-positive: 0;\n          flex-grow: 0;\n  margin-bottom: 1em; }\n  :host.flex-end {\n    -webkit-box-pack: end;\n        -ms-flex-pack: end;\n            justify-content: flex-end; }\n  :host > * {\n    -webkit-box-flex: 1;\n        -ms-flex: 1 1 auto;\n            flex: 1 1 auto; }\n    :host > *.no-flex {\n      -webkit-box-flex: 0;\n          -ms-flex: none;\n              flex: none; }\n  :host label {\n    text-transform: uppercase; }\n  :host input[type=text], :host input[type=password] {\n    padding: 0 1.5em;\n    height: 3em;\n    line-height: 3em; }\n  :host .color {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    height: 2em; }\n    :host .color .hex {\n      text-transform: uppercase;\n      background: #5d605d;\n      height: 100%;\n      line-height: 2em;\n      padding-right: 0.6em; }\n    :host .color input[type=color] {\n      width: 4em;\n      height: 100%;\n      background: #5d605d;\n      border: 0 none;\n      cursor: pointer; }\n  :host textarea {\n    height: 8em;\n    line-height: 1.2em;\n    padding: 0.4em; }\n  :host.size-small {\n    margin-bottom: 0.6em; }\n    :host.size-small label {\n      text-transform: uppercase;\n      font-size: 0.8em; }\n    :host.size-small input {\n      padding: 0 1em;\n      height: 2em;\n      line-height: 2em;\n      font-size: 0.8em; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shared/input/input.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__locale_service__ = __webpack_require__("../../../../../src/app/shared/locale.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_lodash__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var InputComponent = (function () {
    function InputComponent(_localeService) {
        this._localeService = _localeService;
        this.modelChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.hostClass = '';
        this._locales = this._localeService.locales;
    }
    InputComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.label && !this.placeholder) {
            this.placeholder = __WEBPACK_IMPORTED_MODULE_2_lodash__["lowerCase"](this.label);
        }
        this.locale = !!this.locale;
        if (!this.type)
            this.type = 'text';
        switch (this.size) {
            case 'small':
                this.hostClass = 'size-small';
                break;
            case 'big':
                this.hostClass = 'size-big';
                break;
            default:
                this.hostClass = 'size-default';
        }
        this._localeService.locale
            .subscribe(function (locale) { return _this.onLocaleChange(locale); });
    };
    InputComponent.prototype.ngOnChanges = function (changes) {
        if (changes.model)
            this.ngModel = this.model = changes.model.currentValue;
    };
    InputComponent.prototype.onChange = function () {
        if (this.locale) {
            this.model[this._currentLocale] = this.ngModel;
        }
        else {
            this.model = this.ngModel;
        }
        this.modelChange.emit(this.model);
    };
    InputComponent.prototype.onLocaleChange = function (locale) {
        this._currentLocale = locale;
        if (this.locale) {
            this.ngModel = this.model[this._currentLocale];
        }
        else {
            this.ngModel = this.model;
        }
    };
    return InputComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], InputComponent.prototype, "type", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], InputComponent.prototype, "model", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _a || Object)
], InputComponent.prototype, "modelChange", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], InputComponent.prototype, "name", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], InputComponent.prototype, "label", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], InputComponent.prototype, "locale", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], InputComponent.prototype, "size", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], InputComponent.prototype, "placeholder", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostBinding"])('class'),
    __metadata("design:type", String)
], InputComponent.prototype, "hostClass", void 0);
InputComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-input',
        template: __webpack_require__("../../../../../src/app/shared/input/input.component.html"),
        styles: [__webpack_require__("../../../../../src/app/shared/input/input.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__locale_service__["a" /* LocaleService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__locale_service__["a" /* LocaleService */]) === "function" && _b || Object])
], InputComponent);

var _a, _b;
//# sourceMappingURL=input.component.js.map

/***/ }),

/***/ "../../../../../src/app/shared/locale.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocaleService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LOCALES = ['pl', 'en'];
var LocaleService = (function () {
    function LocaleService() {
        this.locales = LOCALES;
        this.locale = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]('pl');
    }
    LocaleService.prototype.setLocale = function (locale) {
        if (this.locales.indexOf(locale) > -1) {
            this.locale.next(locale);
        }
    };
    return LocaleService;
}());
LocaleService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [])
], LocaleService);

//# sourceMappingURL=locale.service.js.map

/***/ }),

/***/ "../../../../../src/app/shared/popup.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__ = __webpack_require__("../../../../rxjs/Subject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopupService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PopupService = (function () {
    function PopupService() {
        this.open$ = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["Subject"]();
        this.close$ = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["Subject"]();
    }
    PopupService.prototype.open = function (message) {
        this.open$.next(message);
        return this.close$.take(1);
    };
    PopupService.prototype.close = function (response) {
        this.close$.next(response);
    };
    return PopupService;
}());
PopupService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [])
], PopupService);

//# sourceMappingURL=popup.service.js.map

/***/ }),

/***/ "../../../../../src/app/shared/popup/popup.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"popup\">\r\n  <div class=\"message\" [innerHTML]=\"_message\"></div>\r\n  <div class=\"controls\">\r\n    <app-button (click)=\"onYesClick()\" [icon]=\"'check'\" [caption]=\"'yes'\"></app-button>\r\n    <app-button (click)=\"onNoClick()\" [icon]=\"'x'\" [caption]=\"'no'\"></app-button>\r\n  </div>\r\n</div>\r\n<div class=\"overlay\"></div>\r\n"

/***/ }),

/***/ "../../../../../src/app/shared/popup/popup.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  position: fixed;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  z-index: 50;\n  visibility: hidden;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n  :host.active {\n    visibility: visible; }\n    :host.active .popup {\n      opacity: 1;\n      -webkit-transform: translate3d(0, 0, 0);\n              transform: translate3d(0, 0, 0); }\n    :host.active .overlay {\n      opacity: 1; }\n\n.popup {\n  background: #3d404b;\n  padding: 2em;\n  transition: all 400ms ease-in-out;\n  -webkit-transform: translate3d(0, -50px, 0);\n          transform: translate3d(0, -50px, 0);\n  position: relative;\n  z-index: 10;\n  opacity: 0; }\n  .popup .message {\n    margin-bottom: 1em; }\n  .popup .controls {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex; }\n    .popup .controls app-button {\n      margin-right: 1em; }\n\n.overlay {\n  position: absolute;\n  z-index: 5;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background: rgba(0, 0, 0, 0.5);\n  transition: all 400ms ease-in-out;\n  opacity: 0; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shared/popup/popup.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__popup_service__ = __webpack_require__("../../../../../src/app/shared/popup.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopupComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PopupComponent = (function () {
    function PopupComponent(_popupService) {
        var _this = this;
        this._popupService = _popupService;
        this.hostClass = '';
        this._message = '';
        this._popupService.open$
            .subscribe(function (msg) { return _this.open(msg); });
    }
    PopupComponent.prototype.ngOnInit = function () {
    };
    PopupComponent.prototype.open = function (message) {
        this._message = message;
        this.hostClass = 'active';
    };
    PopupComponent.prototype.onYesClick = function () {
        this._popupService.close(true);
        this.hostClass = '';
    };
    PopupComponent.prototype.onNoClick = function () {
        this._popupService.close(false);
        this.hostClass = '';
    };
    return PopupComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostBinding"])('class'),
    __metadata("design:type", String)
], PopupComponent.prototype, "hostClass", void 0);
PopupComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-popup',
        template: __webpack_require__("../../../../../src/app/shared/popup/popup.component.html"),
        styles: [__webpack_require__("../../../../../src/app/shared/popup/popup.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__popup_service__["a" /* PopupService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__popup_service__["a" /* PopupService */]) === "function" && _a || Object])
], PopupComponent);

var _a;
//# sourceMappingURL=popup.component.js.map

/***/ }),

/***/ "../../../../../src/app/shared/shared.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__api_service__ = __webpack_require__("../../../../../src/app/shared/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__color_badge_color_badge_component__ = __webpack_require__("../../../../../src/app/shared/color-badge/color-badge.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__title_bar_title_bar_component__ = __webpack_require__("../../../../../src/app/shared/title-bar/title-bar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__icon_icon_component__ = __webpack_require__("../../../../../src/app/shared/icon/icon.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__button_button_component__ = __webpack_require__("../../../../../src/app/shared/button/button.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__input_input_component__ = __webpack_require__("../../../../../src/app/shared/input/input.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_ng2_dnd__ = __webpack_require__("../../../../ng2-dnd/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__locale_service__ = __webpack_require__("../../../../../src/app/shared/locale.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__input_upload_input_upload_component__ = __webpack_require__("../../../../../src/app/shared/input-upload/input-upload.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__enum_pipe__ = __webpack_require__("../../../../../src/app/shared/enum.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__tag_list_tag_list_component__ = __webpack_require__("../../../../../src/app/shared/tag-list/tag-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__info_badge_info_badge_component__ = __webpack_require__("../../../../../src/app/shared/info-badge/info-badge.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__http_tools_service__ = __webpack_require__("../../../../../src/app/shared/http-tools.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__popup_service__ = __webpack_require__("../../../../../src/app/shared/popup.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__popup_popup_component__ = __webpack_require__("../../../../../src/app/shared/popup/popup.component.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SharedModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



















var SharedModule = (function () {
    function SharedModule() {
    }
    return SharedModule;
}());
SharedModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_8__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_9_ng2_file_upload__["FileUploadModule"],
            __WEBPACK_IMPORTED_MODULE_10_ng2_dnd__["a" /* DndModule */].forRoot()
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_2__api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_11__locale_service__["a" /* LocaleService */],
            __WEBPACK_IMPORTED_MODULE_16__http_tools_service__["a" /* HttpToolsService */],
            __WEBPACK_IMPORTED_MODULE_17__popup_service__["a" /* PopupService */]
        ],
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__color_badge_color_badge_component__["a" /* ColorBadgeComponent */],
            __WEBPACK_IMPORTED_MODULE_4__title_bar_title_bar_component__["a" /* TitleBarComponent */],
            __WEBPACK_IMPORTED_MODULE_5__icon_icon_component__["a" /* IconComponent */],
            __WEBPACK_IMPORTED_MODULE_6__button_button_component__["a" /* ButtonComponent */],
            __WEBPACK_IMPORTED_MODULE_7__input_input_component__["a" /* InputComponent */],
            __WEBPACK_IMPORTED_MODULE_12__input_upload_input_upload_component__["a" /* InputUploadComponent */],
            __WEBPACK_IMPORTED_MODULE_14__tag_list_tag_list_component__["a" /* TagListComponent */],
            __WEBPACK_IMPORTED_MODULE_15__info_badge_info_badge_component__["a" /* InfoBadgeComponent */],
            __WEBPACK_IMPORTED_MODULE_18__popup_popup_component__["a" /* PopupComponent */],
            __WEBPACK_IMPORTED_MODULE_13__enum_pipe__["a" /* EnumPipe */]
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_10_ng2_dnd__["a" /* DndModule */],
            __WEBPACK_IMPORTED_MODULE_3__color_badge_color_badge_component__["a" /* ColorBadgeComponent */],
            __WEBPACK_IMPORTED_MODULE_4__title_bar_title_bar_component__["a" /* TitleBarComponent */],
            __WEBPACK_IMPORTED_MODULE_5__icon_icon_component__["a" /* IconComponent */],
            __WEBPACK_IMPORTED_MODULE_6__button_button_component__["a" /* ButtonComponent */],
            __WEBPACK_IMPORTED_MODULE_7__input_input_component__["a" /* InputComponent */],
            __WEBPACK_IMPORTED_MODULE_12__input_upload_input_upload_component__["a" /* InputUploadComponent */],
            __WEBPACK_IMPORTED_MODULE_14__tag_list_tag_list_component__["a" /* TagListComponent */],
            __WEBPACK_IMPORTED_MODULE_15__info_badge_info_badge_component__["a" /* InfoBadgeComponent */],
            __WEBPACK_IMPORTED_MODULE_18__popup_popup_component__["a" /* PopupComponent */],
            __WEBPACK_IMPORTED_MODULE_13__enum_pipe__["a" /* EnumPipe */],
        ]
    })
], SharedModule);

//# sourceMappingURL=shared.module.js.map

/***/ }),

/***/ "../../../../../src/app/shared/tag-list/tag-list.component.html":
/***/ (function(module, exports) {

module.exports = "<app-title-bar [size]=\"'small'\" [title]=\"'Tag List'\">\n  <form #f=\"ngForm\" (ngSubmit)=\"onSubmit(f)\" novalidate>\n    <input type=\"text\" name=\"tag\" placeholder=\"tag\" [(ngModel)]=\"_newTag\" autocomplete=\"new-password\" class=\"small\" required>\n    <app-button [type]=\"'submit'\" [icon]=\"'plus'\" [caption]=\"'Create'\"></app-button>\n  </form>\n</app-title-bar>\n\n<div class=\"tags\" *ngIf=\"list\">\n  <div\n    class=\"tag\"\n    *ngFor=\"let tag of list\"\n  >\n    <div class=\"text\">{{tag}}</div>\n    <app-button (click)=\"onRemoveClick(tag);\" [size]=\"'tiny'\" [icon]=\"'x'\" title=\"Remove\"></app-button>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/shared/tag-list/tag-list.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  display: block; }\n\n.tags {\n  background: #31343d;\n  padding: 0.4em 0.4em 0 0.4em;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap; }\n  .tags .tag {\n    background: #3d404b;\n    padding: 0.2em 0.4em;\n    margin-right: 0.4em;\n    margin-bottom: 0.4em;\n    -webkit-user-select: none;\n       -moz-user-select: none;\n        -ms-user-select: none;\n            user-select: none;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex; }\n    .tags .tag .text {\n      margin-right: 0.4em; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shared/tag-list/tag-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__popup_service__ = __webpack_require__("../../../../../src/app/shared/popup.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TagListComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TagListComponent = (function () {
    function TagListComponent(_popupService) {
        this._popupService = _popupService;
        this.onListChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    TagListComponent.prototype.ngOnInit = function () {
        this._newTag = '';
    };
    TagListComponent.prototype.onSubmit = function (f) {
        if (f.valid) {
            if (__WEBPACK_IMPORTED_MODULE_1_lodash__["indexOf"](this.list, this._newTag) === -1) {
                this.list.push(this._newTag);
                this.onListChange.emit(this.list);
            }
            this._newTag = '';
        }
    };
    TagListComponent.prototype.onRemoveClick = function (tag) {
        var _this = this;
        var msg = 'Are you sure you want to remove tag &quot;' + tag + '&quot; ?';
        this._popupService.open(msg)
            .subscribe(function (confirm) {
            if (confirm)
                _this.removeTag(tag);
        });
    };
    TagListComponent.prototype.removeTag = function (tag) {
        __WEBPACK_IMPORTED_MODULE_1_lodash__["pull"](this.list, tag);
        this.onListChange.emit(this.list);
    };
    return TagListComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Array)
], TagListComponent.prototype, "list", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _a || Object)
], TagListComponent.prototype, "onListChange", void 0);
TagListComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-tag-list',
        template: __webpack_require__("../../../../../src/app/shared/tag-list/tag-list.component.html"),
        styles: [__webpack_require__("../../../../../src/app/shared/tag-list/tag-list.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__popup_service__["a" /* PopupService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__popup_service__["a" /* PopupService */]) === "function" && _b || Object])
], TagListComponent);

var _a, _b;
//# sourceMappingURL=tag-list.component.js.map

/***/ }),

/***/ "../../../../../src/app/shared/title-bar/title-bar.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"group\">\n  <app-button\n    *ngIf=\"backEnabled\"\n    [icon]=\"'arrow-left'\"\n    [size]=\"'big'\"\n    [cssClass]=\"'no-background'\"\n    (click)=\"onBackClick()\"\n  ></app-button>\n  <div class=\"title\">{{title}}</div>\n</div>\n<div class=\"group\">\n  <div class=\"controls\">\n    <ng-content></ng-content>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/shared/title-bar/title-bar.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  margin-bottom: 1em;\n  background: #31343d;\n  padding: 0.2em; }\n  :host .group {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    :host .group .title {\n      padding: 0.6em;\n      font-size: 1.4em; }\n  :host.size-small {\n    padding: 0.6em;\n    margin-bottom: 0.6em; }\n    :host.size-small .group .title {\n      padding: 0.2em 0.6em;\n      font-size: 1.2em; }\n  :host.size-default .group .controls {\n    padding: 0 0.6em; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shared/title-bar/title-bar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TitleBarComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TitleBarComponent = (function () {
    function TitleBarComponent() {
        this.back = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.hostClass = '';
    }
    TitleBarComponent.prototype.ngOnInit = function () {
        switch (this.size) {
            case 'small':
                this.hostClass = 'size-small';
                break;
            case 'big':
                this.hostClass = 'size-big';
                break;
            default:
                this.hostClass = 'size-default';
        }
    };
    TitleBarComponent.prototype.onBackClick = function () {
        this.back.emit();
    };
    return TitleBarComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], TitleBarComponent.prototype, "title", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], TitleBarComponent.prototype, "size", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], TitleBarComponent.prototype, "backEnabled", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _a || Object)
], TitleBarComponent.prototype, "back", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostBinding"])('class'),
    __metadata("design:type", String)
], TitleBarComponent.prototype, "hostClass", void 0);
TitleBarComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-title-bar',
        template: __webpack_require__("../../../../../src/app/shared/title-bar/title-bar.component.html"),
        styles: [__webpack_require__("../../../../../src/app/shared/title-bar/title-bar.component.scss")]
    }),
    __metadata("design:paramtypes", [])
], TitleBarComponent);

var _a;
//# sourceMappingURL=title-bar.component.js.map

/***/ }),

/***/ "../../../../../src/app/user/auth-guard.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user_service__ = __webpack_require__("../../../../../src/app/user/user.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuard; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuard = (function () {
    function AuthGuard(_router, _userService) {
        this._router = _router;
        this._userService = _userService;
    }
    AuthGuard.prototype.canActivate = function () {
        var _this = this;
        return this._userService.user$
            .filter(function (user) { return user !== undefined; })
            .map(function (user) { return !!user; })
            .do(function (user) {
            if (!user)
                _this._router.navigate(['/user/login']);
        });
    };
    return AuthGuard;
}());
AuthGuard = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__user_service__["a" /* UserService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__user_service__["a" /* UserService */]) === "function" && _b || Object])
], AuthGuard);

var _a, _b;
//# sourceMappingURL=auth-guard.service.js.map

/***/ }),

/***/ "../../../../../src/app/user/avatar/avatar.component.html":
/***/ (function(module, exports) {

module.exports = "<div\n  class=\"avatar\"\n  *ngIf=\"user\"\n>\n  <div class=\"symbol\"><span>{{user.symbol}}</span></div>\n  <div\n    class=\"shape\"\n    [ngClass]=\"{\n      'circle': user.type == UserType.Student,\n      'square': user.type == UserType.Teacher,\n      'hexagon': user.type == UserType.Admin\n    }\"\n    [ngStyle]=\"{'background-color': user.color}\"\n  >\n    <svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"36\" height=\"36\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" viewBox=\"0 0 300 300\">\n      <polygon\n        class=\"hex\"\n        points=\"300,150 225,280 75,280 0,150 75,20 225,20\"\n        [ngStyle]=\"{'fill': user.color}\"\n      ></polygon>\n    </svg>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/user/avatar/avatar.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  -webkit-user-select: none;\n     -moz-user-select: none;\n      -ms-user-select: none;\n          user-select: none;\n  cursor: default;\n  display: inline-block;\n  width: 2em;\n  height: 2em; }\n  :host .avatar {\n    width: 100%;\n    height: 100%;\n    position: relative; }\n  :host .symbol {\n    position: absolute;\n    z-index: 10;\n    top: 0;\n    left: 0;\n    bottom: 0;\n    right: 0;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    :host .symbol span {\n      font-size: 0.9em; }\n  :host .shape {\n    width: 100%;\n    height: 100%;\n    position: absolute;\n    z-index: 5;\n    top: 0;\n    left: 0; }\n    :host .shape svg {\n      display: none; }\n    :host .shape.circle {\n      border-radius: 50%; }\n    :host .shape.square {\n      -webkit-transform: rotate(45deg);\n              transform: rotate(45deg);\n      top: 2px;\n      left: 2px;\n      right: 2px;\n      bottom: 2px;\n      width: 90%;\n      height: 90%; }\n    :host .shape.hexagon {\n      background: none !important; }\n      :host .shape.hexagon svg {\n        margin-left: -2px;\n        margin-top: -2px;\n        display: block; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/user/avatar/avatar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user__ = __webpack_require__("../../../../../src/app/user/user.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AvatarComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AvatarComponent = (function () {
    function AvatarComponent() {
        this.UserType = __WEBPACK_IMPORTED_MODULE_1__user__["a" /* User */].Type;
    }
    AvatarComponent.prototype.ngOnInit = function () {
    };
    return AvatarComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__user__["a" /* User */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__user__["a" /* User */]) === "function" && _a || Object)
], AvatarComponent.prototype, "user", void 0);
AvatarComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-avatar',
        template: __webpack_require__("../../../../../src/app/user/avatar/avatar.component.html"),
        styles: [__webpack_require__("../../../../../src/app/user/avatar/avatar.component.scss")]
    }),
    __metadata("design:paramtypes", [])
], AvatarComponent);

var _a;
//# sourceMappingURL=avatar.component.js.map

/***/ }),

/***/ "../../../../../src/app/user/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"box\">\n  <form #f=\"ngForm\" (ngSubmit)=\"onSubmit(f)\" novalidate>\n    <div class=\"form-group\">\n      <label for=\"username\">Username</label>\n      <input name=\"username\" id=\"username\" type=\"text\" class=\"form-control\" (keypress)=\"onKeyPress()\" [(ngModel)]=\"username\">\n    </div>\n    <div class=\"form-group\">\n      <label for=\"password\">Password</label>\n      <input name=\"password\" id=\"password\" type=\"password\" class=\"form-control\" (keypress)=\"onKeyPress()\" [(ngModel)]=\"password\">\n    </div>\n    <div class=\"message\">{{message}}</div>\n    <button>Log in</button>\n  </form>\n\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/user/login/login.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: stretch;\n      -ms-flex-align: stretch;\n          align-items: stretch;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  padding: 5.2em 2em 2em;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  height: 100%; }\n  :host.flex-end {\n    -webkit-box-pack: end;\n        -ms-flex-pack: end;\n            justify-content: flex-end; }\n  :host > * {\n    -webkit-box-flex: 1;\n        -ms-flex: 1 1 auto;\n            flex: 1 1 auto; }\n    :host > *.no-flex {\n      -webkit-box-flex: 0;\n          -ms-flex: none;\n              flex: none; }\n\n.box {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: stretch;\n      -ms-flex-align: stretch;\n          align-items: stretch;\n  -webkit-box-flex: 0;\n      -ms-flex-positive: 0;\n          flex-grow: 0;\n  width: 30%; }\n  .box.flex-end {\n    -webkit-box-pack: end;\n        -ms-flex-pack: end;\n            justify-content: flex-end; }\n  .box > * {\n    -webkit-box-flex: 1;\n        -ms-flex: 1 1 auto;\n            flex: 1 1 auto; }\n    .box > *.no-flex {\n      -webkit-box-flex: 0;\n          -ms-flex: none;\n              flex: none; }\n  .box form {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: stretch;\n        -ms-flex-align: stretch;\n            align-items: stretch;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    background: #444644;\n    padding: 2em; }\n    .box form.flex-end {\n      -webkit-box-pack: end;\n          -ms-flex-pack: end;\n              justify-content: flex-end; }\n    .box form > * {\n      -webkit-box-flex: 1;\n          -ms-flex: 1 1 auto;\n              flex: 1 1 auto; }\n      .box form > *.no-flex {\n        -webkit-box-flex: 0;\n            -ms-flex: none;\n                flex: none; }\n    .box form .form-group {\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-align: stretch;\n          -ms-flex-align: stretch;\n              align-items: stretch;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: column;\n              flex-direction: column;\n      margin-bottom: 1em; }\n      .box form .form-group.flex-end {\n        -webkit-box-pack: end;\n            -ms-flex-pack: end;\n                justify-content: flex-end; }\n      .box form .form-group > * {\n        -webkit-box-flex: 1;\n            -ms-flex: 1 1 auto;\n                flex: 1 1 auto; }\n        .box form .form-group > *.no-flex {\n          -webkit-box-flex: 0;\n              -ms-flex: none;\n                  flex: none; }\n      .box form .form-group label {\n        text-transform: uppercase; }\n      .box form .form-group .form-control {\n        color: #FFFFFF;\n        background: #5d605d;\n        border: 0 none;\n        padding: 0 0.5em;\n        height: 3em;\n        line-height: 3em;\n        padding: 0 1.5em;\n        height: 3em;\n        line-height: 3em; }\n        .box form .form-group .form-control::-webkit-input-placeholder {\n          color: #cccccc; }\n        .box form .form-group .form-control:-moz-placeholder {\n          color: #cccccc; }\n        .box form .form-group .form-control::-moz-placeholder {\n          color: #cccccc; }\n        .box form .form-group .form-control:-ms-input-placeholder {\n          color: #cccccc; }\n        .box form .form-group .form-control.small {\n          height: 2em;\n          line-height: 2em;\n          font-size: 0.9em; }\n    .box form .message {\n      height: 2em; }\n    .box form button {\n      background-color: #FFB855;\n      color: #FFFFFF;\n      font-size: 0.95em;\n      padding: 0.4em 1.2em;\n      border-radius: 0.2em;\n      text-transform: uppercase;\n      font-weight: bold;\n      border: 0 none;\n      cursor: pointer;\n      outline: 0 none;\n      -webkit-user-select: none;\n         -moz-user-select: none;\n          -ms-user-select: none;\n              user-select: none;\n      -webkit-box-flex: 0;\n          -ms-flex-positive: 0;\n              flex-grow: 0;\n      font-size: 1.2em;\n      padding: 0.8em;\n      margin-top: 2em; }\n      .box form button:hover {\n        background-color: #ffad3c; }\n      .box form button:active {\n        background-color: #ffa322;\n        -webkit-transform: translate(0, 1px);\n                transform: translate(0, 1px); }\n      .box form button.no-background {\n        background: none; }\n        .box form button.no-background:hover {\n          background: none; }\n        .box form button.no-background:active {\n          background: none;\n          -webkit-transform: translate(0, 1px);\n                  transform: translate(0, 1px); }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/user/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user_service__ = __webpack_require__("../../../../../src/app/user/user.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoginComponent = (function () {
    function LoginComponent(_userService) {
        this._userService = _userService;
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.onKeyPress = function () {
        this.message = null;
    };
    LoginComponent.prototype.onSubmit = function (f) {
        var _this = this;
        if (f.valid) {
            this._userService
                .login(this.username, this.password)
                .subscribe({ error: function (err) { return _this.onError(err); } });
        }
    };
    LoginComponent.prototype.onError = function (err) {
        this.message = err;
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-login',
        template: __webpack_require__("../../../../../src/app/user/login/login.component.html"),
        styles: [__webpack_require__("../../../../../src/app/user/login/login.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__user_service__["a" /* UserService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__user_service__["a" /* UserService */]) === "function" && _a || Object])
], LoginComponent);

var _a;
//# sourceMappingURL=login.component.js.map

/***/ }),

/***/ "../../../../../src/app/user/routes.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__login_login_component__ = __webpack_require__("../../../../../src/app/user/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user_list_user_list_component__ = __webpack_require__("../../../../../src/app/user/user-list/user-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_guard_service__ = __webpack_require__("../../../../../src/app/user/auth-guard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__user_edit_user_edit_component__ = __webpack_require__("../../../../../src/app/user/user-edit/user-edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user_resolver_service__ = __webpack_require__("../../../../../src/app/user/user-resolver.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routes; });





var routes = [
    {
        path: 'user',
        children: [
            {
                path: 'login',
                component: __WEBPACK_IMPORTED_MODULE_0__login_login_component__["a" /* LoginComponent */]
            },
            {
                path: 'list',
                component: __WEBPACK_IMPORTED_MODULE_1__user_list_user_list_component__["a" /* UserListComponent */],
                canActivate: [__WEBPACK_IMPORTED_MODULE_2__auth_guard_service__["a" /* AuthGuard */]],
            },
            {
                path: 'edit/:id',
                component: __WEBPACK_IMPORTED_MODULE_3__user_edit_user_edit_component__["a" /* UserEditComponent */],
                canActivate: [__WEBPACK_IMPORTED_MODULE_2__auth_guard_service__["a" /* AuthGuard */]],
                resolve: {
                    user: __WEBPACK_IMPORTED_MODULE_4__user_resolver_service__["a" /* UserResolver */]
                }
            }
        ]
    }
];
//# sourceMappingURL=routes.js.map

/***/ }),

/***/ "../../../../../src/app/user/user-edit/user-edit.component.html":
/***/ (function(module, exports) {

module.exports = "<app-title-bar [title]=\"'User edit'\" [backEnabled]=\"true\" (back)=\"onBackClick()\"></app-title-bar>\r\n\r\n<form #f=\"ngForm\" (ngSubmit)=\"onSubmit(f)\" novalidate>\r\n  <app-input [label]=\"'Username'\" [name]=\"'username'\" [(model)]=\"_user.username\" ngDefaultControl></app-input>\r\n  <app-input [label]=\"'Display name'\" [name]=\"'displayName'\" [(model)]=\"_user.displayName\" ngDefaultControl></app-input>\r\n  <div class=\"info\">If you don't want to change password, leave fields below blank.</div>\r\n  <app-input [type]=\"'password'\" [label]=\"'New password'\" [name]=\"'newPassword'\" [(model)]=\"_newPassword\" ngDefaultControl></app-input>\r\n  <app-input [type]=\"'password'\" [label]=\"'Confirm new password'\" [name]=\"'confirmNewPassword'\" [(model)]=\"_confirmNewPassword\" ngDefaultControl></app-input>\r\n  <div class=\"form-group\">\r\n    <label>Type</label>\r\n    <select [(ngModel)]=\"_user.type\" name=\"type\">\r\n      <option *ngFor=\"let type of UserType | enum; let i = index;\" [value]=\"i\">{{type}}</option>\r\n    </select>\r\n  </div>\r\n  <app-input [label]=\"'Symbol'\" [name]=\"'symbol'\" [(model)]=\"_user.symbol\" ngDefaultControl></app-input>\r\n  <app-input [type]=\"'color'\" [label]=\"'Color'\" [name]=\"'color'\" [(model)]=\"_user.color\" ngDefaultControl></app-input>\r\n  <app-input [label]=\"'E-mail address'\" [name]=\"'email'\" [(model)]=\"_user.email\" ngDefaultControl></app-input>\r\n  <app-button [type]=\"'submit'\" [icon]=\"'check'\" [caption]=\"'Save'\"></app-button>\r\n  <app-button (click)=\"onCancelClick();\" [icon]=\"'x'\" [caption]=\"'Cancel'\"></app-button>\r\n</form>\r\n"

/***/ }),

/***/ "../../../../../src/app/user/user-edit/user-edit.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: stretch;\n      -ms-flex-align: stretch;\n          align-items: stretch;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  padding: 5.2em 2em 2em; }\n  :host.flex-end {\n    -webkit-box-pack: end;\n        -ms-flex-pack: end;\n            justify-content: flex-end; }\n  :host > * {\n    -webkit-box-flex: 1;\n        -ms-flex: 1 1 auto;\n            flex: 1 1 auto; }\n    :host > *.no-flex {\n      -webkit-box-flex: 0;\n          -ms-flex: none;\n              flex: none; }\n\n.info {\n  font-size: 0.9em;\n  margin-bottom: 0.4em; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/user/user-edit/user-edit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user__ = __webpack_require__("../../../../../src/app/user/user.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__user_service__ = __webpack_require__("../../../../../src/app/user/user.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserEditComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UserEditComponent = (function () {
    function UserEditComponent(_userService, _route, _router) {
        this._userService = _userService;
        this._route = _route;
        this._router = _router;
        this.UserType = __WEBPACK_IMPORTED_MODULE_1__user__["a" /* User */].Type;
        this._newPassword = '';
        this._confirmNewPassword = '';
    }
    UserEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route.data
            .subscribe(function (data) {
            _this._user = data.user;
        });
    };
    UserEditComponent.prototype.onSubmit = function (f) {
        var _this = this;
        if (f.valid && this.validPassword()) {
            if (this.filledPassword())
                this._user.password = this._newPassword;
            this._userService.update(this._user)
                .subscribe(function () {
                _this.backToList();
            });
        }
    };
    UserEditComponent.prototype.filledPassword = function () {
        return this._newPassword.length > 0;
    };
    UserEditComponent.prototype.validPassword = function () {
        var valid = true;
        if (this.filledPassword())
            valid = this._newPassword == this._confirmNewPassword;
        return valid;
    };
    UserEditComponent.prototype.onBackClick = function () {
        this.backToList();
    };
    UserEditComponent.prototype.onCancelClick = function () {
        this.backToList();
    };
    UserEditComponent.prototype.backToList = function () {
        this._router.navigate(['/user/list']);
    };
    return UserEditComponent;
}());
UserEditComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-user-edit',
        template: __webpack_require__("../../../../../src/app/user/user-edit/user-edit.component.html"),
        styles: [__webpack_require__("../../../../../src/app/user/user-edit/user-edit.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__user_service__["a" /* UserService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__user_service__["a" /* UserService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* ActivatedRoute */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === "function" && _c || Object])
], UserEditComponent);

var _a, _b, _c;
//# sourceMappingURL=user-edit.component.js.map

/***/ }),

/***/ "../../../../../src/app/user/user-list/user-list.component.html":
/***/ (function(module, exports) {

module.exports = "<app-title-bar [title]=\"'Users'\">\n  <form #f=\"ngForm\" (ngSubmit)=\"onSubmit(f)\" novalidate>\n    <input type=\"text\" name=\"username\" placeholder=\"username\" [(ngModel)]=\"_newUser.username\"\n           autocomplete=\"new-password\" class=\"small\">\n    <input type=\"password\" name=\"password\" placeholder=\"password\" [(ngModel)]=\"_newUser.password\"\n           autocomplete=\"new-password\" class=\"small\">\n    <app-button [type]=\"'submit'\" [icon]=\"'plus'\" [caption]=\"'Create'\"></app-button>\n  </form>\n</app-title-bar>\n\n<table class=\"list\">\n  <thead>\n  <tr>\n    <th class=\"symbol\">&nbsp;</th>\n    <th>Display name</th>\n    <th>Username</th>\n    <th>Type</th>\n    <th>Color</th>\n    <th class=\"date\">Created</th>\n    <th class=\"date\">Modified</th>\n    <th class=\"actions\">Actions</th>\n  </tr>\n  </thead>\n  <tbody *ngIf=\"_users\">\n  <tr *ngFor=\"let user of _users\">\n    <td class=\"symbol\">\n      <app-avatar [user]=\"user\"></app-avatar>\n    </td>\n    <td>{{user.displayName}}</td>\n    <td>{{user.username}}</td>\n    <td>{{UserType[user.type]}}</td>\n    <td>\n      <app-color-badge [color]=\"user.color\"></app-color-badge>\n    </td>\n    <td class=\"date\">\n      <app-info-badge [time]=\"user.created\" [user]=\"user.createdBy\"></app-info-badge>\n    </td>\n    <td class=\"date\">\n      <app-info-badge [time]=\"user.modified\" [user]=\"user.modifiedBy\"></app-info-badge>\n    </td>\n    <td class=\"actions\">\n      <app-button (click)=\"onEditClick(user);\" [icon]=\"'pencil'\" [caption]=\"'Edit'\"></app-button>\n      <app-button (click)=\"onRemoveClick(user);\" [icon]=\"'x'\" [caption]=\"'Remove'\"></app-button>\n    </td>\n  </tr>\n  </tbody>\n</table>\n"

/***/ }),

/***/ "../../../../../src/app/user/user-list/user-list.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: stretch;\n      -ms-flex-align: stretch;\n          align-items: stretch;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  padding: 5.2em 2em 2em; }\n  :host.flex-end {\n    -webkit-box-pack: end;\n        -ms-flex-pack: end;\n            justify-content: flex-end; }\n  :host > * {\n    -webkit-box-flex: 1;\n        -ms-flex: 1 1 auto;\n            flex: 1 1 auto; }\n    :host > *.no-flex {\n      -webkit-box-flex: 0;\n          -ms-flex: none;\n              flex: none; }\n\n.list {\n  width: 100%;\n  table-layout: fixed;\n  background: #31343d; }\n  .list th, .list td {\n    padding: 0.4em;\n    text-align: left; }\n  .list th {\n    background: #363943;\n    height: 41px; }\n  .list th.date, .list td.date {\n    width: 158px;\n    white-space: nowrap; }\n  .list th.actions, .list td.actions {\n    width: 220px;\n    white-space: nowrap; }\n  .list th.symbol, .list td.symbol {\n    width: 50px;\n    text-align: center; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/user/user-list/user-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user_service__ = __webpack_require__("../../../../../src/app/user/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user__ = __webpack_require__("../../../../../src/app/user/user.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_popup_service__ = __webpack_require__("../../../../../src/app/shared/popup.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserListComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UserListComponent = (function () {
    function UserListComponent(_userService, _router, _popupService) {
        this._userService = _userService;
        this._router = _router;
        this._popupService = _popupService;
        this.UserType = __WEBPACK_IMPORTED_MODULE_2__user__["a" /* User */].Type;
        this.refresh();
    }
    UserListComponent.prototype.ngOnInit = function () {
        this._newUser = new __WEBPACK_IMPORTED_MODULE_2__user__["a" /* User */]();
    };
    UserListComponent.prototype.refresh = function () {
        var _this = this;
        this._userService.list()
            .subscribe(function (users) { return _this._users = users; });
    };
    UserListComponent.prototype.onSubmit = function (f) {
        var _this = this;
        if (f.valid) {
            this._userService.create(this._newUser)
                .subscribe(function () {
                _this._newUser = new __WEBPACK_IMPORTED_MODULE_2__user__["a" /* User */]();
                _this.refresh();
            });
        }
    };
    UserListComponent.prototype.onEditClick = function (user) {
        this._router.navigate(['/user/edit', user._id]);
    };
    UserListComponent.prototype.onRemoveClick = function (user) {
        var _this = this;
        var msg = 'Are you sure you want to remove user &quot;' + user.displayName + '&quot; ?';
        this._popupService.open(msg)
            .subscribe(function (confirm) {
            if (confirm)
                _this.removeUser(user);
        });
    };
    UserListComponent.prototype.removeUser = function (user) {
        var _this = this;
        this._userService.delete(user)
            .subscribe(function () {
            _this.refresh();
        });
    };
    return UserListComponent;
}());
UserListComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-user-list',
        template: __webpack_require__("../../../../../src/app/user/user-list/user-list.component.html"),
        styles: [__webpack_require__("../../../../../src/app/user/user-list/user-list.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__user_service__["a" /* UserService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__user_service__["a" /* UserService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__shared_popup_service__["a" /* PopupService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__shared_popup_service__["a" /* PopupService */]) === "function" && _c || Object])
], UserListComponent);

var _a, _b, _c;
//# sourceMappingURL=user-list.component.js.map

/***/ }),

/***/ "../../../../../src/app/user/user-resolver.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user_service__ = __webpack_require__("../../../../../src/app/user/user.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserResolver; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserResolver = (function () {
    function UserResolver(_userService, _router) {
        this._userService = _userService;
        this._router = _router;
    }
    UserResolver.prototype.resolve = function (route, state) {
        var _this = this;
        var id = route.params['id'];
        return this._userService.get(id)
            .do(function (user) {
            if (!user) {
                _this._router.navigate(['/user/list']);
            }
        });
    };
    return UserResolver;
}());
UserResolver = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__user_service__["a" /* UserService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__user_service__["a" /* UserService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _b || Object])
], UserResolver);

var _a, _b;
//# sourceMappingURL=user-resolver.service.js.map

/***/ }),

/***/ "../../../../../src/app/user/user.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login_component__ = __webpack_require__("../../../../../src/app/user/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__user_service__ = __webpack_require__("../../../../../src/app/user/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__routes__ = __webpack_require__("../../../../../src/app/user/routes.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__user_list_user_list_component__ = __webpack_require__("../../../../../src/app/user/user-list/user-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__avatar_avatar_component__ = __webpack_require__("../../../../../src/app/user/avatar/avatar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__core_core_module__ = __webpack_require__("../../../../../src/app/core/core.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__auth_guard_service__ = __webpack_require__("../../../../../src/app/user/auth-guard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__user_edit_user_edit_component__ = __webpack_require__("../../../../../src/app/user/user-edit/user-edit.component.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var UserModule = (function () {
    function UserModule() {
    }
    return UserModule;
}());
UserModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_6__routes__["a" /* routes */]),
            __WEBPACK_IMPORTED_MODULE_9__shared_shared_module__["a" /* SharedModule */],
            __WEBPACK_IMPORTED_MODULE_10__core_core_module__["a" /* CoreModule */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_5__user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_11__auth_guard_service__["a" /* AuthGuard */]
        ],
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__login_login_component__["a" /* LoginComponent */],
            __WEBPACK_IMPORTED_MODULE_8__avatar_avatar_component__["a" /* AvatarComponent */],
            __WEBPACK_IMPORTED_MODULE_7__user_list_user_list_component__["a" /* UserListComponent */],
            __WEBPACK_IMPORTED_MODULE_12__user_edit_user_edit_component__["a" /* UserEditComponent */]
        ]
    })
], UserModule);

//# sourceMappingURL=user.module.js.map

/***/ }),

/***/ "../../../../../src/app/user/user.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_api_service__ = __webpack_require__("../../../../../src/app/shared/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user__ = __webpack_require__("../../../../../src/app/user/user.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_Observable__ = __webpack_require__("../../../../rxjs/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var UserService = (function () {
    function UserService(_router, _apiService) {
        var _this = this;
        this._router = _router;
        this._apiService = _apiService;
        this.user$ = new __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__["BehaviorSubject"](undefined);
        this._apiService.makeRequest('user/me', __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* RequestMethod */].Get)
            .map(function (user) {
            if (user)
                return user;
            return null;
        })
            .subscribe(function (user) { return _this.user$.next(user); });
    }
    UserService.prototype.get = function (id) {
        return this._apiService.makeRequest('users/' + id, __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* RequestMethod */].Get)
            .map(function (object) { return new __WEBPACK_IMPORTED_MODULE_4__user__["a" /* User */]().deserialize(object); });
    };
    UserService.prototype.create = function (user) {
        return this._apiService.makeRequest('users', __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* RequestMethod */].Post, null, user.serialize())
            .map(function (object) { return new __WEBPACK_IMPORTED_MODULE_4__user__["a" /* User */]().deserialize(object); });
    };
    UserService.prototype.update = function (user) {
        return this._apiService.makeRequest('users/' + user._id, __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* RequestMethod */].Put, null, user.serialize())
            .map(function (object) { return new __WEBPACK_IMPORTED_MODULE_4__user__["a" /* User */]().deserialize(object); });
    };
    UserService.prototype.delete = function (user) {
        return this._apiService.makeRequest('users/' + user._id, __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* RequestMethod */].Delete, null, user.serialize())
            .map(function (object) { return new __WEBPACK_IMPORTED_MODULE_4__user__["a" /* User */]().deserialize(object); });
    };
    UserService.prototype.list = function () {
        return this._apiService.makeRequest('users', __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* RequestMethod */].Get)
            .map(function (objects) { return objects.map(function (object) { return new __WEBPACK_IMPORTED_MODULE_4__user__["a" /* User */]().deserialize(object); }); });
    };
    UserService.prototype.login = function (username, password) {
        var _this = this;
        return this._apiService.makeRequest('user/login', __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* RequestMethod */].Post, null, {
            username: username,
            password: password
        }).do(function (object) { return _this.onLogin(new __WEBPACK_IMPORTED_MODULE_4__user__["a" /* User */]().deserialize(object)); })
            .catch(function () {
            return __WEBPACK_IMPORTED_MODULE_5_rxjs_Observable__["Observable"].throw('Login or password is incorrect');
        });
    };
    UserService.prototype.logout = function () {
        var _this = this;
        this._apiService.makeRequest('user/logout', __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* RequestMethod */].Get)
            .subscribe(function () { return _this.onLogout(); });
    };
    UserService.prototype.onLogin = function (user) {
        this.user$.next(user);
        this._router.navigate(['/dashboard']);
    };
    UserService.prototype.onLogout = function () {
        this.user$.next(null);
        this._router.navigate(['/user/login']);
    };
    return UserService;
}());
UserService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__shared_api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__shared_api_service__["a" /* ApiService */]) === "function" && _b || Object])
], UserService);

var _a, _b;
//# sourceMappingURL=user.service.js.map

/***/ }),

/***/ "../../../../../src/app/user/user.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return User; });
var User = (function () {
    function User() {
    }
    User.prototype.serialize = function () {
        var serializedObject = this;
        switch (parseInt(serializedObject.type)) {
            case User.Type.Admin:
                serializedObject.type = 'admin';
                break;
            case User.Type.Teacher:
                serializedObject.type = 'teacher';
                break;
            case User.Type.Student:
                serializedObject.type = 'student';
                break;
        }
        return serializedObject;
    };
    User.prototype.deserialize = function (input) {
        this._id = input._id;
        this.username = input.username;
        this.password = input.password;
        this.displayName = input.displayName;
        switch (input.type) {
            case 'admin':
                this.type = User.Type.Admin;
                break;
            case 'teacher':
                this.type = User.Type.Teacher;
                break;
            case 'student':
                this.type = User.Type.Student;
                break;
        }
        this.symbol = input.symbol;
        this.color = input.color;
        this.email = input.email;
        this.created = input.created;
        this.modified = input.modified;
        this.createdBy = input.createdBy;
        this.modifiedBy = input.modifiedBy;
        return this;
    };
    return User;
}());

(function (User) {
    var Type;
    (function (Type) {
        Type[Type["Admin"] = 0] = "Admin";
        Type[Type["Teacher"] = 1] = "Teacher";
        Type[Type["Student"] = 2] = "Student";
    })(Type = User.Type || (User.Type = {}));
})(User || (User = {}));
//# sourceMappingURL=user.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[1]);
//# sourceMappingURL=main.bundle.js.map