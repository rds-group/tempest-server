import * as express from "express";
import RdsApplication from "./../../lib/application";
import RdsStandardModule from "../../lib/standard-module";

export default class FrontendModule extends RdsStandardModule {
  constructor(app: RdsApplication) {
    super(app);

    this.name = 'frontend';
    this.moduleDir = __dirname;
  }

  setupStatic() {
    this.app.express.use('/', express.static(this.publicDir));
  }
}