import * as passport from "passport";
import {BasicStrategy} from "passport-http";
import {Strategy as LocalStrategy} from "passport-local";
import RdsApplication from "./../../lib/application";
import UserModel from "./server/models/user";
import UserRouter from "./server/router";
import RdsStandardModule from "../../lib/standard-module";

export default class UserModule extends RdsStandardModule {
  constructor(app: RdsApplication) {
    super(app);

    this.name = 'user';
    this.moduleDir = __dirname;
    this.modelClasses.push(UserModel);
    this.routerClasses.push(UserRouter);
  }

  setupAuth() {
    let userModel: UserModel = this.getModel('User');
    passport.use(new BasicStrategy(userModel.authBasic()));
    passport.use(new LocalStrategy(userModel.authLocal()));
    passport.serializeUser(userModel.serializeUser());
    passport.deserializeUser(userModel.deserializeUser());
    this.app.express.use(passport.initialize());
    this.app.express.use(passport.session());
  }
}