import {ensureLoggedIn} from "connect-ensure-login";
import RdsRouter from "../../../lib/router";
import UsersController from "./controllers/users";

export default class UserRouter extends RdsRouter {
  setup() {
    super.setup();

    let usersController: UsersController = new UsersController(this.app, this.module, 'User');

    this.router.param('userId', (req, res, next: Function, id: string) => usersController.document(req, res, next, id));

    this.router.route('/api/users')
      .get((req, res) => usersController.all(req, res))
      .all(ensureLoggedIn())
      .post((req, res) => usersController.create(req, res));

    this.router
      .post('/api/user/login', (req, res) => usersController.login(req, res))
      .get('/api/user/logout', (req, res) => usersController.logout(req, res))
      .get('/api/user/me', (req, res) => usersController.me(req, res))
      .get('/api/user/requires-login', ensureLoggedIn(), (req, res) => usersController.me(req, res));

    this.router.route('/api/users/:userId')
      .all(ensureLoggedIn())
      .get((req, res) => usersController.show(req, res))
      .put((req, res) => usersController.update(req, res))
      .delete((req, res) => usersController.delete(req, res));
  }
}