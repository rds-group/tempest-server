import * as Q from 'q';
import * as passportLocalMongoose from "passport-local-mongoose";
import RdsModel from './../../../../lib/model';
import RdsApplication from "../../../../lib/application";

export default class UserModel extends RdsModel {
  userTypes: string[] = ['admin', 'teacher', 'student'];

  constructor(app: RdsApplication) {
    super(app);
    this.name = 'User';
    this.schemaDefinition = {
      username: {
        type: String,
        required: true,
        trim: true,
      },
      displayName: {
        type: String,
        required: true,
        trim: true
      },
      email: {
        type: String,
        match: [/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, 'E-mail is incorrect']
      },
      symbol: {
        type: String,
        required: true,
        trim: true
      },
      type: {
        type: String,
        required: true,
        enum: this.userTypes
      },
      color: {
        type: String,
        required: true,
        trim: true
      }
    };
  }

  protected setupPlugins(): void {
    this.schema.plugin(passportLocalMongoose);
  }

  protected setupSchemaMethods():void {
    this.schema.set('toJSON', {
      transform: function(doc, user) {
        delete user.salt;
        delete user.hash;
        return user;
      }
    });

    let self = this;
    this.schema.methods.updatePassword = function(newPassword: string): Q.Promise<any> {
      if (!newPassword) return Q(this);
      return self.updatePassword(this, newPassword);
    };
  }

  protected updatePassword(user: any, newPassword: string): Q.Promise<any> {
    let deferred = Q.defer();
    user.setPassword(newPassword, err => {
      if (err) return deferred.reject(err);
      deferred.resolve(user);
    });
    return deferred.promise;
  }

  public register(userData: any, password: string, callback: Function): void {
    this.$.register(userData, password, callback);
  }

  public authBasic(): Function {
    return this.$.authenticate();
  }

  public authLocal(): Function {
    return this.$.authenticate();
  }

  public serializeUser(): Function {
    return this.$.serializeUser();
  }

  public deserializeUser(): Function {
    return this.$.deserializeUser();
  }
}

