import * as _ from "lodash";
import * as passport from "passport";
import RdsController from '../../../../lib/controller';

export default class UsersController extends RdsController {

  me(req, res) {
    let user = req.user;
    if (user) {
      res.json(user);
    } else {
      res.json(false);
    }
  }

  login(req, res) {
    passport.authenticate('local', (err, user) => {
      if (!err && user) {
        req.login(user, () => {
          this.app.log('info', 'User logged in ' + user.username);
          res.json(user.toJSON());
        });
      } else {
        this.app.log('error', 'User login failed attempt ' + req.username, err);
        res.status(401).json(false);
      }
    })(req, res);
  }

  logout(req, res) {
    req.logout();
    res.json(true);
  }

  create(req, res) {
    let user = new this.model.$(req.body);
    user.symbol = user.username ? _.upperCase(user.username.substr(0, 1)) : null;
    user.type = 'student';
    user.color = '#000';
    user.displayName = user.username;
    if (req.user) user.createdBy = req.user;
    if (req.user) user.modifiedBy = req.user;
    this.model.register(user, req.body.password, err => {
      if (err) return res.status(500).json({message: 'FATAL ERROR ABORT THE MISSION'});
      res.json(user);
    });
  }

  update(req, res) {
    let document = req.document;
    let newPassword = req.body.password;
    this.model.extendDocument(document, req.body);
    document.modified = Date.now();
    if (req.user) document.modifiedBy = req.user;

    document
      .updatePassword(newPassword)
      .then(document => document.save())
      .then(document => {
        res.json(document);
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({message: 'FATAL ERROR ABORT THE MISSION'});
      });
  }

  all(req, res) {
    this.model.$
      .find({})
      .populate('createdBy', 'displayName username symbol')
      .populate('modifiedBy', 'displayName username symbol')
      .exec()
      .then((documents) => {
        let users = [];
        _.forEach(documents, document => {
          users.push(document.toJSON());
        });
        users = _.sortBy(users, user => {
          switch(user.type) {
            case 'admin':
              return 0;
            case 'teacher':
              return 1;
            case 'student':
              return 2;
          }
          return 3;
        });
        res.json(users);
      })
      .catch((err) => {
        res.status(500).json({message: 'FATAL ERROR ABORT THE MISSION', errors: err});
      });
  }
}