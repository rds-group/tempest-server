import {ensureLoggedIn} from "connect-ensure-login";
import RdsRouter from "../../../lib/router";
import CardsController from "./controllers/cards";
import CardModule from "../card";
import CardSetsController from "./controllers/card-sets";

export default class CardRouter extends RdsRouter {
  module: CardModule;

  setup() {
    super.setup();

    let cardsController: CardsController = new CardsController(this.app, this.module, 'Card');
    let cardSetsController: CardSetsController = new CardSetsController(this.app, this.module, 'CardSet');

    this.router.param('cardId', (req, res, next: Function, id: string) => cardsController.document(req, res, next, id));

    this.router.route('/api/cards')
      .get((req, res) => cardsController.all(req, res))
      .all(ensureLoggedIn())
      .post((req, res) => cardsController.create(req, res))
      .put((req, res) => cardsController.updateMany(req, res));

    this.router.route('/api/cards/upload')
      .all(ensureLoggedIn())
      .post(this.module.upload.single('file'), (req, res) => cardsController.upload(req, res));

    this.router.route('/api/cards/scrap')
      .all(ensureLoggedIn())
      .post((req, res) => cardsController.scrap(req, res));

    this.router.route('/api/cards/:cardId')
      .get((req, res) => cardsController.show(req, res))
      .all(ensureLoggedIn())
      .put((req, res) => cardsController.update(req, res))
      .delete((req, res) => cardsController.delete(req, res));

    this.router.param('cardSetId', (req, res, next: Function, id: string) => cardSetsController.document(req, res, next, id));

    this.router.route('/api/card-sets')
      .get((req, res) => cardSetsController.all(req, res))
      .all(ensureLoggedIn())
      .post((req, res) => cardSetsController.create(req, res));

    this.router.route('/api/card-sets/:cardSetId')
      .get((req, res) => cardSetsController.show(req, res))
      .all(ensureLoggedIn())
      .put((req, res) => cardSetsController.update(req, res))
      .delete((req, res) => cardSetsController.delete(req, res));
  }
}