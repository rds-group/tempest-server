import * as Q from "q";
import * as path from "path";
import RdsController from '../../../../lib/controller';
import RdsApplication from "../../../../lib/application";
import RdsStandardModule from "../../../../lib/standard-module";
import FileUtils from "../../../../lib/utils/file";
import CardManager from '../utils/card-manager';

export default class CardsController extends RdsController {
  public cardManager: CardManager;
  private libraryImageDir: string;

  constructor(app: RdsApplication, module: RdsStandardModule, modelName: string) {
    super(app, module, modelName);
    this.cardManager = new CardManager();
    this.libraryImageDir = path.join(this.module.publicDir, 'img');
  }

  document(req, res, next: Function, id: string) {
    this.model.$
      .findById(id)
      .populate('createdBy', 'displayName username symbol')
      .populate('modifiedBy', 'displayName username symbol')
      .exec()
      .then(document => {
        if (!document) {
          return res.status(404).json({message: '404 NOT FOUND'});
        }

        req.document = document;
        next();
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({message: 'FATAL ERROR ABORT THE MISSION'});
      });
  }

  create(req, res) {
    let document = new this.model.$(req.body);
    document.data = {objects: []};
    document.library = [];
    document.pinLimit = 2;
    document.version = 1;
    document.createdBy = req.user;
    document.modifiedBy = req.user;

    document
      .save()
      .then(() => {
        res.json(document);
      })
      .catch((err) => {
        console.log(err);
        res.status(500).json({message: 'FATAL ERROR ABORT THE MISSION'});
      });
  }

  update(req, res) {
    let document = req.document;
    this.model.extendDocument(document, req.body);

    document.version++;
    document.modified = Date.now();
    document.modifiedBy = req.user;

    this.cardManager.stripAbsoluteUrls(document);

    document
      .save()
      .then(() => {
        res.json(document);
      })
      .catch((err) => {
        console.log(err);
        res.status(500).json({message: 'FATAL ERROR ABORT THE MISSION'});
      });

  }

  upload(req, res) {
    let cardId: string = req.body.id;
    let fileDir: string = path.join(this.libraryImageDir, cardId);
    let fileName: string = FileUtils.getNiceFileName(req.file.originalname);
    let target: string = path.join(fileDir, fileName);
    FileUtils.createDirIfNotExists(fileDir);
    Q.all([
      this.model.$.findById(cardId).exec(),
      FileUtils.copy(req.file.path, target)
    ])
      .then(values => {
        let card = values[0];
        FileUtils.remove(req.file.path);

        if (!card) {
          this.app.log('error', 'Card image upload error, card not found');
          return res.status(404).json({message: '404 NOT FOUND'});
        }

        res.json({
          message: 'File upload success',
          fileName: fileName
        });
      })
      .catch(err => {
        FileUtils.remove(req.file.path);
        this.app.log('error', 'Card image upload error', err);
        return res.status(500).json({message: 'Card image upload error'});
      });
  }

  all(req, res) {
    this.model.$
      .find({})
      .populate('createdBy', 'displayName username symbol')
      .populate('modifiedBy', 'displayName username symbol')
      .exec()
      .then(documents => {
        res.json(documents);
      })
      .catch(err => {
        res.status(500).json({message: 'FATAL ERROR ABORT THE MISSION', errors: err});
      });
  }

  scrap(req, res) {
    let cardId: string = req.body.id;
    let imageUrl: string = req.body.url;
    let fileDir: string = path.join(this.libraryImageDir, cardId);
    let fileName: string = FileUtils.getNiceFileName(imageUrl);
    let target: string = path.join(fileDir, fileName);
    FileUtils.createDirIfNotExists(fileDir);
    FileUtils.scrap(imageUrl, target)
      .then(() => {
        res.json({
          message: 'File scrap success',
          fileName: fileName
        });
      })
      .catch(err => {
        res.status(500).json({message: 'FATAL ERROR ABORT THE MISSION', errors: err});
      });
  }
}