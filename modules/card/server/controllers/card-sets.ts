import * as Q from "q";
import * as _ from "lodash";
import RdsController from "../../../../lib/controller";
import CardModel from "../models/card";
import CardModule from "../../card";

export default class CardSetsController extends RdsController {
  module: CardModule;

  document(req, res, next: Function, id: string) {
    this.model.$
      .findById(id)
      .populate({
        path: 'cards',
        options: {
          sort: {
            order: 'asc'
          }
        }
      })
      .populate('createdBy', 'displayName username symbol')
      .populate('modifiedBy', 'displayName username symbol')
      .exec()
      .then(document => {
        if (!document) {
          return res.status(404).json({message: '404 NOT FOUND'});
        }

        req.document = document;
        next();
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({message: 'FATAL ERROR ABORT THE MISSION'});
      });
  }

  create(req, res) {
    let document = new this.model.$(req.body);
    document.createdBy = req.user;
    document.modifiedBy = req.user;

    document
      .save()
      .then(() => {
        res.json(document);
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({message: 'FATAL ERROR ABORT THE MISSION'});
      });
  }

  update(req, res) {
    let document = req.document;
    this.model.extendDocument(document, req.body);
    document.modified = Date.now();
    document.modifiedBy = req.user;

    document
      .save()
      .then(document => {
        res.json(document);
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({message: 'FATAL ERROR ABORT THE MISSION'});
      });
  }

  all(req, res) {
    this.model.$
      .find({})
      .populate('createdBy', 'displayName username symbol')
      .populate('modifiedBy', 'displayName username symbol')
      .exec()
      .then(cardSets => this.addPreviews(cardSets))
      .then(cardSets => res.json(cardSets))
      .catch(err => {
        res.status(500).json({message: 'FATAL ERROR ABORT THE MISSION', errors: err});
      });
  }

  private addPreviews(cardSets) {
    let promises = [];
    let cardModel: CardModel = <CardModel>this.module.getModel('Card');
    _.forEach(cardSets, (cardSet, i) => {
      cardSets[i] = cardSet = cardSet.toObject();
      let firstCardId = _.head(cardSet.cards);
      if (firstCardId) {
        promises.push(cardModel.$
          .findOne({_id: firstCardId})
          .select('preview')
          .exec()
          .then(card => cardSet.preview = this.module.urlManager.buildCardSetPreviewUrl(card))
        );
      }
    });
    return Q.all(promises).then(() => cardSets);
  }

}