import * as ip from 'ip';

export default class UrlManager {
  private server: any;
  private serverUrl: string;

  constructor(server: any) {
    this.server = server;
    this.serverUrl = this.buildServerAddress();
  }

  buildServerAddress():string {
    let host = ip.address();
    let port = this.server.address().port;
    return 'http://' + host + ':' + port;
  }

  buildCardSetPreviewUrl(card: any):string {
    if (!card || !card.preview) return null;
    return this.serverUrl + '/card/img/' + card._id + '/' + card.preview;
  }
}