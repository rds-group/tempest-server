import * as path from 'path';
import * as Q from 'q';
import * as _ from 'lodash';
import * as fs from 'fs';
import {CronJob} from 'cron';
import RdsApplication from "../../../../lib/application";
import CardModule from "../../card";
import CardModel from "../models/card";

export default class Janitor {
  app: RdsApplication;
  module: CardModule;
  cleanImagesJob: CronJob;
  cardModel: CardModel;
  private libraryImageDir: string;

  constructor(app: RdsApplication, module: CardModule) {
    this.app = app;
    this.module = module;
    this.libraryImageDir = path.join(this.module.publicDir, 'img');
    this.setup();
  }

  setup() {
    this.cardModel = this.module.getModel('Card');
    this.cleanImagesJob = new CronJob('0 30 4 * * *', () => this.cleanImages());
    this.cleanImagesJob.start();
    this.app.log('info', 'Card Janitor setup complete');
  }

  private cleanImages() {
    this.app.log('info', 'Janitor clean images task start');
    this.cardModel.$
      .find({})
      .exec()
      .then(cards => this.cleanCardsImages(cards))
      .then(() => {
        this.app.log('info', 'Janitor clean images task success');
      })
      .catch(err => {
        this.app.log('info', 'Janitor clean images find failure', err);
      });
  }

  private cleanCardsImages(cards: any[]): Q.Promise<any> {
    let removes: Function[] = [];
    _.forEach(cards, () => removes.push(() => this.cleanCardImages(cards.shift())));
    return removes.reduce(Q.when, Q(null));
  }

  private cleanCardImages(card: any): Q.Promise<any> {
    this.app.log('info', 'Janitor cleaning card ' + card._id + ' ' + card.name);
    let deferred = Q.defer();
    if (card) {
      let cardImgDir: string = path.join(this.libraryImageDir, card._id.toString());
      let validCardFiles: string[] = this.getValidCardFiles(card);
      let cardFilesAtDisk: string[] = this.getCardFilesAtDisk(card);
      let filesToRemove: string[] = _.difference(cardFilesAtDisk, validCardFiles);
      let removalPromises: Q.Promise[] = [];
      let unlink = Q.nfbind(fs.unlink);
      _.forEach(filesToRemove, file => {
        let filePath: string = path.join(cardImgDir, file);
        removalPromises.push(unlink(filePath));
      });
      return Q.all(removalPromises);
    } else {
      deferred.reject(new Error('Card does not exists'));
    }
    return deferred.promise;
  }

  private getValidCardFiles(card: any): string[] {
    let validCardFiles: string[] = [];
    if (card.data && card.data.objects) {
      _.forEach(card.data.objects, fabricObject => {
        if (fabricObject.type == 'image') {
          let parsed: any = path.parse(fabricObject.src);
          validCardFiles.push(parsed.base);
        }
      });
    }
    if (card.preview && card.preview.length > 0) {
      validCardFiles.push(card.preview);
    }
    return validCardFiles;
  }

  private getCardFilesAtDisk(card: any): string[] {
    let cardFilesAtDisk: string[] = [];
    let cardImgDir: string = path.join(this.libraryImageDir, card._id.toString());
    if (fs.existsSync(cardImgDir)) cardFilesAtDisk = fs.readdirSync(cardImgDir);
    return cardFilesAtDisk;
  }

}
