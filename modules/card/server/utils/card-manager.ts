import * as _ from 'lodash';
import * as url from 'url';

import {IFabricImage} from './fabric-types';

export default class CardManager{

  constructor() {
  }

  stripAbsoluteUrls(card: any) {
    if (!card || !card.data || !card.data.objects) return;

    _.forEach(card.data.objects, fabricObject => {
      switch(fabricObject.type) {
        case 'image':
          let fabricImage: IFabricImage = fabricObject;
          let parsedSrc = url.parse(fabricImage.src);
          fabricImage.src = parsedSrc.pathname;
      }
    });
  }
}