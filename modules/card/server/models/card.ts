import RdsModel from './../../../../lib/model';
import RdsApplication from "../../../../lib/application";

export default class CardModel extends RdsModel {
  constructor(app: RdsApplication) {
    super(app);
    this.name = 'Card';
    this.schemaDefinition = {
      name: {
        type: String,
        trim: true
      },
      cardSet: {
        type: this.mongooseSchema.ObjectId,
        ref: 'CardSet'
      },
      version: {
        type: Number,
        required: true
      },
      pinLimit: {
        type: Number,
        default: 2
      },
      comments: {
        type: String
      },
      data: {
        type: this.mongooseSchema.Types.Mixed,
        required: true
      },
      preview: {
        type: String
      },
      order: {
        type: Number,
        default: Number.MAX_SAFE_INTEGER
      }
    };
  }
}
