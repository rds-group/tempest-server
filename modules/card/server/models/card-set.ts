import RdsModel from './../../../../lib/model';
import RdsApplication from "../../../../lib/application";

export default class CardSetModel extends RdsModel {
  constructor(app: RdsApplication) {
    super(app);
    this.name = 'CardSet';
    this.schemaDefinition = {
      name: {
        type: String,
        required: true,
        trim: true
      },
      tags: [{
        type: String,
        trim: true
      }],
      cards: [{
        type: this.mongooseSchema.ObjectId,
        ref: 'Card'
      }]
    };
  }
}
