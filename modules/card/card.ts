import * as path from "path";
import * as multer from "multer";
import * as express from "express";
import RdsApplication from './../../lib/application';
import RdsStandardModule from "../../lib/standard-module";
import CardSetModel from "./server/models/card-set";
import CardModel from './server/models/card';
import CardRouter from "./server/router";
import UrlManager from "./server/utils/url-manager";
import Janitor from "./server/utils/janitor";

export default class CardModule extends RdsStandardModule {
  uploadDir: string;
  upload: any;
  urlManager: UrlManager;
  janitor: Janitor;

  constructor(app: RdsApplication) {
    super(app);

    this.name = 'card';
    this.moduleDir = __dirname;
    this.modelClasses.push(CardSetModel, CardModel);
    this.routerClasses.push(CardRouter);
    this.urlManager = new UrlManager(this.app.server.httpServer);
  }

  setup() {
    super.setup();
    this.janitor = new Janitor(this.app, this);
  }

  setupDirs() {
    super.setupDirs();
    this.uploadDir = path.join(this.serverDir, 'uploads');
  }

  setupStatic() {
    this.app.express.use('/card/', express.static(this.publicDir));
  }

  setupUploads() {
    this.upload = multer({
      dest: this.uploadDir
    });
  }
}