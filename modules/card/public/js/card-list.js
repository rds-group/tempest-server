$(document).ready(function () {
  function create() {
    var card = {
      name: $('#name').val(),
      version: 1,
      data: {}
    };
    $.ajax({
      method: 'POST',
      url: '/api/cards',
      data: JSON.stringify(card),
      dataType: 'json',
      contentType: 'application/json'
    }).done(function () {
      window.location.href = window.location.href;
    }).fail(function (response) {
      alert(response);
    });
  }

  $('#create').on('click', create);
});
