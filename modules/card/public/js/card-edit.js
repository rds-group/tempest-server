$(document).ready(function () {
  var canvas = new fabric.Canvas('card', {width: 1280, height: 720});

  function loadImages(cardId) {
    for(var i = 1; i < 13; i++) {
      let src = 'http://10.10.112.13:3002/card/img/' + cardId + '/' + i + '.jpg';
      let image = $('<img>');
      image.attr('src', src);
      image.css('display', 'none');
      $('.images').append(image);
      image.on('load', function() {
        image.css('display', 'inline');
      });
      image.on('click', function() {
        fabric.Image.fromURL(src, function(oImg) {
          canvas.add(oImg);
        });
      });
    }
  }

  function serializeCard() {
    var card = {
      _id: $('#card-id').val(),
      name: $('#card-name').val(),
      version: $('#card-version').val(),
      data: canvas.toObject()
    };

    return JSON.stringify(card);
  }

  function load(cardId) {
    $.ajax({
      method: 'GET',
      url: '/api/cards/' + cardId,
      dataType: 'json',
    }).done(function (response) {
      $('#card-name').val(response.name);
      $('#card-version').val(response.version);
      canvas.loadFromJSON(response.data, canvas.renderAll.bind(canvas));
    });
  }

  loadImages($('#card-id').val());
  load($('#card-id').val());

  function save() {
    $.ajax({
      method: 'PUT',
      url: '/api/cards/' + $('#card-id').val(),
      data: serializeCard(),
      dataType: 'json',
      contentType: 'application/json'
    }).done(function (response) {
      $('#card-version').val(response.version);
      console.log('success', response);
    }).fail(function (response) {
      console.log('error', response);
    });
  }

  $('#card-form').on('submit', function (e) {
    e.preventDefault();
    save();
  });
});
