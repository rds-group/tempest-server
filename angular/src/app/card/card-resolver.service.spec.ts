import {TestBed, inject} from '@angular/core/testing';

import {CardResolver} from './card-resolver.service';

describe('CardResolver', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CardResolver]
    });
  });

  it('should ...', inject([CardResolver], (service: CardResolver) => {
    expect(service).toBeTruthy();
  }));
});
