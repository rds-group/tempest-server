import {Injectable} from '@angular/core';
import {ApiService} from "../shared/api.service";
import {Observable} from "rxjs/Observable";
import {Card} from "./card";
import {RequestMethod} from "@angular/http";
import {ScrapInfo} from "./scrap-info";

@Injectable()
export class CardService {

  constructor(private _apiService: ApiService) {
  }

  public get(id: string): Observable<Card> {
    return this._apiService.makeRequest('cards/' + id, RequestMethod.Get);
  }

  public create(card: Card): Observable<Card> {
    return this._apiService.makeRequest('cards', RequestMethod.Post, null, card);
  }

  public update(card: Card): Observable<Card> {
    return this._apiService.makeRequest('cards/' + card._id, RequestMethod.Put, null, card);
  }

  public updateMany(cards: Card[]): Observable<boolean> {
    return this._apiService.makeRequest('cards', RequestMethod.Put, null, cards);
  }

  public delete(card: Card): Observable<Card> {
    return this._apiService.makeRequest('cards/' + card._id, RequestMethod.Delete, null, card);
  }

  public list(): Observable<Card[]> {
    return this._apiService.makeRequest('cards', RequestMethod.Get);
  }

  public scrap(info: ScrapInfo): Observable<any> {
    return this._apiService.makeRequest('cards/scrap', RequestMethod.Post, null, info);
  }

}
