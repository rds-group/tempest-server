import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CardListComponent} from './card-set-edit/card-list/card-list.component';
import {RouterModule} from "@angular/router";
import {routes} from "./routes";
import {FormsModule} from "@angular/forms";
import {SharedModule} from "../shared/shared.module";
import {CoreModule} from "../core/core.module";
import {CardService} from "./card.service";
import {CardResolver} from "./card-resolver.service";
import {CardEditComponent} from './card-edit/card-edit.component';
import {FileUploadModule} from "ng2-file-upload";
import {ImageAdditionComponent} from './card-edit/image-addition/image-addition.component';
import {CanvasComponent} from './card-edit/canvas/canvas.component';
import {PreviewService} from "./preview.service";
import {CardPreviewComponent} from './card-preview/card-preview.component';
import {FabricTextService} from "./card-edit/canvas/fabric-text.service";
import {CardSetService} from "./card-set.service";
import {CardSetResolver} from "./card-set-resolver.service";
import { CardSetListComponent } from './card-set-list/card-set-list.component';
import { CardSetEditComponent } from './card-set-edit/card-set-edit.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    SharedModule,
    CoreModule,
    FileUploadModule
  ],
  providers: [
    CardService,
    CardResolver,
    CardSetService,
    CardSetResolver,
    PreviewService,
    FabricTextService
  ],
  declarations: [
    CardListComponent,
    CardEditComponent,
    ImageAdditionComponent,
    CanvasComponent,
    CardPreviewComponent,
    CardSetListComponent,
    CardSetEditComponent
  ]
})
export class CardModule {
}
