import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from "@angular/router";
import {CardSetService} from "./card-set.service";
import {CardSet} from "./card-set";
import {Observable} from "rxjs/Observable";

@Injectable()
export class CardSetResolver implements Resolve<CardSet> {

  constructor(private _cardSetService: CardSetService, private _router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<CardSet> {
    let id = route.params['id'];
    return this._cardSetService.get(id)
      .do(cardSet => {
        if (!cardSet) {
          this._router.navigate(['/card-set/list']);
        }
      });
  }

}
