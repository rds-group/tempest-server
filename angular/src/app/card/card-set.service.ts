import {Injectable} from '@angular/core';
import {Observable} from "rxjs/Observable";
import {ApiService} from "../shared/api.service";
import {CardSet} from "./card-set";
import {RequestMethod} from "@angular/http";

@Injectable()
export class CardSetService {

  constructor(private _apiService: ApiService) {
  }

  public get(id: string): Observable<CardSet> {
    return this._apiService.makeRequest('card-sets/' + id, RequestMethod.Get);
  }

  public create(cardSet: CardSet): Observable<CardSet> {
    return this._apiService.makeRequest('card-sets', RequestMethod.Post, null, cardSet);
  }

  public update(cardSet: CardSet): Observable<CardSet> {
    return this._apiService.makeRequest('card-sets/' + cardSet._id, RequestMethod.Put, null, cardSet);
  }

  public delete(cardSet: CardSet): Observable<CardSet> {
    return this._apiService.makeRequest('card-sets/' + cardSet._id, RequestMethod.Delete, null, cardSet);
  }

  public list(): Observable<CardSet[]> {
    return this._apiService.makeRequest('card-sets', RequestMethod.Get);
  }
}
