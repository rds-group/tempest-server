import {Injectable, NgZone} from '@angular/core';
import {FileItem, FileUploader, FileUploaderOptions} from "ng2-file-upload";
import {ApiService} from "../shared/api.service";
import {Card} from "./card";

@Injectable()
export class PreviewService {

  private _previewUploadOptions: FileUploaderOptions;

  constructor(private _apiService: ApiService, private _ngZone: NgZone) {
    this._previewUploadOptions = {
      url: this._apiService.URL + 'cards/upload',
      autoUpload: true,
      removeAfterUpload: true,
      additionalParameter: {
        id: null
      }
    };
  }

  save(dataUrl: string, card: Card): Promise<string> {
    return this.dataUrlToFile(dataUrl)
      .then(file => this.upload(file, card));
  }

  private dataUrlToFile(dataUrl: string): Promise<File> {
    return fetch(dataUrl)
      .then(response => response.arrayBuffer())
      .then(buffer => new File([buffer], 'preview.png', {type: 'image/png'}));
  }

  private upload(file: File, card: Card): Promise<string> {
    return new Promise(resolve => {
      this._previewUploadOptions.additionalParameter.id = card._id;
      let uploader: FileUploader = new FileUploader(this._previewUploadOptions);
      uploader.onCompleteItem = (item: FileItem, responseString: string) => {
        let response = JSON.parse(responseString);
        this._ngZone.run(() => resolve(response.fileName));
      };
      uploader.addToQueue([file]);
    });
  }

}
