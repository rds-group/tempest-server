import {Routes} from "@angular/router";
import {AuthGuard} from "../user/auth-guard.service";
import {CardResolver} from "./card-resolver.service";
import {CardEditComponent} from "./card-edit/card-edit.component";
import {CardSetListComponent} from "./card-set-list/card-set-list.component";
import {CardSetEditComponent} from "./card-set-edit/card-set-edit.component";
import {CardSetResolver} from "./card-set-resolver.service";

export const routes: Routes = [
  {
    path: 'card-set',
    canActivate: [AuthGuard],
    children: [
      {
        path: 'list',
        component: CardSetListComponent
      },
      {
        path: 'edit/:id',
        component: CardSetEditComponent,
        resolve: {
          card: CardSetResolver
        }
      }
    ]
  },
  {
    path: 'card',
    canActivate: [AuthGuard],
    children: [
      {
        path: 'edit/:id',
        component: CardEditComponent,
        resolve: {
          card: CardResolver
        }
      }
    ]
  }
];
