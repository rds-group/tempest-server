import {TestBed, inject} from '@angular/core/testing';

import {CardSetResolver} from './card-set-resolver.service';

describe('CardSetResolver', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CardSetResolver]
    });
  });

  it('should ...', inject([CardSetResolver], (service: CardSetResolver) => {
    expect(service).toBeTruthy();
  }));
});
