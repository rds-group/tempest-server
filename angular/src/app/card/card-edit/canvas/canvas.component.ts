import {Component, Input, OnInit} from '@angular/core';
import {Card} from "../../card";
import {PreviewService} from "../../preview.service";
import {FabricTextService} from "./fabric-text.service";
import {TextStyle} from "./text-style";
import * as _ from "lodash";
import {Image} from "../../image";
import {ApiService} from "../../../shared/api.service";

declare const fabric: any;

const CANVAS_WIDTH: number = 1280;
const CANVAS_HEIGHT: number = 720;

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.scss']
})
export class CanvasComponent implements OnInit {

  @Input() card: Card;

  private _canvas;
  private _previewOptions = {format: 'png', multiplier: 0.2};
  private _backgroundColor: string = '#FFFFFF';
  private _defaultObjectPosition: object = {left: 100, top: 100};
  private _selectedObject: any = null;
  private _defaultFigureColor: string = '#2E7D32';
  private _figureColor: string = this._defaultFigureColor;
  private _textStyles: TextStyle[];
  private _textStyle: TextStyle = null;
  private _fontFamilies: string[];
  private _fontFamily: string = null;
  private _textSizes: number[];
  private _textSize: number = null;
  private _textColor: string;

  private _imageUploadActive: boolean;

  constructor(private _previewService: PreviewService,
              private _fabricTextService: FabricTextService,
              private _apiService: ApiService) {
    this._fabricTextService.textStyles$.subscribe(textStyles => this._textStyles = textStyles);
    this._fabricTextService.fontFamilies$.subscribe(fontFamilies => this._fontFamilies = fontFamilies);
    this._fabricTextService.textSizes$.subscribe(textSizes => this._textSizes = textSizes);
  }

  ngOnInit() {
    this.initCanvas();
    this._imageUploadActive = false;
  }

  onObjectSelect(object: any) {
    this._selectedObject = object;

    if (this._selectedObject.type === 'i-text') this.onTextObjectSelect();
    if (this._selectedObject.type === 'rect' || this._selectedObject.type === 'circle') this.onFigureObjectSelect();
  }

  onObjectDeselect() {
    this._selectedObject = null;
  }

  onFigureObjectSelect() {
    this._figureColor = this._selectedObject.fill;
  }

  onTextObjectSelect() {
    this._textStyle = this._fabricTextService.getMatchingTextStyle(this._selectedObject);
    this._fontFamily = this._selectedObject.fontFamily;
    this._textSize = this._selectedObject.fontSize;
    this._textColor = this._selectedObject.fill;
  }

  addObjectToCanvas(object) {
    this._canvas.add(object);
  }

  onObjectAdded(object) {
    object.on('removed', event => this.onObjectRemoved(event));
  }

  onObjectRemoved(object) {
    console.log('Removed', object);
  }

  addImage(imageUrl: string) {
    fabric.Image.fromURL(imageUrl, image => this.addObjectToCanvas(image), {crossOrigin: 'use-credentials'});
  }

  onRemoveSelectedObjectClick() {
    this._canvas.getActiveObject().remove();
  }

  onAddImageClick() {
    this._imageUploadActive = !this._imageUploadActive;
  }

  onCloseImageUploadClick() {
    this._imageUploadActive = false;
  }

  onAddLibraryItemToCard(image: Image) {
    let absoluteImageUrl: string = this._apiService.base + '/card/img/' + this.card._id + '/' + image.fileName;
    this.addImage(absoluteImageUrl);
    this._imageUploadActive = false;
  }

  onAddTextFieldClick() {
    this.addObjectToCanvas(new fabric.IText('empty text', _.merge(
      this._defaultObjectPosition,
      this._fabricTextService.getDefaultTextProperties()
    )));
  }

  onAddSquareClick() {
    this.addObjectToCanvas(new fabric.Rect(_.merge(
      this._defaultObjectPosition,
      {width: 200, height: 150, fill: this._defaultFigureColor}
    )));
  }

  onAddCircleClick() {
    this.addObjectToCanvas(new fabric.Circle(_.merge(
      this._defaultObjectPosition,
      {width: 200, height: 150, radius: 100, fill: this._defaultFigureColor}
    )));
  }

  onBackgroundColorChange(colorHex: string) {
    let color = new fabric.Color(colorHex);
    this._canvas.setBackgroundColor(color.toRgba(), () => {
      this._canvas.renderAll.bind(this._canvas)();
    });
  }

  onBringToFrontClick() {
    this._selectedObject.bringToFront();
  }

  onBringToBackClick() {
    this._selectedObject.sendToBack();
  }

  onLayerUpClick() {
    this._selectedObject.bringForward();
  }

  onLayerDownClick() {
    this._selectedObject.sendBackwards();
  }

  onFigureColorChange(colorHex: string) {
    this._figureColor = colorHex;
    this._selectedObject.setFill(this._figureColor);
    this._canvas.renderAll.bind(this._canvas)();
  }

  onTextStyleChange() {
    if (this._selectedObject.type === 'i-text') {
      this._selectedObject.setFontWeight(this._textStyle.fontWeight);
      this._selectedObject.setFontStyle(this._textStyle.fontStyle);
      this._canvas.renderAll.bind(this._canvas)();
    }
  }

  onFontFamilyChange() {
    if (this._selectedObject.type === 'i-text') {
      this._selectedObject.setFontFamily(this._fontFamily);
      this._canvas.renderAll.bind(this._canvas)();
    }
  }

  onTextSizeChange() {
    if (this._selectedObject.type === 'i-text') {
      this._selectedObject.setFontSize(this._textSize);
      this._canvas.renderAll.bind(this._canvas)();
    }
  }

  onTextColorChange(colorHex: string) {
    if (this._selectedObject.type === 'i-text') {
      this._textColor = colorHex;
      this._selectedObject.setFill(this._textColor);
      this._canvas.renderAll.bind(this._canvas)();
    }
  }

  serialize(): Promise<null> {
    return new Promise(resolve => {
      this.card.data = this._canvas.toJSON();
      this._previewService.save(this._canvas.toDataURL(this._previewOptions), this.card)
        .then(fileName => {
          this.card.preview = fileName;
          resolve();
        });
    });
  }

  private deserialize(): Promise<null> {
    return new Promise(resolve => {
      if (this.card.data.background) {
        let color = fabric.Color.fromRgba(this.card.data.background);
        this._backgroundColor = '#' + color.toHex();
      }
      this._canvas.loadFromJSON(this.card.data, () => {
        let color = new fabric.Color(this._backgroundColor);
        this._canvas.setBackgroundColor(color.toRgba(), () => {
          this._canvas.renderAll.bind(this._canvas)();
          resolve();
        });
      });
    });
  }

  private initCanvas() {
    this._canvas = new fabric.Canvas('canvas', {
      width: CANVAS_WIDTH,
      height: CANVAS_HEIGHT,
      preserveObjectStacking: true
    });

    this.deserialize()
      .then(() => {
        this._canvas.on('object:selected', event => this.onObjectSelect(event.target));
        this._canvas.on('selection:cleared', () => this.onObjectDeselect());
        this._canvas.on('object:added', event => this.onObjectAdded(event.target));

        let objects: any[] = this._canvas.getObjects();
        _.forEach(objects, object => {
          object.on('removed', event => this.onObjectRemoved(event));
        });
      });
  }

}
