export class TextProperties {
  fontFamily: string;
  fontStyle: string;
  fontWeight: number;
  fontSize: number;
  fill: string;
}
