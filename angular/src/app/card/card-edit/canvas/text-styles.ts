import {TextStyle} from "./text-style";

export const TEXT_STYLES: TextStyle[] = [
  {
    name: 'Regular',
    fontStyle: 'normal',
    fontWeight: 400
  },
  {
    name: 'Bold',
    fontStyle: 'normal',
    fontWeight: 700
  },
  {
    name: 'Italic',
    fontStyle: 'italic',
    fontWeight: 400
  },
  {
    name: 'Bold Italic',
    fontStyle: 'italic',
    fontWeight: 700
  },
];
