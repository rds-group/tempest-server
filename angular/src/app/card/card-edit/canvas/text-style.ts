export class TextStyle {
  name: string;
  fontStyle: string;
  fontWeight: number;
}
