import {TestBed, inject} from '@angular/core/testing';

import {FabricTextService} from './fabric-text.service';

describe('FabricTextService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FabricTextService]
    });
  });

  it('should ...', inject([FabricTextService], (service: FabricTextService) => {
    expect(service).toBeTruthy();
  }));
});
