import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {TextStyle} from "./text-style";
import * as _ from "lodash";

import {TEXT_STYLES} from "./text-styles";
import {TextProperties} from "./text-properties";

@Injectable()
export class FabricTextService {

  public textStyles$: BehaviorSubject<TextStyle[]> = new BehaviorSubject(null);
  public fontFamilies$: BehaviorSubject<string[]> = new BehaviorSubject(null);
  public textSizes$: BehaviorSubject<number[]> = new BehaviorSubject(null);

  constructor() {
    this.initTextStyles();
    this.initFontFamilies();
    this.initTextSizes();
  }

  initTextStyles() {
    this.textStyles$.next(TEXT_STYLES);
  }

  initFontFamilies() {
    this.fontFamilies$.next([
      'Arial',
      'Times New Roman',
      'Courier New',
      'Comic Sans MS',
      'Helvetica',
      'Tahoma',
      'Verdana'
    ]);
  }

  initTextSizes() {
    this.textSizes$.next([24, 30, 36, 42, 48, 54, 60, 70, 80]);
  }

  getDefaultTextProperties(): TextProperties {
    let defaults: TextProperties = new TextProperties();
    let defaultStyles: TextStyle = _.head(this.textStyles$.getValue());
    defaults.fontStyle = defaultStyles.fontStyle;
    defaults.fontWeight = defaultStyles.fontWeight;
    defaults.fontFamily = _.head(this.fontFamilies$.getValue());
    defaults.fontSize = 42;
    defaults.fill = '#212121';
    return defaults;
  }

  getMatchingTextStyle(object: any): TextStyle {
    return _.find(this.textStyles$.getValue(), {
      fontStyle: object.fontStyle,
      fontWeight: object.fontWeight,
    });
  }

}
