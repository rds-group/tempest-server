import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {CardService} from "../card.service";
import {Card} from "../card";
import {ActivatedRoute, Router} from "@angular/router";
import {CanvasComponent} from "./canvas/canvas.component";
import {ApiService} from "../../shared/api.service";
import * as _ from "lodash";

@Component({
  selector: 'app-card-edit',
  templateUrl: './card-edit.component.html',
  styleUrls: ['./card-edit.component.scss']
})
export class CardEditComponent implements OnInit {

  @ViewChild(CanvasComponent) private _canvasComponent: CanvasComponent;
  private _card: Card;

  constructor(private _apiService: ApiService,
              private _cardService: CardService,
              private _route: ActivatedRoute,
              private _router: Router) {
  }

  ngOnInit() {
    this._route.data
      .subscribe((data: { card: Card }) => {
        this._card = data.card;
        this.fixUrls();
      });
  }

  onSubmit(f: NgForm) {
    if (f.valid) {
      this._canvasComponent.serialize()
        .then(() => {
          this._cardService.update(this._card)
            .subscribe(() => this.backToCardSet());
        });
    }
  }

  onBackClick() {
    this.backToCardSet();
  }

  onCancelClick() {
    this.backToCardSet();
  }

  backToCardSet() {
    this._router.navigate(['/card-set/edit/' + this._card.cardSet]);
  }

  fixUrls() {
    if (this._card.data && this._card.data.objects) {
      _.forEach(this._card.data.objects, object => {
        if (object.type === 'image') {
          object.src = this._apiService.base + object.src;
        }
      });
    }
  }

}
