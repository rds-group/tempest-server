import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FileUploaderOptions} from "ng2-file-upload";
import {ApiService} from "../../../shared/api.service";
import {Card} from "../../card";
import {Image} from "../../image";
import {CardService} from "../../card.service";
import {ScrapInfo} from "../../scrap-info";
import {HttpToolsService} from "../../../shared/http-tools.service";

@Component({
  selector: 'app-image-addition',
  templateUrl: './image-addition.component.html',
  styleUrls: ['./image-addition.component.scss']
})
export class ImageAdditionComponent implements OnInit {

  @Input() card: Card;
  @Output() addToCard: EventEmitter<Image> = new EventEmitter();

  private _image: Image;
  private _imageUploadOptions: FileUploaderOptions;
  private _imageUrl: string;
  private _pastedImageUrl: string;
  private _pastedLegitImageUrl: string;
  private _error: Error;

  constructor(private _apiService: ApiService,
              private _cardService: CardService) {
  }

  ngOnInit() {
    this._imageUploadOptions = {
      url: this._apiService.URL + 'cards/upload',
      queueLimit: 1,
      autoUpload: true,
      removeAfterUpload: true,
      //allowedMimeType: ['image/*'],
      //maxFileSize: 5*1024*1024,
      additionalParameter: {
        id: this.card._id
      }
    };
    this._imageUrl = this._apiService.base + '/card/img/' + this.card._id + '/';
    this._image = new Image();
  }

  onUploadFinished() {
    this.addToCard.emit(this._image);
    this._image = new Image();
  }

  onPastedImageUrlChange(url: string) {
    this._pastedImageUrl = url;
    this._error = null;
  }

  onPastedUrlPreviewClick() {
    if (this._pastedImageUrl && HttpToolsService.validUrl(this._pastedImageUrl)) {
        this._pastedLegitImageUrl = this._pastedImageUrl;
    } else {
      this._error = new Error('Url is incorrect');
    }
  }

  onPastedUrlAcceptClick() {
    if (this._pastedImageUrl && HttpToolsService.validUrl(this._pastedImageUrl)) {
      let info: ScrapInfo = {
        id: this.card._id,
        url: this._pastedImageUrl
      };

      this._cardService.scrap(info)
        .subscribe(response => {
          let image = new Image();
          image.fileName = response.fileName;
          this.addToCard.emit(image);
          this._pastedLegitImageUrl = null;
          this._pastedImageUrl = null
        });
    } else {
      this._error = new Error('Url is incorrect');
    }
  }

}
