import {Component, OnInit} from '@angular/core';
import {CardSet} from "../card-set";
import {CardSetService} from "../card-set.service";
import {Router} from "@angular/router";
import {NgForm} from "@angular/forms";
import * as _ from "lodash";
import {PopupService} from "../../shared/popup.service";

@Component({
  selector: 'app-card-set-list',
  templateUrl: './card-set-list.component.html',
  styleUrls: ['./card-set-list.component.scss']
})
export class CardSetListComponent implements OnInit {

  private _cardSets: CardSet[];
  private _filteredCardSets: CardSet[];
  private _newCardSet: CardSet;
  private _tagFilter: string;

  constructor(private _cardSetService: CardSetService, private _router: Router, private _popupService: PopupService) {
  }

  ngOnInit() {
    this.refresh();
    this._newCardSet = new CardSet();
    this._tagFilter = '';
  }

  protected refresh(): void {
    this._cardSetService.list()
      .subscribe(cardSets => {
        this._cardSets = cardSets;
        this.filter();
      });
  }

  onSubmit(f: NgForm) {
    if (f.valid) {
      this._cardSetService.create(this._newCardSet)
        .subscribe(() => {
          this._newCardSet = new CardSet();
          this.refresh();
        });
    }
  }

  onEditClick(cardSet: CardSet): void {
    this._router.navigate(['/card-set/edit', cardSet._id]);
  }

  onRemoveClick(cardSet: CardSet): void {
    let cardSetName: string = _.truncate(cardSet.name, {
      'separator': ' ',
      'omission': ' [...]'
    });
    let msg: string = 'Are you sure you want to remove cardset &quot;' + cardSetName + '&quot; ?';

    this._popupService.open(msg)
      .subscribe(confirm => {
        if (confirm) this.removeCardSet(cardSet);
      })
  }

  removeCardSet(cardSet: CardSet): void {
    this._cardSetService.delete(cardSet)
      .subscribe(() => {
        this.refresh();
      });
  }

  onTagFilterKey() {
    this.filter();
  }

  filter() {
    this._filteredCardSets = _.filter(this._cardSets, cardSet => {
      if (this._tagFilter.length == 0) return true;
      return _.some(cardSet.tags, tag => _.includes(tag, this._tagFilter));
    });
  }

}
