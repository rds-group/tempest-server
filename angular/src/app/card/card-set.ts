import {Card} from "./card";
import {User} from "../user/user";

export class CardSet {
  _id: string;
  name: string;
  preview: string;
  tags: string[];
  cards: Card[] | string[];
  created: string;
  createdBy: User;
  modified: string;
  modifiedBy: User;
}
