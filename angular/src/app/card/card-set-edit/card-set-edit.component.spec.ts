import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CardSetEditComponent} from './card-set-edit.component';

describe('CardSetEditComponent', () => {
  let component: CardSetEditComponent;
  let fixture: ComponentFixture<CardSetEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CardSetEditComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardSetEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
