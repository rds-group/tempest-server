import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Card} from "../../card";
import {CardService} from "../../card.service";
import {Router} from "@angular/router";
import * as _ from "lodash";
import {CardSet} from "../../card-set";
import {PopupService} from "../../../shared/popup.service";

@Component({
  selector: 'app-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.scss']
})
export class CardListComponent implements OnInit {

  @Input() cardSet: CardSet;
  @Output() onCardCreated: EventEmitter<Card> = new EventEmitter();
  @Output() onCardRemoved: EventEmitter<Card> = new EventEmitter();

  constructor(private _cardService: CardService,
              private _router: Router,
              private _popupService: PopupService) {
  }

  ngOnInit() {
  }

  onCardDrop() {
    _.forEach(<Card[]>this.cardSet.cards, (c, i) => c.order = i);
    this._cardService.updateMany(<Card[]>this.cardSet.cards)
      .subscribe(() => {
      });
  }

  onSubmit(f: NgForm) {
    if (f.valid) {
      let card: Card = new Card();
      card.cardSet = this.cardSet._id;
      this._cardService.create(card)
        .subscribe(card => {
          this.onCardCreated.emit(card);
          this.editCard(card);
        });
    }
  }

  onEditClick(card: Card): void {
    this.editCard(card);
  }

  onRemoveClick(card: Card): void {
    let cardName: string = _.truncate(card.name, {
      'separator': ' ',
      'omission': ' [...]'
    });
    let msg: string = 'Are you sure you want to remove card &quot;' + cardName + '&quot; ?';

    this._popupService.open(msg)
      .subscribe(confirm => {
        if (confirm) this.removeCard(card);
      })
  }

  editCard(card: Card): void {
    if (card) {
      this._router.navigate(['/card/edit', card._id]);
    }
  }

  removeCard(card: Card): void {
    this._cardService.delete(card)
      .subscribe(() => this.onCardRemoved.emit(card));
  }

}
