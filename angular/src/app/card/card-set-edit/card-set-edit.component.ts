import {Component, OnInit} from '@angular/core';
import {CardSet} from "../card-set";
import {ApiService} from "../../shared/api.service";
import {CardSetService} from "../card-set.service";
import {ActivatedRoute, Router} from "@angular/router";
import {NgForm} from "@angular/forms";
import {Card} from "../card";
import * as _ from "lodash";

@Component({
  selector: 'app-card-set-edit',
  templateUrl: './card-set-edit.component.html',
  styleUrls: ['./card-set-edit.component.scss']
})
export class CardSetEditComponent implements OnInit {

  private _cardSet: CardSet;

  constructor(private _apiService: ApiService,
              private _cardSetService: CardSetService,
              private _route: ActivatedRoute,
              private _router: Router) {
  }

  ngOnInit() {
    this._route.data
      .subscribe((data: { card: CardSet }) => {
        this._cardSet = data.card;
      });
  }

  onCardCreated(card: Card) {
    (<Card[]>this._cardSet.cards).push(card);
    this._cardSetService.update(this._cardSet)
      .subscribe(cardSet => this._cardSet = cardSet);
  }

  onCardRemoved(card: Card) {
    _.remove(<Card[]>this._cardSet.cards, c => c._id == card._id);
    this._cardSetService.update(this._cardSet)
      .subscribe(cardSet => this._cardSet = cardSet);
  }

  onTagsChange(tags: string[]) {
    this._cardSet.tags = tags;
    this._cardSetService.update(this._cardSet)
      .subscribe(cardSet => this._cardSet = cardSet);
  }

  onSubmit(f: NgForm) {
    if (f.valid) {
      this._cardSetService.update(this._cardSet)
        .subscribe(() => this.backToList());
    }
  }

  onBackClick() {
    this.backToList();
  }

  onCancelClick() {
    this.backToList();
  }

  backToList() {
    this._router.navigate(['/card-set/list']);
  }

}
