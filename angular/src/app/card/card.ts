import {CardSet} from "./card-set";
import {User} from "../user/user";

export class Card {
  _id: string;
  name: string;
  cardSet: CardSet | string;
  version: number;
  pinLimit: number;
  comments: string;
  data: any;
  preview: string;
  order: number;
  created: string;
  createdBy: User;
  modified: string;
  modifiedBy: User;
}
