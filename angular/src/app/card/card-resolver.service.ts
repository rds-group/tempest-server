import {Injectable} from '@angular/core';
import {Observable} from "rxjs/Observable";
import {Card} from "./card";
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from "@angular/router";
import {CardService} from "./card.service";

@Injectable()
export class CardResolver implements Resolve<Card> {

  constructor(private _cardService: CardService, private _router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Card> {
    let id = route.params['id'];
    return this._cardService.get(id)
      .do(card => {
        if (!card) {
          this._router.navigate(['/card/list']);
        }
      });
  }

}
