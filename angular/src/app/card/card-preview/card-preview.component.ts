import {Component, HostBinding, Input, OnInit} from '@angular/core';
import {ApiService} from "../../shared/api.service";
import {Card} from "../card";
import {CardSet} from "../card-set";

@Component({
  selector: 'app-card-preview',
  templateUrl: './card-preview.component.html',
  styleUrls: ['./card-preview.component.scss']
})
export class CardPreviewComponent implements OnInit {

  @Input() card: Card;
  @Input() cardSet: CardSet;
  @Input() size: string;
  @HostBinding('class') hostClass: string = '';
  private _url: string;
  private _noPreview: boolean;
  private _iconSize: string;

  constructor(private _apiService: ApiService) {
  }

  ngOnInit() {
    this._noPreview = true;

    if (this.card) {
      this._url = this._apiService.base + '/card/img/' + this.card._id + '/' + this.card.preview;
      if (this.card.preview) this._noPreview = false;
    }

    if (this.cardSet) {
      this._url = this.cardSet.preview;
      if (this.cardSet.preview) this._noPreview = false;
    }

    switch (this.size) {
      case 'small':
        this.hostClass = 'size-small';
        this._iconSize = 'big';
        break;
      default:
        this.hostClass = 'size-default';
        this._iconSize = 'large';
    }
  }

}
