import {Injectable} from '@angular/core';
import {Http, RequestMethod, RequestOptions, RequestOptionsArgs, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";

@Injectable()
export class ApiService {

  public base: string = 'http://91.219.209.131:3002';
  //public base: string = 'http://localhost:3002';
  public URL: string = this.base + '/api/';

  constructor(private _http: Http) {
  }

  public makeRequest(path: string, type: RequestMethod, params?: URLSearchParams, body?: any): Observable<any> {
    const options: RequestOptionsArgs = new RequestOptions();
    options.withCredentials = true;
    if (params) options.params = params;

    let observable: Observable<Response>;
    switch (type) {
      case RequestMethod.Get:
        observable = this._http.get(this.URL + path, options);
        break;
      case RequestMethod.Delete:
        observable = this._http.delete(this.URL + path, options);
        break;
      case RequestMethod.Post:
        observable = this._http.post(this.URL + path, body, options);
        break;
      case RequestMethod.Put:
        observable = this._http.put(this.URL + path, body, options);
        break;
    }

    return observable
      .map(res => this.onResponse(res))
      .catch(error => this.onError(error));
  }

  private onResponse(res: Response) {
    if (res.ok)
      return res.json();

    return this.onError(res);
  }

  private onError(error: any) {
    let message: string = 'An error occurred.';
    return Observable.throw(message);
  }

}
