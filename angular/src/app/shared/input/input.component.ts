import {Component, EventEmitter, HostBinding, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {LocaleService} from "../locale.service";
import * as _ from "lodash";

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit, OnChanges {

  @Input() type: string;
  @Input() model: any;
  @Output() modelChange: EventEmitter<any> = new EventEmitter();
  @Input() name: string;
  @Input() label: string;
  @Input() locale: boolean;
  @Input() size: string;
  @Input() placeholder: string;

  @HostBinding('class') hostClass: string = '';

  ngModel: any;

  _currentLocale: string;
  _locales: string[];

  constructor(private _localeService: LocaleService) {
    this._locales = this._localeService.locales;
  }

  ngOnInit() {
    if (this.label && !this.placeholder) {
      this.placeholder = _.lowerCase(this.label);
    }
    this.locale = !!this.locale;
    if (!this.type) this.type = 'text';

    switch (this.size) {
      case 'small':
        this.hostClass = 'size-small';
        break;
      case 'big':
        this.hostClass = 'size-big';
        break;
      default:
        this.hostClass = 'size-default';
    }

    this._localeService.locale
      .subscribe(locale => this.onLocaleChange(locale));
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.model) this.ngModel = this.model = changes.model.currentValue;
  }

  onChange(): void {
    if (this.locale) {
      this.model[this._currentLocale] = this.ngModel;
    } else {
      this.model = this.ngModel;
    }
    this.modelChange.emit(this.model);
  }

  onLocaleChange(locale: string): void {
    this._currentLocale = locale;
    if (this.locale) {
      this.ngModel = this.model[this._currentLocale];
    } else {
      this.ngModel = this.model
    }
  }

}
