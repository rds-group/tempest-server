import {Component, HostBinding, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  @Input() icon: string;
  @Input() caption: string;
  @Input() type: string;
  @Input() size: string;
  @Input() cssClass: string;

  @HostBinding('class') hostClass: string = '';

  constructor() {
  }

  ngOnInit() {
    if (!this.type) {
      this.type = 'button';
    }

    switch (this.size) {
      case 'tiny':
        this.hostClass = 'size-tiny';
        break;
      case 'small':
        this.hostClass = 'size-small';
        break;
      case 'big':
        this.hostClass = 'size-big';
        break;
      case 'full':
        this.hostClass = 'size-full';
        break;
      default:
        this.hostClass = 'size-default';
    }
  }

}
