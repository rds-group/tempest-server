import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ApiService} from "./api.service";
import {ColorBadgeComponent} from "./color-badge/color-badge.component";
import {TitleBarComponent} from './title-bar/title-bar.component';
import {IconComponent} from './icon/icon.component';
import {ButtonComponent} from './button/button.component';
import {InputComponent} from './input/input.component';
import {FormsModule} from "@angular/forms";
import {FileUploadModule} from "ng2-file-upload";
import {DndModule} from "ng2-dnd";
import {LocaleService} from "./locale.service";
import {InputUploadComponent} from "./input-upload/input-upload.component";
import {EnumPipe} from './enum.pipe';
import {TagListComponent} from './tag-list/tag-list.component';
import {InfoBadgeComponent} from './info-badge/info-badge.component';
import {HttpToolsService} from "./http-tools.service";
import {PopupService} from "./popup.service";
import {PopupComponent} from "./popup/popup.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FileUploadModule,
    DndModule.forRoot()
  ],
  providers: [
    ApiService,
    LocaleService,
    HttpToolsService,
    PopupService
  ],
  declarations: [
    ColorBadgeComponent,
    TitleBarComponent,
    IconComponent,
    ButtonComponent,
    InputComponent,
    InputUploadComponent,
    TagListComponent,
    InfoBadgeComponent,
    PopupComponent,
    EnumPipe
  ],
  exports: [
    DndModule,
    ColorBadgeComponent,
    TitleBarComponent,
    IconComponent,
    ButtonComponent,
    InputComponent,
    InputUploadComponent,
    TagListComponent,
    InfoBadgeComponent,
    PopupComponent,
    EnumPipe,
  ]
})
export class SharedModule {
}
