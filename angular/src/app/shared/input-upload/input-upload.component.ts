import {Component, EventEmitter, HostBinding, Input, NgZone, OnInit, Output} from '@angular/core';
import {FileItem, FileUploader, FileUploaderOptions} from "ng2-file-upload";

@Component({
  selector: 'app-input-upload',
  templateUrl: './input-upload.component.html',
  styleUrls: ['./input-upload.component.scss']
})
export class InputUploadComponent implements OnInit {

  @Input() model: any;
  @Output() modelChange: EventEmitter<any> = new EventEmitter();
  @Input() name: string;
  @Input() label: string;
  @Input() options: FileUploaderOptions;
  @Input() imageUrl: string;
  @Input() size: string;
  @Input() fileType: string;
  @Input() single: boolean;

  @HostBinding('class') hostClass: string = '';

  private _uploader: FileUploader;
  private _isFileOverDropZone: boolean = false;
  private _progress: number = 0;

  constructor(private _ngZone: NgZone) {
  }

  ngOnInit() {
    this.initUploader();

    if (this.single != true) this.single = false;

    switch (this.size) {
      case 'small':
        this.hostClass = 'size-small';
        break;
      case 'big':
        this.hostClass = 'size-big';
        break;
      default:
        this.hostClass = 'size-default';
    }
  }

  initUploader() {
    this._uploader = new FileUploader(this.options);

    this._uploader.onProgressAll = (progress: number) => {
      this._ngZone.run(() => this._progress = progress);
    };

    this._uploader.onCompleteItem = (item: FileItem, responseString: string) => {
      this._ngZone.run(() => {
        this._progress = 0;
        let response = JSON.parse(responseString);
        this.model = response.fileName;
        this.modelChange.emit(this.model);
        if (this.single) {
          this.model = null;
        }
      });
    };
  }

  public onFileOverDropZone(isOver: boolean): void {
    this._isFileOverDropZone = isOver;
  }

}
