import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-color-badge',
  templateUrl: './color-badge.component.html',
  styleUrls: ['./color-badge.component.scss']
})
export class ColorBadgeComponent implements OnInit {

  @Input() color: string;

  constructor() {
  }

  ngOnInit() {
  }

}
