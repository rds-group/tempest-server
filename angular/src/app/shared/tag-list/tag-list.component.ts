import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgForm} from "@angular/forms";
import * as _ from "lodash";
import {PopupService} from "../popup.service";

@Component({
  selector: 'app-tag-list',
  templateUrl: './tag-list.component.html',
  styleUrls: ['./tag-list.component.scss']
})
export class TagListComponent implements OnInit {

  @Input() list: string[];
  @Output() onListChange: EventEmitter<string[]> = new EventEmitter();

  private _newTag: string;

  constructor(private _popupService: PopupService) {
  }

  ngOnInit() {
    this._newTag = '';
  }

  onSubmit(f: NgForm) {
    if (f.valid) {
      if (_.indexOf(this.list, this._newTag) === -1) {
        this.list.push(this._newTag);
        this.onListChange.emit(this.list);
      }
      this._newTag = '';
    }
  }

  onRemoveClick(tag: string) {
    let msg: string = 'Are you sure you want to remove tag &quot;' + tag + '&quot; ?';

    this._popupService.open(msg)
      .subscribe(confirm => {
        if (confirm) this.removeTag(tag);
      })
  }

  removeTag(tag: string): void {
    _.pull(this.list, tag);
    this.onListChange.emit(this.list);
  }

}
