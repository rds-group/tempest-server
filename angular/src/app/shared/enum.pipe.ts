import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'enum'
})
export class EnumPipe implements PipeTransform {

  transform(enumObject: any): Array<string> {
    let keys = Object.keys(enumObject);
    return keys.slice(keys.length / 2, keys.length);
  }

}
