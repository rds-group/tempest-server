import {Component, HostBinding, OnInit} from '@angular/core';
import {PopupService} from "../popup.service";

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})
export class PopupComponent implements OnInit {

  @HostBinding('class') hostClass: string = '';
  private _message: string = '';


  constructor(private _popupService: PopupService) {
    this._popupService.open$
      .subscribe(msg => this.open(msg))
  }

  ngOnInit() {
  }

  open(message: string) {
    this._message = message;
    this.hostClass = 'active';
  }

  onYesClick() {
    this._popupService.close(true);
    this.hostClass = '';
  }

  onNoClick() {
    this._popupService.close(false);
    this.hostClass = '';
  }

}
