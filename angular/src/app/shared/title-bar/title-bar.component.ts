import {Component, EventEmitter, HostBinding, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-title-bar',
  templateUrl: './title-bar.component.html',
  styleUrls: ['./title-bar.component.scss']
})
export class TitleBarComponent implements OnInit {

  @Input() title: string;
  @Input() size: string;
  @Input() backEnabled: boolean;
  @Output() back: EventEmitter<null> = new EventEmitter();
  @HostBinding('class') hostClass: string = '';

  constructor() {
  }

  ngOnInit() {
    switch (this.size) {
      case 'small':
        this.hostClass = 'size-small';
        break;
      case 'big':
        this.hostClass = 'size-big';
        break;
      default:
        this.hostClass = 'size-default';
    }
  }

  onBackClick() {
    this.back.emit();
  }

}
