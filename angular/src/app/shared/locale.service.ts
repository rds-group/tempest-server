import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs/BehaviorSubject";

const LOCALES: string[] = ['pl', 'en'];

@Injectable()
export class LocaleService {

  public locales: string[] = LOCALES;
  public locale: BehaviorSubject<string> = new BehaviorSubject('pl');

  constructor() {
  }

  public setLocale(locale: string) {
    if (this.locales.indexOf(locale) > -1) {
      this.locale.next(locale);
    }
  }

}
