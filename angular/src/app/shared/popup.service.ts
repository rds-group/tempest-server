import {Injectable} from '@angular/core';
import {Observable} from "rxjs/Observable";
import {Subject} from "rxjs/Subject";

@Injectable()
export class PopupService {

  open$: Subject<string> = new Subject();
  close$: Subject<boolean> = new Subject();

  constructor() {
  }

  open(message: string): Observable<boolean> {
    this.open$.next(message);
    return this.close$.take(1);
  }

  close(response: boolean) {
    this.close$.next(response);
  }

}
