import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {InfoBadgeComponent} from './info-badge.component';

describe('InfoBadgeComponent', () => {
  let component: InfoBadgeComponent;
  let fixture: ComponentFixture<InfoBadgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InfoBadgeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoBadgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
