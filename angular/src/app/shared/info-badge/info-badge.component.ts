import {Component, Input, OnInit} from '@angular/core';
import {User} from "../../user/user";

@Component({
  selector: 'app-info-badge',
  templateUrl: './info-badge.component.html',
  styleUrls: ['./info-badge.component.scss']
})
export class InfoBadgeComponent implements OnInit {

  @Input() time: string;
  @Input() user: User;

  constructor() {
  }

  ngOnInit() {
  }

}
