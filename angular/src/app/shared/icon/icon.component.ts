import {Component, HostBinding, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss']
})
export class IconComponent implements OnInit {

  @Input() name: string;
  @Input() size: string;
  @HostBinding('class') hostClass: string = '';

  constructor() {
  }

  ngOnInit() {
    switch (this.size) {
      case 'tiny':
        this.hostClass = 'size-tiny';
        break;
      case 'small':
        this.hostClass = 'size-small';
        break;
      case 'big':
        this.hostClass = 'size-big';
        break;
      case 'large':
        this.hostClass = 'size-large';
        break;
      default:
        this.hostClass = 'size-default';
    }
  }

}
