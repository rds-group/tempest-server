import {Component} from '@angular/core';
import {UserService} from "./user/user.service";
import {User} from "./user/user";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  private _user: User;

  constructor(private _userService: UserService) {
    this._userService.user$
      .subscribe(user => this._user = user);
  }
}
