import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from "@angular/router";
import {TopBarComponent} from './top-bar/top-bar.component';
import {IconSetComponent} from './icon-set/icon-set.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    TopBarComponent,
    IconSetComponent
  ],
  exports: [
    TopBarComponent,
    IconSetComponent
  ]
})
export class CoreModule {
}
