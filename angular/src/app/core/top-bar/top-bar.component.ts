import {Component, OnInit} from '@angular/core';
import {UserService} from "../../user/user.service";
import {Router} from "@angular/router";
import {User} from "../../user/user";

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit {

  private _user: User;

  constructor(private _router: Router, private _userService: UserService) {
    this._userService.user$
      .subscribe(user => this._user = user);
  }

  ngOnInit() {
  }

  onLogoClick() {
    this._router.navigate(['/dashboard']);
  }

  onLogoutClick() {
    this._userService.logout();
  }

}
