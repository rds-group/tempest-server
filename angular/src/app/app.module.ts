import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {RouterModule} from "@angular/router";
import {SharedModule} from "./shared/shared.module";
import {CoreModule} from "./core/core.module";
import {UserModule} from "./user/user.module";
import {DashboardModule} from "./dashboard/dashboard.module";
import {CardModule} from "./card/card.module";
import {AppComponent} from "./app.component";
import {UserResolver} from "./user/user-resolver.service";
import {routes} from "./routes";

import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/do";
import "rxjs/add/operator/toPromise";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/take";
import "rxjs/add/observable/throw"

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes),
    SharedModule,
    CoreModule,
    UserModule,
    DashboardModule,
    CardModule
  ],
  providers: [
    UserResolver
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
