import {Routes} from "@angular/router";
import {LoginComponent} from "./login/login.component";
import {UserListComponent} from "./user-list/user-list.component";
import {AuthGuard} from "./auth-guard.service";
import {UserEditComponent} from "./user-edit/user-edit.component";
import {UserResolver} from "./user-resolver.service";

export const routes: Routes = [
  {
    path: 'user',
    children: [
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'list',
        component: UserListComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'edit/:id',
        component: UserEditComponent,
        canActivate: [AuthGuard],
        resolve: {
          user: UserResolver
        }
      }
    ]
  }
];
