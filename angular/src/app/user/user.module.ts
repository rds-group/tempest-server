import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './login/login.component';
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {UserService} from "./user.service";
import {routes} from "./routes";
import {UserListComponent} from './user-list/user-list.component';
import {AvatarComponent} from './avatar/avatar.component';
import {SharedModule} from "../shared/shared.module";
import {CoreModule} from "../core/core.module";
import {AuthGuard} from "./auth-guard.service";
import {UserEditComponent} from "./user-edit/user-edit.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    SharedModule,
    CoreModule
  ],
  providers: [
    UserService,
    AuthGuard
  ],
  declarations: [
    LoginComponent,
    AvatarComponent,
    UserListComponent,
    UserEditComponent
  ]
})
export class UserModule {
}
