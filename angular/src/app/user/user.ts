import {Serializable} from "../shared/serializable";

export class User implements Serializable<User> {
  _id: string;
  username: string;
  password: string;
  displayName: string;
  type: User.Type;
  symbol: string;
  color: string;
  email: string;
  created: string;
  modified: string;
  createdBy: string;
  modifiedBy: string;

  serialize() {
    let serializedObject: any = this;
    switch (parseInt(serializedObject.type)) {
      case User.Type.Admin:
        serializedObject.type = 'admin';
        break;
      case User.Type.Teacher:
        serializedObject.type = 'teacher';
        break;
      case User.Type.Student:
        serializedObject.type = 'student';
        break;
    }
    return serializedObject;
  }

  deserialize(input) {
    this._id = input._id;
    this.username = input.username;
    this.password = input.password;
    this.displayName = input.displayName;
    switch (input.type) {
      case 'admin':
        this.type = User.Type.Admin;
        break;
      case 'teacher':
        this.type = User.Type.Teacher;
        break;
      case 'student':
        this.type = User.Type.Student;
        break;
    }
    this.symbol = input.symbol;
    this.color = input.color;
    this.email = input.email;
    this.created = input.created;
    this.modified = input.modified;
    this.createdBy = input.createdBy;
    this.modifiedBy = input.modifiedBy;

    return this;
  }
}

export namespace User {
  export enum Type {
    Admin,
    Teacher,
    Student
  }
}
