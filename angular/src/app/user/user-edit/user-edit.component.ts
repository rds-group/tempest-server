import {Component, OnInit} from '@angular/core';
import {NgForm} from "@angular/forms";
import {User} from "../user";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../user.service";

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {

  private _user: User;
  private UserType = User.Type;
  private _newPassword: string = '';
  private _confirmNewPassword: string = '';

  constructor(private _userService: UserService, private _route: ActivatedRoute, private _router: Router) {
  }

  ngOnInit() {
    this._route.data
      .subscribe((data: { user: User }) => {
        this._user = data.user;
      });
  }

  onSubmit(f: NgForm) {
    if (f.valid && this.validPassword()) {
      if (this.filledPassword()) this._user.password = this._newPassword;
      this._userService.update(this._user)
        .subscribe(() => {
          this.backToList();
        });
    }
  }

  filledPassword(): boolean {
    return this._newPassword.length > 0;
  }

  validPassword(): boolean {
    let valid: boolean = true;
    if (this.filledPassword()) valid = this._newPassword == this._confirmNewPassword;
    return valid;
  }

  onBackClick() {
    this.backToList();
  }

  onCancelClick() {
    this.backToList();
  }

  backToList() {
    this._router.navigate(['/user/list']);
  }

}
