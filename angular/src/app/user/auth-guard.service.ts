import {Injectable} from '@angular/core';
import {CanActivate, Router} from "@angular/router";
import {UserService} from "./user.service";
import {Observable} from "rxjs/Observable";

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private _router: Router, private _userService: UserService) {
  }

  canActivate(): Observable<boolean> {
    return this._userService.user$
      .filter(user => user !== undefined)
      .map(user => !!user)
      .do(user => {
        if (!user) this._router.navigate(['/user/login']);
      });
  }

}
