import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from "@angular/router";
import {User} from "./user";
import {UserService} from "./user.service";
import {Observable} from "rxjs/Observable";

@Injectable()
export class UserResolver implements Resolve<User> {

  constructor(private _userService: UserService, private _router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User> {
    let id = route.params['id'];
    return this._userService.get(id)
      .do(user => {
        if (!user) {
          this._router.navigate(['/user/list']);
        }
      });
  }

}
