import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {UserService} from "../user.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  private username: string;
  private password: string;
  private message: string;

  constructor(private _userService: UserService) {
  }

  ngOnInit() {

  }

  onKeyPress() {
    this.message = null;
  }

  onSubmit(f: NgForm) {
    if (f.valid) {
      this._userService
        .login(this.username, this.password)
        .subscribe({error: err => this.onError(err)});
    }
  }

  onError(err: string) {
    this.message = err;
  }

}
