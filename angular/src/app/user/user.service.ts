import {Injectable} from '@angular/core';
import {ApiService} from "../shared/api.service";
import {Router} from "@angular/router";
import {RequestMethod} from "@angular/http";
import {User} from "./user";
import {Observable} from "rxjs/Observable";
import {BehaviorSubject} from "rxjs/BehaviorSubject";

@Injectable()
export class UserService {

  public user$: BehaviorSubject<User> = new BehaviorSubject(undefined);

  constructor(private _router: Router, private _apiService: ApiService) {
    this._apiService.makeRequest('user/me', RequestMethod.Get)
      .map(user => {
        if (user) return user;
        return null;
      })
      .subscribe(user => this.user$.next(user));
  }

  public get(id: string): Observable<User> {
    return this._apiService.makeRequest('users/' + id, RequestMethod.Get)
      .map(object => new User().deserialize(object));
  }

  public create(user: User): Observable<User> {
    return this._apiService.makeRequest('users', RequestMethod.Post, null, user.serialize())
      .map(object => new User().deserialize(object));
  }

  public update(user: User): Observable<User> {
    return this._apiService.makeRequest('users/' + user._id, RequestMethod.Put, null, user.serialize())
      .map(object => new User().deserialize(object));
  }

  public delete(user: User): Observable<User> {
    return this._apiService.makeRequest('users/' + user._id, RequestMethod.Delete, null, user.serialize())
      .map(object => new User().deserialize(object));
  }

  public list(): Observable<User[]> {
    return this._apiService.makeRequest('users', RequestMethod.Get)
      .map(objects => objects.map(object => new User().deserialize(object)));
  }

  public login(username: string, password: string): Observable<User> {
    return this._apiService.makeRequest('user/login', RequestMethod.Post, null, {
      username: username,
      password: password
    }).do(object => this.onLogin(new User().deserialize(object)))
      .catch(() => {
        return Observable.throw('Login or password is incorrect');
      });
  }

  public logout(): void {
    this._apiService.makeRequest('user/logout', RequestMethod.Get)
      .subscribe(() => this.onLogout());
  }

  private onLogin(user: User): void {
    this.user$.next(user);
    this._router.navigate(['/dashboard']);
  }

  private onLogout(): void {
    this.user$.next(null);
    this._router.navigate(['/user/login']);
  }

}
