import {Component, OnInit} from '@angular/core';
import {UserService} from "../user.service";
import {User} from "../user";
import {Router} from "@angular/router";
import {NgForm} from "@angular/forms";
import {PopupService} from "../../shared/popup.service";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  private _users: User[];
  private _newUser: User;
  private UserType = User.Type;

  constructor(private _userService: UserService, private _router: Router, private _popupService: PopupService) {
    this.refresh();
  }

  ngOnInit() {
    this._newUser = new User();
  }

  protected refresh(): void {
    this._userService.list()
      .subscribe(users => this._users = users);
  }

  onSubmit(f: NgForm) {
    if (f.valid) {
      this._userService.create(this._newUser)
        .subscribe(() => {
          this._newUser = new User();
          this.refresh();
        });
    }
  }

  onEditClick(user: User): void {
    this._router.navigate(['/user/edit', user._id]);
  }

  onRemoveClick(user: User): void {
    let msg: string = 'Are you sure you want to remove user &quot;' + user.displayName + '&quot; ?';

    this._popupService.open(msg)
      .subscribe(confirm => {
        if (confirm) this.removeUser(user);
      })
  }

  removeUser(user: User): void {
    this._userService.delete(user)
      .subscribe(() => {
        this.refresh();
      });
  }

}
