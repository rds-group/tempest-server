import RdsApplication from "../lib/application";
import UserModule from "../modules/user/user";
import CardModule from "../modules/card/card";
import LessonManager from "../socket_modules/lesson-manager/lesson-manager";
import FrontendModule from "../modules/frontend/frontend";

const PORT: string = '3002';

let app = new RdsApplication('tempest', PORT, [UserModule, CardModule, FrontendModule], [LessonManager]);
app.run();