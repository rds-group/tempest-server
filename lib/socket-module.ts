import * as createSocketServer from 'socket.io';
import RdsApplication from './application';
import {Server} from 'net';
import RdsModule from "./module";

export interface IRdsSocketModule {
  new (app: RdsApplication): RdsSocketModule;
}

export default class RdsSocketModule extends RdsModule {
  name: string;
  app: RdsApplication;
  socketServer: SocketIO.Server;

  constructor(app: RdsApplication) {
    super(app);
    this.name = 'socket-module';
    this.app = app;
  }

  public setup(httpServer: Server): void {
    this.setupSocket(httpServer);
    this.app.log('info', 'Socket Module ' + this.name + ' setup complete');
  }

  protected setupSocket(httpServer: Server): void {
    this.socketServer = createSocketServer(httpServer);
  }
}