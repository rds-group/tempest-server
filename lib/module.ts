import RdsApplication from "./application";

export interface IRdsModule {
  new (app: RdsApplication): RdsModule;
}

export default class RdsModule {
  name: string;
  app: RdsApplication;

  constructor(app: RdsApplication) {
    this.name = 'module';
    this.app = app;
  }
}