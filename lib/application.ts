import * as _ from "lodash";
import * as createExpress from "express";
import * as createLogger from "morgan";
import * as bodyParser from "body-parser";
import * as createCookieParser from "cookie-parser";
import * as createSession from "express-session";
import * as createHelmet from "helmet";
import * as createModRewrite from "connect-modrewrite";
import * as createCors from "cors";
import * as mongoose from "mongoose";
import * as Q from "q";
import CoreModule from "./core/core";
import RdsLogger from "./logger";
import RdsServer from "./server";
import RdsSocketModule, {IRdsSocketModule} from "./socket-module";
import RdsStandardModule, {IRdsStandardModule} from "./standard-module";

export default class RdsApplication {
  name: string;
  port: string;
  express: any;
  logger: any;
  server: RdsServer;
  standardModuleClasses: IRdsStandardModule[];
  standardModules: Map<string, RdsStandardModule>;
  socketModuleClasses: IRdsSocketModule[];
  socketModules: Map<string, RdsSocketModule>;

  constructor(name: string, port: string, standardModuleClasses: IRdsStandardModule[], socketModuleClasses: IRdsSocketModule[]) {
    this.name = name;
    this.port = port;
    this.logger = new RdsLogger();

    this.standardModuleClasses = [CoreModule];
    _.forEach(standardModuleClasses, standardModuleClass => this.standardModuleClasses.push(standardModuleClass));
    this.standardModules = new Map<string, RdsStandardModule>();

    this.socketModuleClasses = [];
    _.forEach(socketModuleClasses, socketModuleClass => this.socketModuleClasses.push(socketModuleClass));
    this.socketModules = new Map<string, RdsSocketModule>();

    this.setup();
  }

  public log(level: string, msg: string, data?: any): void {
    this.logger.log(level, msg, data);
  }

  public run(): void {
    this.log('info', 'Application ' + this.name + ' run');
    this.server = new RdsServer(this);
    this.server.run();
    this.setupSocketModules();
  }

  public getStandardModule(name: string): RdsStandardModule {
    return this.standardModules.get(name);
  }

  protected setup(): void {
    this.log('info', 'Application ' + this.name + ' setup');

    this.express = createExpress();
    this.express.use(createLogger('dev'));
    this.express.use(bodyParser.json({limit: '50mb'}));
    this.express.use(bodyParser.urlencoded({limit: '50mb', extended: false}));
    this.express.use(createCookieParser());
    this.express.use(createHelmet());
    this.express.use(createSession({
      secret: 'wSap7942sKFASiKxJ8Lw2zsMeTxZJL5Bb7Re9JuJfeRmPmGQgFKqNcHqZBEFvmv0SLPjnyZ0efMvftqe',
      resave: true,
      saveUninitialized: true
    }));
    this.express.use(createModRewrite([
      '!^/api/.*|\\.html|\\.js|\\.css|\\.swf|\\.jp(e?)g|\\.JP(E?)G|\\.PNG|\\.png|\\.ico|\\.gif|\\.svg|\\.eot|\\.ttf|\\.woff|\\.txt|\\.pdf$ / [L]',
    ]));

    this.setupCrossDomain();
    this.setupDatabase();
  }

  protected setupDatabase(): void {
    (<any>mongoose).Promise = Q.Promise;
    mongoose
      .connect('mongodb://localhost/' + this.name)
      .then(() => {
        this.log('info', 'Mongoose connected');
        this.setupStandardModules();
      })
      .catch((err) => {
        this.log('error', 'Mongoose connection failed.', err);
      });
  }

  protected setupCrossDomain(): void {
    this.express.use(createCors({
      credentials: true,
      origin: true
    }));
    this.log('info', 'CrossDomain setup complete');
  }

  protected setupStandardModules(): void {
    this.log('info', 'Running up modules');
    _.forEach(this.standardModuleClasses, standardModuleClass => {
      let standardModule: RdsStandardModule = new standardModuleClass(this);
      standardModule.setup();
      this.standardModules.set(standardModule.name, standardModule);
    });
  }

  protected setupSocketModules() {
    this.log('info', 'Running up socket modules');
    if (!this.server.httpServer) throw new Error('Cannot run socket server, HTTP server now running');
    _.forEach(this.socketModuleClasses, socketModuleClass => {
      let socketModule: RdsSocketModule = new socketModuleClass(this);
      socketModule.setup(this.server.httpServer);
      this.socketModules.set(socketModule.name, socketModule);
    });
  }
}