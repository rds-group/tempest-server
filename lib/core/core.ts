import * as expressLayouts from "express-ejs-layouts";
import RdsApplication from "../application";
import * as path from "path";
import CoreRouter from "./server/router";
import RdsStandardModule from "../standard-module";

export default class CoreModule extends RdsStandardModule {
  constructor(app: RdsApplication) {
    super(app);
    this.name = 'core';
    this.moduleDir = __dirname;
    this.routerClasses.push(CoreRouter);
  }

  setupLayouts() {
    this.app.express.set('views', path.join(this.serverDir, 'views'));
    this.app.express.set('view engine', 'ejs');
    this.app.express.use(expressLayouts);
  }
}