import * as path from "path";
import * as winston from "winston";
import * as DailyRotateFile from "winston-daily-rotate-file";

export default class RdsLogger {
  winston: any;

  constructor() {
    this.winston = new (winston.Logger)({
      transports: [
        new (winston.transports.Console)(),
        new (DailyRotateFile)({
          filename: './log',
          dirname: path.join(__dirname, '..', 'logs'),
          datePattern: 'yyyy-MM-dd.',
          prepend: true,
        })
      ]
    });
  }

  public log(level: string, msg: string, data?: any): void {
    this.winston.log(level, msg, data);
  }
}