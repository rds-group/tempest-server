import * as express from "express";
import {Router} from "express";
import RdsApplication from "./application";
import RdsStandardModule from "./standard-module";

export interface IRdsRouter {
  new (app: RdsApplication, module: RdsStandardModule): RdsRouter;
}

export default class RdsRouter{
  name: string;
  app: RdsApplication;
  module: RdsStandardModule;
  router: Router;

  constructor(app: RdsApplication, module: RdsStandardModule) {
    this.name = 'router';
    this.app = app;
    this.module = module;
  }

  setup() {
    this.router = express.Router();
  }
}