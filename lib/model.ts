import * as _ from "lodash";
import * as mongoose from 'mongoose';
import RdsApplication from "./application";

export interface IRdsModel {
  new (app: RdsApplication): RdsModel;
}

export default class RdsModel {
  name: string;
  app: RdsApplication
  schemaDefinition: object;
  mongooseSchema: any;
  schema: any;
  $: any;
  basicSchemaDefinition: object;

  constructor(app: RdsApplication) {
    this.app = app;
    this.name = 'model';
    this.mongooseSchema = mongoose.Schema;
    this.basicSchemaDefinition = {
      created: {
        type: Date,
        default: Date.now
      },
      createdBy: {
        type: this.mongooseSchema.ObjectId,
        ref: 'User'
      },
      modified: {
        type: Date,
        default: Date.now
      },
      modifiedBy: {
        type: this.mongooseSchema.ObjectId,
        ref: 'User'
      }
    };
  }

  public setup(): void {
    this.setupSchema();
    this.setupPlugins();
    this.setupSchemaStatics();
    this.setupSchemaMethods();
    this.registerSchema();
  }

  public buildErrors(errors: any[]) {
    _.forEach(errors, (error, i) => {
      errors[i] = _.pick(error, ['message']);
    });
    return errors;
  }

  public extendDocument(document: any, newData: any) {
    _.forEach(this.$.schema.obj, (schemaInfo, pathname) => {
      if (newData[pathname]) {
        if (this.$.schema.pathType(pathname) === 'virtual') {
          document.set(pathname + '.all', newData[pathname]);
        } else {
          document[pathname] = newData[pathname];
        }
        document.markModified(pathname);
      }
    });
  }

  protected setupPlugins(): void {
  }

  protected setupSchema(): void {
    if (this.name && this.schemaDefinition) {
      _.merge(this.schemaDefinition, this.basicSchemaDefinition);
      this.schema = new this.mongooseSchema(this.schemaDefinition);
    } else {
      throw new Error('Model name or definition not set.');
    }
  }

  protected setupSchemaStatics(): void {
  }

  protected setupSchemaMethods(): void {
  }

  protected registerSchema(): void {
    this.app.log('info', 'Registering Model Schema ' + this.name);
    mongoose.model(this.name, this.schema);
    this.$ = mongoose.model(this.name);
  }
}