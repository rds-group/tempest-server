import createDebug from "debug";
import * as http from "http";
import RdsApplication from "./application";

export default class RdsServer {
  app: RdsApplication;
  debug: any;
  port: number;
  httpServer: any;

  constructor(app: RdsApplication) {
    this.app = app;
    this.debug = createDebug(this.app.name + ':server');
    this.port = this.normalizePort(process.env.PORT || this.app.port);
  }

  run() {
    this.httpServer = http.createServer(this.app.express);
    this.httpServer.listen(this.port);
    this.httpServer.on('error', () => this.onError);
    this.httpServer.on('listening', () => this.onListening);
    this.app.log('info', 'Server running on port ' + this.port);
  }

  onError(error) {
    if (error.syscall !== 'listen') {
      throw error;
    }

    let bind = 'Port ' + this.port;

    switch (error.code) {
      case 'EACCES':
        console.error(bind + ' requires elevated privileges');
        process.exit(1);
        break;
      case 'EADDRINUSE':
        console.error(bind + ' is already in use');
        process.exit(1);
        break;
      default:
        throw error;
    }
  }

  onListening() {
    let address = this.httpServer.address();
    let bind = typeof address === 'string' ? 'pipe ' + address : 'port ' + address.port;
    this.debug('Listening on ' + bind);
  }

  normalizePort(value: string) {
    let port: number = parseInt(value, 10);
    if (isNaN(port)) throw new Error('port number is incorrect');
    if (port >= 0) return port;
    throw new Error('port number is incorrect');
  }
}