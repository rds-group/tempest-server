import * as _ from "lodash";
import * as Q from "q";
import RdsApplication from "./application";
import RdsStandardModule from "./standard-module";

export default class RdsController {
  app: RdsApplication;
  module: RdsStandardModule;
  modelName: string;
  model: any;

  constructor(app: RdsApplication, module: RdsStandardModule, modelName: string) {
    this.app = app;
    this.module = module;
    this.modelName = modelName;
    this.model = module.getModel(modelName);
  }

  document(req, res, next: Function, id: string) {
    this.model.$
      .findById(id)
      .populate('createdBy', 'displayName username symbol')
      .populate('modifiedBy', 'displayName username symbol')
      .exec()
      .then(document => {
        if (!document) {
          return res.status(404).json({message: '404 NOT FOUND'});
        }

        req.document = document;
        next();
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({message: 'FATAL ERROR ABORT THE MISSION'});
      });
  }

  create(req, res) {
    let document = new this.model.$(req.body);
    if (req.user) document.createdBy = req.user;
    if (req.user) document.modifiedBy = req.user;

    document
      .save()
      .then(() => {
        res.json(document);
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({message: 'FATAL ERROR ABORT THE MISSION'});
      });
  }

  update(req, res) {
    let document = req.document;
    this.model.extendDocument(document, req.body);
    document.modified = Date.now();
    if (req.user) document.modifiedBy = req.user;

    document
      .save()
      .then(document => {
        res.json(document);
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({message: 'FATAL ERROR ABORT THE MISSION'});
      });
  }

  updateMany(req, res) {
    let objects: any = req.body;
    let saveOperations: Q.Promise = [];

    _.forEach(objects, object => {
      saveOperations.push(this.model.$
        .findById(object._id)
        .exec()
        .then(document => {
          if (!document) return Q.reject('404 NOT FOUND');
          _.extend(document, object);
          return document.save();
        })
        .catch(err => {
          console.log(err);
          res.status(500).json({message: 'FATAL ERROR ABORT THE MISSION'});
        }));
    });

    Q.all(saveOperations)
      .then(() => res.json(true))
      .catch(err => {
        console.log(err);
        res.status(500).json({message: 'FATAL ERROR ABORT THE MISSION'});
      });
  }

  delete(req, res) {
    let document = req.document;

    document
      .remove()
      .then(() => {
        res.json(document);
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({message: 'FATAL ERROR ABORT THE MISSION'});
      });
  }

  show(req, res) {
    res.json(req.document);
  }

  all(req, res) {
    this.model.$
      .find({})
      .populate('createdBy', 'displayName username symbol')
      .populate('modifiedBy', 'displayName username symbol')
      .exec()
      .then(documents => {
        res.json(documents);
      })
      .catch(err => {
        res.status(500).json({message: 'FATAL ERROR ABORT THE MISSION', errors: err});
      });
  }
}