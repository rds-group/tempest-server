import * as _ from "lodash";
import * as path from "path";
import * as express from "express";
import RdsApplication from "./application";
import {IRdsModel, default as RdsModel} from "./model";
import {IRdsRouter, default as RdsRouter} from "./router";
import RdsModule from "./module";

export interface IRdsStandardModule {
  new (app: RdsApplication): RdsStandardModule;
}

export default class RdsStandardModule extends RdsModule {
  moduleDir: string;
  publicDir: string;
  serverDir: string;
  assetsDir: string;
  modelClasses: IRdsModel[];
  models: Map<string, RdsModel>;
  routerClasses: IRdsRouter[];
  routers: Map<string, RdsRouter>;

  constructor(app: RdsApplication) {
    super(app);
    this.name = 'module';
    this.app = app;
    this.modelClasses = [];
    this.models = new Map<string, RdsModel>();
    this.routerClasses = [];
    this.routers = new Map<string, RdsRouter>();
  }

  public setup(): void {
    this.setupDirs();
    this.setupLayouts();
    this.setupStatic();
    this.setupModels();
    this.setupAuth();
    this.setupUploads();
    this.setupRoutes();
    this.app.log('info', 'Module ' + this.name + ' setup complete');
  }

  public getModel(name: string): any {
    return this.models.get(name);
  }

  protected setupDirs(): void {
    if (!this.moduleDir) throw new Error('ModuleDir not set');

    this.publicDir = path.join(this.moduleDir, 'public');
    this.serverDir = path.join(this.moduleDir, 'server');
    this.assetsDir = path.join(this.publicDir, 'assets');
  }

  protected setupLayouts(): void {

  }

  protected setupStatic(): void {
    this.app.express.use('/' + this.name, express.static(this.publicDir));
  }

  protected setupModels(): void {
    _.forEach(this.modelClasses, modelClass => {
      let model: RdsModel = new modelClass(this.app);
      model.setup();
      this.models.set(model.name, model);
    })
  }

  protected setupAuth(): void {

  }

  protected setupUploads(): void {

  }

  protected setupRoutes(): void {
    _.forEach(this.routerClasses, routerClass => {
      let router: RdsRouter = new routerClass(this.app, this);
      router.setup();
      this.app.express.use('/', router.router);
      this.routers.set(router.name, router);
    })
  }
}