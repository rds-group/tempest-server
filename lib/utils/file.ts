import * as Q from "q";
import {ReadStream, WriteStream} from "fs";
import * as http from "http";
import * as https from "https";
import * as stream from "stream";
import * as fs from "fs";
import * as randomString from "randomstring";
import * as slug from "slug";
import * as path from "path";
import {Url} from "url";
import * as URL from "url";

export default class FileUtils {
  static copy(source: string, target: string): Q.Promise<any> {
    let deferred = Q.defer();
    let readStream: ReadStream = fs.createReadStream(source);
    readStream.on('error', deferred.reject);
    let writeStream: WriteStream = fs.createWriteStream(target);
    writeStream.on('error', deferred.reject);
    writeStream.on('finish', deferred.resolve);
    readStream.pipe(writeStream);
    return deferred.promise;
  }

  static remove(path: string): void {
    fs.unlink(path);
  }

  static createDirIfNotExists(path: string): void {
    if (!fs.existsSync(path)) fs.mkdirSync(path);
  }

  static getNiceFileName(originalFileName: string): string {
    let randomPart: string = randomString.generate({
      length: 4,
      capitalization: 'lowercase'
    });
    let parsed: any = path.parse(originalFileName);
    let parts: string[] = [
      slug(parsed.name, {lowercase: true}).substr(0, 12),
      '-',
      randomPart,
      parsed.ext
    ];
    return parts.join('');
  }

  static scrap(url: string, targetFilePath: string): Q.Promise<any> {
    let deferred = Q.defer();
    let fileUrl: Url = URL.parse(url);
    let requestModule: any;
    switch (fileUrl.protocol) {
      case 'http:':
        requestModule = http;
        break;
      case 'https:':
        requestModule = https;
    }
    if (requestModule) {
      requestModule.get(url, res => {
        let data = new stream.Transform();
        res.on('data', chunk => data.push(chunk));
        res.on('end', () => {
          try {
            fs.writeFileSync(targetFilePath, data.read());
            deferred.resolve();
          } catch (e) {
            deferred.reject(e);
          }
        });
      });
    } else {
      deferred.reject(new Error('Unsupported protocol'));
    }
    return deferred.promise;
  }
}