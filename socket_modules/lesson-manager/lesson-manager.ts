import * as mongoose from 'mongoose';
import * as _ from 'lodash';
import {Server} from 'net';

import RdsSocketModule from './../../lib/socket-module';
import {Lesson, Card, Teacher, Pin} from './models/lesson';
import RdsApplication from "../../lib/application";
import UserModel from "../../modules/user/server/models/user";

export default class LessonManager extends RdsSocketModule {
  registeredUsers: Map<string, any>;
  lesson: Lesson;

  constructor(app: RdsApplication) {
    super(app);
    this.name = 'lesson-manager';
    this.app = app;
  }

  setup(httpServer: Server) {
    super.setup(httpServer);

    this.registeredUsers = new Map<string, any>();
    this.socketServer.on('connection', this.onConnection.bind(this));
  }

  onConnection(socket: any) {
    this.app.log('info', 'Socket connection ' + socket.id + ' ' + socket.handshake.headers['user-agent']);

    socket.on('latency.ping', (data, cb) => this.onLatencyPing(data, cb));
    socket.on('latency.report', data => this.onLatencyReport(socket, data));
    socket.on('disconnect', () => this.onDisconnect(socket));
    socket.on('user.register', (data, cb) => this.onUserRegister(socket, data, cb));
  }

  onLatencyPing(data: any, cb: Function) {
    cb(data);
  }

  onLatencyReport(socket: any, data: any) {
    this.app.log('info', 'Socket latency.report ' + socket.id, data);
  }

  onDisconnect(socket: any) {
    this.app.log('info', 'Socket disconnect ' + socket.id + ' ' + socket.handshake.headers['user-agent']);
    if (socket.user) {
      this.registeredUsers.delete(socket.user._id.toString());
      if (this.lesson) {
        if (socket.user.type == 'teacher') {
          this.lesson = null;
          this.socketServer.to('registered-users').emit('lesson.end');
        }

        if (socket.user.type == 'student') {
          this.lesson.removeStudent(socket.user);
          this.socketServer.to('registered-users').emit('lesson.update', this.lesson);
        }
      }
      let registeredUsers = Array.from(this.registeredUsers.values());
      this.socketServer.to('registered-users').emit('registered-users.update', registeredUsers);
    }
  }

  onUserRegister(socket: any, data?: any, cb?: Function) {
    this.app.log('info', 'Socket user.register ' + socket.id, data);
    let userModel: UserModel = this.app.getStandardModule('user').getModel('User');
    if (!data || !data.username || !data.password) {
      (cb || _.noop)(false);
      return;
    }
    userModel.authBasic()(data.username, data.password, (err, user) => {
      if (err || !user) {
        this.app.log('info', 'Socket user.register auth failure ' + socket.id, {err: err, user: user});
        return (cb || _.noop)(false);
      }
      this.app.log('info', 'Socket user.register auth success ' + socket.id, user.toJSON());
      let userId = user._id.toString();
      if (!this.registeredUsers.has(userId)) {
        user = user.toJSON();
        user.isActive = true;
        this.registeredUsers.set(user._id.toString(), user);
        socket.join('registered-users');
        socket.on('user.unregister', data => this.onUserUnregister(socket, data));
        socket.on('user.status', data => this.onUserStatus(socket, data));
        socket.on('lesson.start', (data, cb) => this.onLessonStart(socket, data, cb));
        socket.on('lesson.end', (data, cb) => this.onLessonEnd(socket, data, cb));
        socket.on('lesson.clear', data => this.onLessonClear(socket, data));
        socket.on('lesson.lock', data => this.onLessonLock(socket, data));
        socket.on('lesson.unlock', data => this.onLessonUnlock(socket, data));
        socket.on('lesson.teacher.pin.add', data => this.onLessonTeacherPinAdd(socket, data));
        socket.on('lesson.teacher.pin.remove', data => this.onLessonTeacherPinRemove(socket, data));
        socket.on('lesson.teacher.clear', data => this.onLessonTeacherClear(socket, data));
        socket.on('lesson.student.pin.add', data => this.onLessonStudentPinAdd(socket, data));
        socket.on('lesson.student.pin.remove', data => this.onLessonStudentPinRemove(socket, data));
        socket.on('lesson.student.clear', data => this.onLessonStudentClear(socket, data));

        let registeredUsers = Array.from(this.registeredUsers.values());
        this.socketServer.to('registered-users').emit('registered-users.update', registeredUsers);

        socket.user = user;
        if (user.type == 'student') {
          if (this.lesson) {
            this.lesson.addStudent(user);
            this.socketServer.to('registered-users').emit('lesson.update', this.lesson);
          }
        }
        (cb || _.noop)(user);
      } else {
        (cb || _.noop)(false);
      }
    });
  }

  onUserUnregister(socket: any, data?: any) {
    this.app.log('info', 'Socket user.unregister ' + socket.id, data);
    if (socket.user) {
      let user = socket.user;
      socket.leave('registered-users');
      socket.removeListener('user.unregister', data => this.onUserUnregister(socket, data));
      socket.removeListener('user.status', data => this.onUserStatus(socket, data));
      socket.removeListener('lesson.start', (data, cb) => this.onLessonStart(socket, data, cb));
      socket.removeListener('lesson.end', (data, cb) => this.onLessonEnd(socket, data, cb));
      socket.removeListener('lesson.clear', data => this.onLessonClear(socket, data));
      socket.removeListener('lesson.lock', data => this.onLessonLock(socket, data));
      socket.removeListener('lesson.unlock', data => this.onLessonUnlock(socket, data));
      socket.removeListener('lesson.teacher.pin.add', data => this.onLessonTeacherPinAdd(socket, data));
      socket.removeListener('lesson.teacher.pin.remove', data => this.onLessonTeacherPinRemove(socket, data));
      socket.removeListener('lesson.teacher.clear', data => this.onLessonTeacherClear(socket, data));
      socket.removeListener('lesson.student.pin.add', data => this.onLessonStudentPinAdd(socket, data));
      socket.removeListener('lesson.student.pin.remove', data => this.onLessonStudentPinRemove(socket, data));
      socket.removeListener('lesson.student.clear', data => this.onLessonStudentClear(socket, data));

      if (this.lesson) {
        if (user.type == 'teacher') {
          this.lesson = null;
          this.socketServer.to('registered-users').emit('lesson.end')
        }
        if (user.type == 'student') {
          this.lesson.removeStudent(user);
          this.socketServer.to('registered-users').emit('lesson.update', this.lesson);
        }
      }
      this.registeredUsers.delete(user._id.toString());
      let registeredUsers = Array.from(this.registeredUsers.values());
      this.socketServer.to('registered-users').emit('registered-users.update', registeredUsers);
    }
  }

  onUserStatus(socket: any, data?: any) {
    this.app.log('info', 'Socket user.status ' + socket.id, data);
    if (socket.user) {
      socket.user.isActive = data;
      let registeredUsers = Array.from(this.registeredUsers.values());
      this.socketServer.to('registered-users').emit('registered-users.update', registeredUsers);
    }
  }

  onLessonStart(socket: any, data?: any, cb?: Function) {
    this.app.log('info', 'Socket lesson.start ' + socket.id, data);
    if (socket.user.type == 'teacher') {
      let CardModel = mongoose.model('Card');
      CardModel.findOne({_id: data.cardId}).exec()
        .then(cardDocument => {
          let card = new Card(cardDocument);
          let teacher = new Teacher(socket.user);
          this.lesson = new Lesson(card, teacher);
          this.registeredUsers.forEach((user) => {
            if (user.type == 'student') {
              this.lesson.addStudent(user);
            }
          });
          this.socketServer.to('registered-users').emit('lesson.update', this.lesson);
          (cb || _.noop)(this.lesson);
        })
        .catch(() => {
          (cb || _.noop)(this.lesson);
        });
    } else {
      (cb || _.noop)(false);
    }
  }

  onLessonEnd(socket: any, data?: any, cb?: Function) {
    this.app.log('info', 'Socket lesson.end ' + socket.id, data);
    if (this.lesson) {
      this.lesson = null;
      this.socketServer.to('registered-users').emit('lesson.end');

      //this.registeredUsers.clear();
      //let registeredUsers = Array.from(this.registeredUsers.values());
      //this.socketServer.to('registered-users').emit('registered-users.update', registeredUsers);
      (cb || _.noop)(true);
    } else {
      (cb || _.noop)(false);
    }
  }

  onLessonClear(socket: any, data?: any) {
    this.app.log('info', 'Socket lesson.clear ' + socket.id, data);
    if (socket.user.type == 'teacher' && this.lesson) {
      this.lesson.clear();
      this.socketServer.to('registered-users').emit('lesson.update', this.lesson);
    }
  }

  onLessonLock(socket: any, data?: any) {
    this.app.log('info', 'Socket lesson.lock ' + socket.id, data);
    if (socket.user.type == 'teacher' && this.lesson) {
      this.lesson.lock();
      this.socketServer.to('registered-users').emit('lesson.update', this.lesson);
    }
  }

  onLessonUnlock(socket: any, data?: any) {
    this.app.log('info', 'Socket lesson.unlock ' + socket.id, data);
    if (socket.user.type == 'teacher' && this.lesson) {
      this.lesson.unlock();
      this.socketServer.to('registered-users').emit('lesson.update', this.lesson);
    }
  }

  onLessonTeacherPinAdd(socket: any, data?: any) {
    this.app.log('info', 'Socket lesson.teacher.pin.add ' + socket.id, data);
    if (socket.user.type == 'teacher' && this.lesson) {
      let pin = new Pin(data.id, data.x, data.y);
      this.lesson.addTeacherPin(pin);
      this.socketServer.to('registered-users').emit('lesson.update', this.lesson);
    }
  }

  onLessonTeacherPinRemove(socket: any, data?: any) {
    this.app.log('info', 'Socket lesson.teacher.pin.remove ' + socket.id, data);
    if (socket.user.type == 'teacher' && this.lesson) {
      this.lesson.removeTeacherPin(data.id);
      this.socketServer.to('registered-users').emit('lesson.update', this.lesson);
    }
  }

  onLessonTeacherClear(socket: any, data?: any) {
    this.app.log('info', 'Socket lesson.teacher.clear ' + socket.id, data);
    if (socket.user.type == 'teacher' && this.lesson) {
      this.lesson.clearTeacher();
      this.socketServer.to('registered-users').emit('lesson.update', this.lesson);
    }
  }

  onLessonStudentPinAdd(socket: any, data?: any) {
    this.app.log('info', 'Socket lesson.student.pin.add ' + socket.id, data);
    if (this.lesson) {
      let pin = new Pin(data.id, data.x, data.y);
      this.lesson.addStudentPin(socket.user, pin);
      this.socketServer.to('registered-users').emit('lesson.update', this.lesson);
    }
  }

  onLessonStudentPinRemove(socket: any, data?: any) {
    this.app.log('info', 'Socket lesson.student.pin.remove ' + socket.id, data);
    if (this.lesson) {
      this.lesson.removeStudentPin(socket.user, data.id);
      this.socketServer.to('registered-users').emit('lesson.update', this.lesson);
    }
  }

  onLessonStudentClear(socket: any, data?: any) {
    this.app.log('info', 'Socket lesson.student.clear ' + socket.id, data);
    if (this.lesson) {
      this.registeredUsers.forEach((user) => {
        if (user._id == data.userId) {
          this.lesson.clearStudent(user);
        }
      });
      this.socketServer.to('registered-users').emit('lesson.update', this.lesson);
    }
  }
}