import * as _ from 'lodash';

export class Card {
  public cardId: string;

  constructor(cardDocument: any) {
    this.cardId = cardDocument._id;
  }
}

export class Pin {
  constructor(public id: string, public x: number, public y: number) {
  }
}

class User {
  public userId: string;
  public pins: Pin[];

  constructor(userDocument: any) {
    this.userId = userDocument._id;
    this.pins = [];
  }

  addPin(pin: Pin) {
    this.pins.push(pin);
  }

  removePin(pinId: string) {
    _.remove(this.pins, (pin) => {
      return pin.id == pinId
    });
  }

  clear() {
    this.pins = [];
  }
}

export class Teacher extends User {

}

export class Student extends User {

}

export class Lesson {
  public card: Card;
  public teacher: Teacher;
  public students: Student[];
  public isLocked: boolean;

  constructor(card: Card, teacher: Teacher) {
    this.card = card;
    this.teacher = teacher;
    this.students = [];
    this.isLocked = true;
  }

  lock() {
    this.isLocked = true;
  }

  unlock() {
    this.isLocked = false;
  }

  clear() {
    this.teacher.clear();
    _.forEach(this.students, (student) => {
      student.clear();
    });
  }

  addStudent(userDocument: any) {
    let student = new Student(userDocument);
    this.students.push(student);
  }

  removeStudent(userDocument: any) {
    _.remove(this.students, (student) => {
      return student.userId == userDocument._id
    });
  }

  addTeacherPin(pin: Pin) {
    this.teacher.addPin(pin);
  }

  removeTeacherPin(pinId: string) {
    this.teacher.removePin(pinId);
  }

  clearTeacher() {
    this.teacher.clear();
  }

  addStudentPin(userDocument: any, pin: Pin) {
    _.forEach(this.students, (student) => {
      if (student.userId == userDocument._id) {
        student.addPin(pin);
      }
    });
  }

  removeStudentPin(userDocument: any, pinId: string) {
    _.forEach(this.students, (student) => {
      if (student.userId == userDocument._id) {
        student.removePin(pinId);
      }
    });
  }

  clearStudent(userDocument: any) {
    _.forEach(this.students, (student) => {
      if (student.userId == userDocument._id) {
        student.clear();
      }
    });
  }
}